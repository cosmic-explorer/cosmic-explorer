"""
"""
import importlib
import os


def load_budget(name_or_path, freq=None, bname=None):
    from gwinc import load_budget as gwinc_load_budget

    if os.path.exists(name_or_path):
        return gwinc_load_budget(name_or_path, freq=freq, bname=bname)

    path = importlib.resources.files("cosmicexplorer").joinpath("gwinc", name_or_path)
    return gwinc_load_budget(str(path), freq=freq, bname=bname)
