import finesse.analysis.actions as fa
from gwinc.optomechanicalmodels.lib import MatsHelper
from gwinc.optomechanicalmodels.common import standardize_params
from gwinc.nb import precomp
from gwinc.suspension import precomp_suspension

from ...finesse.model import CosmicExplorerFactory, InitialLock
from ...qlib import to_2p, to_2p_vec


loss_ports = dict(
    Arm=[
        "ETMX.fr.o",
        "ETMY.fr.o",
    ],
    SEC=["SEM.fr.o"],
)

strain_exc = {"DARM.AC": 1}


def run_fresp(F_Hz):
    factory = CosmicExplorerFactory("reverse_ligo.yaml")
    # factory = CosmicExplorerFactory("split_telescope.yaml")
    # factory.vertex_type = "split_telescope"
    model = factory.make()
    # model.modes("off")
    model.modes("even", maxtem=2)
    print("stability")
    for cav in model.cavities:
        print(cav.name, cav.is_stable)
    fsig = model.fsig.f.ref
    readout = [
        ("OFI.p3.o", +fsig),
        ("OFI.p3.o", -fsig),
    ]
    sol = model.run(
        fa.Series(
            InitialLock(),
            fa.FrequencyResponse(F_Hz, ["DARM.AC"], ["BHD.DC.o"], name="fresp"),
            fa.FrequencyResponse3(
                F_Hz,
                [
                    ("OFI.p2.i", +fsig),
                    ("OFI.p2.i", -fsig),
                    ("SEM.fr.o", +fsig),
                    ("SEM.fr.o", -fsig),
                    ("ETMX.fr.o", +fsig),
                    ("ETMX.fr.o", -fsig),
                    ("ETMY.fr.o", +fsig),
                    ("ETMY.fr.o", -fsig),
                ],
                readout,
                name="fresp3",
            ),
            fa.FrequencyResponse4(
                F_Hz,
                [
                    "DARM.AC",
                ],
                readout,
                name="fresp4",
            ),
        )
    )
    return sol


def finesse2gwinc(fresp3, fresp4, ifo):
    import numpy as np
    mats = MatsHelper()
    mats.H["AS"], mlib = to_2p(fresp3[:, :, :2])
    mats.T["OFI.p2.i"] = to_2p(fresp3[:, :, :2])[0]
    mats.T["SEM.fr.o"] = to_2p(fresp3[:, :, 2:4])[0] * np.sqrt(ifo.Optics.BSLoss)
    mats.T["ETMX.fr.o"] = to_2p(fresp3[:, :, 4:6])[0] * np.sqrt(ifo.Optics.Loss)
    mats.T["ETMY.fr.o"] = to_2p(fresp3[:, :, 6:])[0] * np.sqrt(ifo.Optics.Loss)
    mats.T["Loss_injection"] = mats.T["OFI.p2.i"] * np.sqrt(ifo.Squeezer.InjectionLoss)
    mats.T["DARM.AC"] = to_2p_vec(fresp4)[0]
    readout_loss = 1 - ifo.Optics.PhotoDetectorEfficiency
    # mats.T["Loss_readout"] = mlib.diag(np.sqrt(readout_loss))
    mats.T["Loss_readout"] = np.ones_like(mats.T["Loss_injection"]) * np.sqrt(readout_loss)
    return mats, mlib


@precomp(sustf=precomp_suspension)
def precomp_optomechanical_plant(freq, ifo, sustf):
    from wield.utilities.file_io import load
    import importlib
    params = standardize_params(ifo)
    sol = run_fresp(freq)
    mats, mlib = finesse2gwinc(sol["fresp3"].out, sol["fresp4"].out, ifo)
    # path = importlib.resources.files("cosmicexplorer").joinpath(
    #     "gwinc", "CEFinesseQuantum", "quantum_fresp.h5")
    # data = load(str(path))
    # mats, mlib = finesse2gwinc(data.fresp3, data.fresp4, ifo)
    pc = dict()
    pc["mats"] = mats
    pc["loss_ports"] = loss_ports
    pc["strain_exc"] = strain_exc
    pc["mlib"] = mlib
    return pc
