"""
Core Cosmic Explorer DRFPMI
"""
from wield.model import optics, base
from wield import model
from wield.bunch import Bunch

from . import VERTEX_TYPES

def DRFPMI(params, *, sys=None, vertex_type="reverse_ligo"):
    if vertex_type not in VERTEX_TYPES:
        raise ValueError(f"unrecognized vertex type {vertex_type}")

    if sys is None:
        sys = model.system1064()

    lengths = params.Length_m

    # add core optics
    mirrors = ["ETMX", "ITMX", "ETMY", "ITMY", "PRM", "SEM", "BS"]
    if vertex_type == "reverse_ligo":
        mirrors.extend(["XM2", "XM3", "YM2", "YM3"])
    elif vertex_type in ["split_telescope", "crab", "braid"]:
        mirrors.extend(["XM", "YM", "PR2", "SE2"])
    for mirror in mirrors:
        pars = Bunch(params[mirror])
        key = mirror + "/"
        sys[key] = optics.Mirror()
        sys[key + "T"] = pars.T
        ROC_m = pars.get("Rc_m", None)
        if isinstance(ROC_m, list):
            ROC_m = ROC_m[0]
        sys[key + "ROC[m]"] = -ROC_m if ROC_m is not None else None
        sys[key + "AOI[deg]"] = pars.get("aoi_deg", 0)
        sys[key + "depth[m]"] = pars.get("thickness_m", 0)

    # add arm cavities
    Larm_m = lengths.pop("ARM")
    sys["ITMX_ETMX/"] = optics.Space()
    sys["ITMY_ETMY/"] = optics.Space()
    sys["ITMX_ETMX/length[m]"] = Larm_m
    sys["ITMY_ETMY/length[m]"] = Larm_m
    sys.bond_add(
        'ITMX+A | ITMX_ETMX+A-t | ETMX+A',
        'ITMY+A | ITMY_ETMY+A-t | ETMY+A',
    )
    sys['XARM/'] = optics.Cavity()
    sys['YARM/'] = optics.Cavity()
    sys['XARM/waypoints'] = ['ITMX/', 'ETMX/']
    sys['YARM/waypoints'] = ['ITMY/', 'ETMY/']

    # add vertex
    for name, length in lengths.items():
        sys[name + "/"] = optics.Space()
        sys[name + "/length[m]"] = length

    if vertex_type == 'reverse_ligo':
        sys.bond_add(
            'PRM+A | PRM_BS+A-t | BS+A1',
            'SEM+A | SEM_BS+A-t | BS+B2',
            [
                'BS+A2 | BS_YM2+A-t | YM2+A1-r | YM2_YM3+A-t | YM3+A1-r',
                'YM3_ITMY+A-t | ITMY+B',
            ],
            [
                'BS+B1 | BS_XM2+A-t | XM2+A1-r | XM2_XM3+A-t | XM3+A1-r',
                'XM3_ITMX+A-t | ITMX+B',
            ],
        )

    elif vertex_type in ['split_telescope', 'crab', 'braid']:
        sys.bond_add(
            'PRM+A | PRM_PR2+A-t | PR2+A1-r | PR2_BS+A-t | BS+A1',
            'SEM+A | SEM_SE2+A-t | SE2+A1-r | SE2_BS+A-t | BS+B2',
            'BS+A2 | BS_YM+A-t | YM+A1-r | YM_ITMY+A-t | ITMY+B',
            'BS+B1 | BS_XM+A-t | XM+A1-r | XM_ITMX+A-t | ITMX+B',
        )

    return sys
