from wield.control.SFLU import SFLU

from .elements import GraphElement
from .components import Link
from ...qlib import MatrixLib


class Model:
    def __init__(self, nhom=0):
        self.__elements = {}
        self.__edges = {}
        self.__components = {}
        self.__graph = GraphElement()
        self._mlib = MatrixLib(nhom=nhom)
        self._inputs = []
        self._outputs = []
        self.__ss = None

    @property
    def mlib(self):
        return self._mlib

    @property
    def graph(self):
        return self.__graph

    @property
    def components(self):
        return self.__components

    @property
    def ss(self):
        return self.__ss

    @property
    def nhom(self):
        return self.mlib.nhom

    def add(self, component, *graph_args, **graph_kwargs):
        if hasattr(self, component.name):
            raise AttributeError(
                f"A component named {component.name} already exists",
            )
        assert self.nhom == component.nhom
        setattr(self, component.name, component)
        self.__components[component.name] = component
        component.update_graph(self.graph, *graph_args, **graph_kwargs)
        return component

    def connect(self, to, fr, *args, **kwargs):
        name = f"{to}___{fr}"
        link = self.add(Link(name=name, nhom=self.nhom, *args, **kwargs))
        self.graph.edges[(to, fr)] = name
        return link

    def _edge_map(self):
        return {
            "1": self.mlib.Id,
            "1s": self.mlib.Id_s,
        }

    def add_input(self, pnt, edge="1", loc=(0, 0)):
        # FIXME: add some assertions
        self.graph.locations[f"{pnt}.exc"] = loc
        self.graph.edges[(pnt, f"{pnt}.exc")] = edge

    def add_output(self, pnt, edge="1", loc=(0, 0)):
        # FIXME: add some assertions
        self.graph.locations[f"{pnt}.tp"] = loc
        self.graph.edges[(f"{pnt}.tp", pnt)] = edge

    def _build_sflu(self, reduce_auto=True):
        sflu = SFLU.SFLU(edges=self.graph.build_edges(), graph=True)
        self.graph.update_sflu(sflu)
        if reduce_auto:
            sflu.reduce_auto()
        return sflu

    def computeDC(self, Rset, Cmap):
        sflu = self._build_sflu()
        edge_map = self._edge_map()
        for component in self.components.values():
            edge_map.update(component.edgesDC())
        comp = sflu.computer(eye=self.mlib.Id)
        comp.compute(edge_map=edge_map)
        results = comp.inverse_col(Rset=Rset, Cmap=Cmap)
        return results

    def computeAC(self, F_Hz, Rmap, Cset, resultsDC={}):
        sflu = self._build_sflu()
        edge_map = self._edge_map()
        for component in self.components.values():
            edge_map.update(component.edgesAC(F_Hz=F_Hz, resultsDC=resultsDC))
        comp = sflu.computer(eye=self.mlib.Id)
        comp.compute(edge_map=edge_map)
        results = comp.inverse_row(Rmap=Rmap, Cset=Cset)
        return results

    def computeACSS(self, F_Hz, Rmap, Cset, resultsDC={}):
        sflu = self._build_sflu()
        edge_map = self._edge_map()
        for component in self.components.values():
            edge_map.update(
                component.edgesACSS(resultsDC=resultsDC, Fmax_Hz=F_Hz.max())
            )
        comp = sflu.SScomputer(eye=self.mlib.Id)
        self.__ss = comp.SScompletion(edge_map)
        results = comp.inverse_row_fresponse(Rmap=Rmap, Cset=Cset, F_Hz=F_Hz)
        return results
