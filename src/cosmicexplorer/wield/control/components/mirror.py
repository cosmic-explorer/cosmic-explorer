import numpy as np
from scipy.constants import c as c_light

from wield.control import SISO

from .component import Component
from ..elements import GraphElement, RPMirrorElement
from ....qlib import MatrixLib


class Mirror(Component):
    """
    Defines DC and AC edges for mirrors with radiation pressure

    Parameters
    ----------
    name : str
      Name of the mirror
    Thr : float, optional
      Transmissivity of the HR surface, 0 by default
    Lhr : float, optional
      Loss of the HR surface, 0 by default
    Rar : float, optional
      Reflectivity of the AR surface, 0 by default
    suscept : callable, optional
      Mechanical susceptibility as a function of frequency in Hz, 0 by default
    lambda_m : float, optional
      Wavelength [m], 1064e-9 by default
    overlap : scalar, (nhom + 1, nhom + 1) matrix, or (dim, dim) matrix
      Matrix of overlap integrals between optical and mechanical modes,
      1 by default
    mlib : MatrixLib instance, optional
      MatrixLib to use for calculations, MatrixLib(nhom=0) by default

    Examples
    --------
    5 ppm transmissive free mass mirror of mass M_kg
    >>> suscept = lambda F_Hz: -1 / (M_kg * (2 * np.pi * F_Hz)**2)
    >>> mirr = RPMirrorEdge('M', Thr=5e-6, suscept=suscept)

    Bulk mode of a M_kg mass mirror with mechanical frequency Fm_Hz,
    mechanical Q of Qm, and mode overlap integrals Bnm for one HOM
    >>> def suscept(F_Hz):
            den = Fm_Hz**2 - F_Hz**2 + 1j * Fm_Hz * F_Hz / Qm
            return 1 / (M_kg * (2 * np.pi)**2 * den)
    >>> overlap = np.array([
            [B00, B01],
            [B01, B11],
        ])
    >>> mlib = MatrixLib(nhom=1)
    >>> mirr = RPMirrorEdge('M', suscept=suscept, overlap=overlap, mlib=mlib)
    """
    def __init__(
            self,
            name,
            Thr=0,
            Lhr=0,
            Rar=0,
            suscept=lambda x: np.zeros_like(x),
            suscept_ss=SISO.zpk([], [], 0).asSS,
            lambda_m=1064e-9,
            overlap=1,
            nhom=0,
    ):
        super().__init__(name=name, nhom=nhom)
        self.t = self.mlib.promote(np.sqrt(Thr))
        self.l = self.mlib.promote(np.sqrt(Lhr))
        self.r = self.mlib.promote(np.sqrt(
            self.mlib.Id
            - self.mlib.promote(Thr)
            - self.mlib.promote(Lhr)
            - self.mlib.promote(Rar)
        ))
        self.suscept = suscept
        self.suscept_ss = suscept_ss
        self.lambda_m = lambda_m
        self.overlap = self.mlib.promote(overlap)

    def update_graph(self, graph, translation_xy=(0, 0), rotation_deg=0):
        # super().update_graph(graph)
        # subgraph = GraphElement()
        # subgraph.locations.update({
        #     "fr.i": (-6, +7),
        #     "fr.o": (-6, -7),
        #     "bk.i": (+6, -7),
        #     "bk.o": (+6, +7),

        #     "pos": (0, 0),

        #     "fr.i.tp": (-9, +10),
        #     "fr.o.tp": (-9, -10),
        #     "bk.i.tp": (+9, -10),
        #     "bk.o.tp": (+9, +10),

        #     "pos.tp": (-9, 0),
        #     "pos.exc": (+9, 0),
        # })
        # subgraph.edges.update({
        #     ("fr.o", "fr.i"): ".fr.r",
        #     ("bk.o", "bk.i"): ".bk.r",
        #     ("bk.o", "fr.i"): ".fr.t",
        #     ("fr.o", "bk.i"): ".bk.t",

        #     ("pos", "fr.i"): ".fr.xq.i",
        #     ("pos", "fr.o"): ".fr.xq.o",
        #     ("pos", "bk.i"): ".bk.xq.i",
        #     ("pos", "bk.o"): ".bk.xq.o",

        #     ("fr.o", "pos"): ".fr.px",
        #     ("bk.o", "pos"): ".bk.px",

        #     ("fr.i.tp", "fr.i"): "1",
        #     ("fr.o.tp", "fr.o"): "1",
        #     ("bk.i.tp", "bk.i"): "1",
        #     ("bk.o.tp", "bk.o"): "1",

        #     ("pos.tp", "pos"): "1s",
        #     ("pos", "pos.exc"): "1s",

        # })
        subgraph = RPMirrorElement()
        graph.subgraph_add(
            self.name,
            subgraph,
            translation_xy=translation_xy,
            rotation_deg=rotation_deg,
        )

    def _optic_edges(self):
        edge_map = {
            self.name + ".fr.r": -self.r,
            self.name + ".bk.r": +self.r,
            self.name + ".fr.t": self.t,
            self.name + ".bk.t": self.t,
            self.name + ".fr.l": self.l,
            self.name + ".bk.l": self.l,
        }
        return edge_map

    def edgesDC(self):
        edge_map = self._optic_edges()
        # no radiation pressure at DC
        zz = {k: 0 * getattr(self.mlib, k) for k in ["Id_a", "Id_v", "Id_s"]}
        edge_map.update({
            self.name + ".fr.xq.i": zz["Id_a"],
            self.name + ".fr.xq.o": zz["Id_a"],
            self.name + ".bk.xq.i": zz["Id_a"],
            self.name + ".bk.xq.o": zz["Id_a"],
            self.name + ".fr.px": zz["Id_v"],
            self.name + ".bk.px": zz["Id_v"],
        })
        return edge_map

    def _edgesAC(self, chi, resultsDC):
        edge_map = self._optic_edges()

        # DC fields at the mirror faces
        def get_fieldsDC(tp):
            try:
                return resultsDC[self.name + tp]
            except KeyError:
                return 0 * self.mlib.Id_v

        fieldsDC_fr_i = get_fieldsDC(".fr.i.tp")
        fieldsDC_fr_o = get_fieldsDC(".fr.o.tp")
        fieldsDC_bk_i = get_fieldsDC(".bk.i.tp")
        fieldsDC_bk_o = get_fieldsDC(".bk.o.tp")

        # displacement to p (phase) quadrature
        px = 4 * np.pi / self.lambda_m * self.r @ self.overlap
        px_fr = px @ self.mlib.Mrotation(np.pi/2) @ fieldsDC_fr_i
        px_bk = px @ self.mlib.Mrotation(np.pi/2) @ fieldsDC_bk_i

        # q (amplitude) quadrature to displacement
        def xq_port(fieldsDC):
            return chi @ (2 / c_light * self.mlib.adjoint(self.overlap @ fieldsDC))

        xq_fr_i = +xq_port(fieldsDC_fr_i)
        xq_fr_o = +xq_port(fieldsDC_fr_o)
        xq_bk_i = -xq_port(fieldsDC_bk_i)
        xq_bk_o = -xq_port(fieldsDC_bk_o)

        edge_map.update({
            self.name + ".fr.xq.i": xq_fr_i,
            self.name + ".fr.xq.o": xq_fr_o,
            self.name + ".bk.xq.i": xq_bk_i,
            self.name + ".bk.xq.o": xq_bk_o,
            self.name + ".fr.px": px_fr,
            self.name + ".bk.px": px_bk,
        })
        return edge_map

    def edgesAC(self, F_Hz, resultsDC, *args, **kwargs):
        # mechanical susceptibility, reshaped for multiplication
        chi = self.suscept(F_Hz)
        if np.isscalar(chi):
            chi = chi * self.mlib.Id_s
        else:
            chi = chi.reshape((-1, 1, 1))
        return self._edgesAC(chi=chi, resultsDC=resultsDC)

    def edgesACSS(self, resultsDC, *args, **kwargs):
        return self._edgesAC(chi=self.suscept_ss.ss, resultsDC=resultsDC)
