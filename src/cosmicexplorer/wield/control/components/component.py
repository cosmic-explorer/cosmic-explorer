from abc import ABC, abstractmethod

from ....qlib import MatrixLib


class Component(ABC):
    def __init__(self, name, nhom=0):
        self._name = name
        self._mlib = MatrixLib(nhom=nhom)

    @property
    def name(self):
        return self._name

    @property
    def mlib(self):
        return self._mlib

    @property
    def nhom(self):
        return self.mlib.nhom

    @abstractmethod
    def update_graph(self, graph, translation_xy=(0, 0), rotation_deg=0):
        # graph.subgraph_add(self.name, self, translation_xy=(0, 0))
        pass

    @abstractmethod
    def edgesDC(self):
        pass

    @abstractmethod
    def edgesAC(self, F_Hz, edgesDC={}):
        pass

    @abstractmethod
    def edgesACSS(self, edgesDC={}):
        pass
