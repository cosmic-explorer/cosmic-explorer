import numpy as np
from scipy.constants import c as c_light

from wield.control import SISO, ss_bare
import wield.control.ss_bare.design

from .component import Component

pi2i = 2j * np.pi


class Link(Component):
    """
    Defines DC and AC edges for propagation links

    Parameters
    ----------
    name : str
      Name of the link
    L_m: float
      Macroscopic length of the link [m]
    detune_rad : float, optional
      Common microscopic detuning of all fields [rad], 0 by default
    gouy_rad : nhom element list of scalars or (N,) arrays, optional
      Gouy phases for each HOM [rad], all 0 by default
    MM_fr : scalar or (dim, dim) array, optional
      Mode matching basis transformation at the beginning of the link,
      (dim, dim) identity by default
    MM_to : scalar or (dim, dim) array, optional
      Mode matching basis transformation at the end of the link,
      (dim, dim) identity by default
    mlib : MatrixLib instance, optional
      MatrixLib to use for calculations, MatrixLib(nhom=0) by default
    """
    def __init__(
            self,
            name,
            L_m,
            detune_rad=0,
            gouy_rad=None,
            MM_fr=1,
            MM_to=1,
            nhom=0,
    ):
        super().__init__(name=name, nhom=nhom)
        self.L_m = L_m
        self.detune_rad = detune_rad
        self.MM_fr = self.mlib.promote(MM_fr)
        self.MM_to = self.mlib.promote(MM_to)

        if gouy_rad is None:
            self.gouy_rad = np.zeros(self.nhom)
        else:
            if np.isscalar(gouy_rad):
                if self.nhom == 0:
                    self.gouy_rad = np.zeros(0)
                elif self.nhom == 1:
                    self.gouy_rad = np.array([gouy_rad])
                else:
                    raise ValueError("need gouy_phases for all HOMs")
            else:
                assert len(gouy_rad) == self.nhom
                self.gouy_rad = gouy_rad

    def update_graph(self, graph):
        # all graph connections currently made in Model
        pass

    def _edges(self, Lmat):
        """
        This works equivalently in statespace
        """
        edge_map = {
            self.name: self.MM_to @ Lmat @ self.MM_fr,
        }
        return edge_map

    def edgesDC(self):
        """
        Returns the DC edge map dictionary
        """
        Lmat = self.mlib.Mrotation(self.detune_rad, *self.gouy_rad)
        return self._edges(Lmat)

    def edgesAC(self, F_Hz, *args, **kwargs):
        """
        Returns the AC edge map dictionary

        F_Hz: Frequency vector at which to evaluate the edge map
        """
        delay = self.mlib.diag(np.exp(-pi2i * F_Hz * self.L_m / c_light))
        Lmat = delay @ self.mlib.Mrotation(self.detune_rad, *self.gouy_rad)
        return self._edges(Lmat)

    def edgesACSS(self, Fmax_Hz, *args, **kwargs):
        """
        Returns the AC edge map dictionary

        F_Hz: Frequency vector at which to evaluate the edge map
        """
        # TODO, need to compute the order more appropriately
        if self.L_m > 0:
            order = int(self.L_m / c_light * Fmax_Hz + 1)
            # print("Useorder: ", order)
            singledelay = SISO.design.delay_thiran_raw(
                delay_s=self.L_m / c_light,
                order=order + 2,
            ).asSS
            delayABCD = ss_bare.design.replicateSS(ss=singledelay.ss, dim=self.mlib.dim)

            Lmat = delayABCD @ self.mlib.Mrotation(self.detune_rad, *self.gouy_rad)
        else:
            Lmat = self.mlib.Mrotation(self.detune_rad, *self.gouy_rad)
        return self._edges(Lmat)
