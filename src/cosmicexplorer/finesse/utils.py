"""
"""

def sqz_metrics_from_finesse(fresp_sol, LOphase_rad=0):
    """
    Calculate the squeezing degradation metrics from a finesse FrequencyResponse3

    Parameters
    ----------
    fresp_sol : (nfreqs, 2, 2, nhoms, nhoms) array
        Output from a finesse FrequencyResponse3
    LOphase_rad : float, optional
        LO phase. Default: 0 rad.
    """
    from ..qlib import to_2p, thetaXietaGamma
    assert fresp_sol.shape[1:3] == (2, 2)
    tfs_2p, mlib = to_2p(fresp_sol)
    LOa = mlib.adjoint(mlib.LO(LOphase_rad))
    LOdotAS = LOa @ tfs_2p
    mq = LOdotAS[..., 0, 0]
    mp = LOdotAS[..., 0, 1]
    metrics = thetaXietaGamma(mq, mp)
    metrics['MM_loss'] = mlib.Vnorm_sq(mlib.adjoint(LOdotAS)) - metrics.etaGamma
    return metrics


class PortHelper:
    def __init__(self):
        self.ports = dict()

    def __setitem__(self, key, val):
        self.ports[key] = val

    def __getitem__(self, key):
        try:
            return self.ports[key].full_name
        except AttributeError:
            return self.ports[key]

    def make_test_points(self, fsig):
        test_points = []
        for port in self.ports.values():
            test_points.extend(
                [
                    (port, +fsig),
                    (port, -fsig),
                ]
            )
        return test_points

    def __call__(self, fsig):
        return self.make_test_points(fsig)

    def idx(self, key):
        return list(self.ports.keys()).index(key)

    def u(self, key):
        return 2 * self.idx(key)

    def l(self, key):
        return 2 * self.idx(key) + 1

    def slc(self, key):
        idx = self.idx(key)
        return slice(2 * idx, 2 * (idx + 1))


def sum_homs(homs, vals, axis=0):
    assert len(homs) == vals.shape[axis]
    total = {}
    for hom, val in zip(homs, vals):
        order = np.sum(hom)
        if order in total:
            total[order] += val
        else:
            total[order] = val
    return total
