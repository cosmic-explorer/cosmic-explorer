from abc import ABC, abstractmethod

from finesse import Model
from wield.bunch import Bunch

from .. import components as fc
from .arm import SingleArmFactory
from .drfpmi import MICH
from .factory import add_all_amplitude_detectors
from ... import update_mapping


class CoupledCavityFactory(SingleArmFactory, ABC):
    def set_default_options(self):
        super().set_default_options()
        self.options.add_cavities[self.cavity] = True
        self.options.zero_all_aoi = False
        self.options.manual_rc_gouy_phase = False
        self.options.manual_arm_gouy_phase = False

    @property
    @abstractmethod
    def RM(self):
        pass

    @property
    @abstractmethod
    def cavity(self):
        pass

    def set_default_drives(self, L_force=False, P_torque=True, Y_torque=True):
        super().set_default_drives(
            L_force=L_force, P_torque=P_torque, Y_torque=Y_torque,
        )
        Ltype = "F_" if L_force else ""
        Ptype = "F_" if P_torque else ""
        Ytype = "F_" if Y_torque else ""
        update_mapping(
            self.local_drives,
            Bunch(
                L=Bunch({
                    self.RM: {f"{self.RM}.mech.{Ltype}z": 1},
                    "BS": {f"BS.mech.{Ltype}z": 1},
                }),
                P=Bunch({
                    self.RM: {f"{self.RM}.mech.{Ptype}pitch": 1},
                    "BS": {f"BS.mech.{Ptype}pitch": 1},
                }),
                Y=Bunch({
                    self.RM: {f"{self.RM}.mech.{Ytype}yaw": 1},
                    "BS": {f"BS.mech.{Ytype}yaw": 1},
                }),
            )
        )

    def set_default_LSC(self):
        super().set_default_LSC()
        self.LSC_output_matrix[f"{self.cavity}L"] = Bunch({self.RM: +1})

        self.LSC_input_matrix[f"{self.cavity}L"] = Bunch()

    def make(self):
        model = Model()
        self.pre_make(model)
        self.make_arm(model)
        self.add_BS(model)
        MICH(
            params=self.params.IFO,
            arm=self.arm,
            model=model,
            refl_port=self.ports.arm_refl,
        )
        self.add_recycling_cavity(model)
        self.add_io(model)
        self.add_suspensions(model)
        self.add_LSC(model)
        if self.options.ASC.add:
            self.add_ASC(model)
        if self.options.add_cavities:
            self.add_cavities(model)
        if self.options.add_amplitude_detectors:
            self.add_amplitude_detectors(model)
        if self.options.zero_all_aoi:
            for optic in self.params.IFO:
                if optic in model.elements:
                    if hasattr(model.get(optic), "alpha"):
                        model.get(optic).alpha = 0
        if self.options.manual_rc_gouy_phase:
            self.set_manual_rc_gouy_phase(model)
        if self.options.manual_arm_gouy_phase:
            self.set_manual_arm_gouy_phase(model)
        self.post_make(model)
        # we're strict energy conservationists here in Cosmic Explorer
        model.phase_config(zero_k00=False)
        return model

    @abstractmethod
    def add_BS(self, model):
        pass

    @abstractmethod
    def add_recycling_cavity(self, model):
        pass

    @abstractmethod
    def add_io(self, model):
        pass

    def set_manual_rc_gouy_phase(self, model):
        space = model.get(self.RM).fr.attached_to
        assert isinstance(space, fc.Space)
        avg_deg, astig_deg = self._zero_cavity_gouy_phase(
            model.get(f"cav{self.cavity}"),
        )
        # divide average by 2 for default to be the same one-way
        self._set_space_gouy_phase(
            model,
            space,
            f"{self.cavity}_Gouy_deg",
            avg_deg / 2,
            astig_deg / 2,
        )

    def set_manual_arm_gouy_phase(self, model):
        space = model.get(self.ETM).fr.attached_to
        assert isinstance(space, fc.Space)
        avg_deg, astig_deg = self._zero_cavity_gouy_phase(
            model.get(f"cav{self.arm}ARM"),
        )
        # divide average by 2 for default to be the same one-way
        self._set_space_gouy_phase(
            model,
            space,
            f"ARM_Gouy_deg",
            avg_deg / 2,
            astig_deg / 2,
        )

    def add_amplitude_detectors(self, model):
        super().add_amplitude_detectors(model)
        add_all_amplitude_detectors(
            model=model,
            port=model.get(self.RM).fr.o,
            key=self.cavity,
            add_power_detectors=True,
        )

    def pre_make(self, model):
        pass

    def post_make(self, model):
        ITM_HR = model.get(self.ITM).fr_sub.i
        RM_HR = model.get(self.RM).fr.o
        rc_path = model.path(RM_HR, ITM_HR, symbolic=True)
        model.add(fc.Variable(
            f"L_{self.cavity}",
            rc_path.optical_length,
            description=f"Optical {self.cavity} length",
        ))
        model.add(fc.Variable(
            f"L_{self.cavity}_physical",
            rc_path.physical_length,
            description=f"Physical {self.cavity} length",
        ))
