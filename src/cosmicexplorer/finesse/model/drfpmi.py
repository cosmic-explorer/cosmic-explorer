"""
Core Cosmic Explorer DRFPMI
"""
from numpy import inf

from finesse import Model

from .. import components as fc
from . import to_finesse_kwargs


def ARM(params, *, arm="X", model=None, include_ITMlens=False):
    assert arm in ["X", "Y"]
    if model is None:
        model = Model()

    for mirror in [f"ETM{arm}", f"ITM{arm}"]:
        model.add(fc.ThickMirror(mirror, **to_finesse_kwargs(params[mirror])))
    model.connect(
        model.get(f"ITM{arm}").fr,
        model.get(f"ETM{arm}").fr,
        L=params.Length_m.ARM,
        name=f"L{arm}",
    )
    if include_ITMlens:
        model.add(fc.Lens(f"ITM{arm}lens", params[f"ITM{arm}"].get("f_m", inf)))
        model.connect(
            model.get(f"ITM{arm}lens").p2,
            model.get(f"ITM{arm}").bk,
        )
        refl_port = model.get(f"ITM{arm}lens").p1
    else:
        refl_port = model.get(f"ITM{arm}").bk
    return model, refl_port


def MICH(params, model, refl_port, arm="X"):
    assert arm in ["X", "Y"]
    assert "BS" in model.elements

    if arm == "X":
        bs_port = model.BS.bk1
    elif arm == "Y":
        bs_port = model.BS.fr2

    folding_optics = [f"M{arm}1", f"M{arm}2"]
    for bs in folding_optics:
        model.add(fc.ThickBeamsplitter(bs, **to_finesse_kwargs(params[bs])))

    lengths = params.Length_m
    model.connect(
        bs_port, model.get(f"M{arm}1.fr1"), lengths[f"BS_M{arm}1"],
        name=f"BS_M{arm}1",
    )
    model.connect(
        model.get(f"M{arm}1.fr2"), model.get(f"M{arm}2.fr1"),
        lengths[f"M{arm}1_M{arm}2"], name=f"M{arm}1_M{arm}2",
    )
    model.connect(
        model.get(f"M{arm}2.fr2"), refl_port,
        lengths[f"M{arm}2_ITM{arm}"], name=f"M{arm}2_ITM{arm}",
    )


def DRFPMI(params, *, model=None, include_ITMlens=False):
    if model is None:
        model = Model()

    # Arm cavities
    _, x_refl_port = ARM(
        params, arm="X", model=model, include_ITMlens=include_ITMlens,
    )
    _, y_refl_port = ARM(
        params, arm="Y", model=model, include_ITMlens=include_ITMlens,
    )

    # other core optics
    for mirror in ["PRM", "SEM"]:
        model.add(fc.ThickMirror(mirror, **to_finesse_kwargs(params[mirror])))
    model.add(fc.ThickBeamsplitter("BS", **to_finesse_kwargs(params.BS)))

    for bs in ["PR2", "PR3"]:
        model.add(fc.ThickBeamsplitter(bs, **to_finesse_kwargs(params[bs])))

    # MICH
    MICH(
        params=params,
        arm="X",
        model=model,
        refl_port=x_refl_port,
    )
    MICH(
        params=params,
        arm="Y",
        model=model,
        refl_port=y_refl_port,
    )

    lengths = params.Length_m
    # PRC
    model.connect(model.PRM.fr, model.PR2.fr1, lengths.PRM_PR2, name="PRM_PR2")
    model.connect(model.PR2.fr2, model.PR3.fr1, lengths.PR2_PR3, name="PR2_PR3")
    model.connect(model.PR3.fr2, model.BS.fr1, lengths.PR3_BS, name="PR3_BS")
    # SEC
    model.connect(model.SEM.fr, model.BS.bk2, lengths.SEM_BS, name="SEM_BS")

    return model
