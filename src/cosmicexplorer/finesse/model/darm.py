import numpy as np

from finesse import model
from wield.bunch import Bunch
import finesse.analysis.actions as fa
from finesse.symbols import Constant
from finesse.detectors import MathDetector

from .. import components as fc
from .coupledcavity import CoupledCavityFactory
from . import FilterCavity
from . import to_finesse_kwargs
from .factory import add_all_amplitude_detectors


class DARMFactory(CoupledCavityFactory):
    def set_default_options(self):
        super().set_default_options()
        self.options.add_FC = False
        # self.options.add_cavities.SEC = True

    def set_default_LSC(self):
        super().set_default_LSC()
        self.LSC_output_matrix.DARM = Bunch({self.ETM: +1})

        self.LSC_input_matrix["DARM"] = Bunch()

    @property
    def RM(self):
        return "SEM"

    @property
    def cavity(self):
        return "SEC"

    def add_BS(self, model):
        model.add(
            fc.ThickBeamsplitter("BS", **to_finesse_kwargs(self.params.IFO.BS))
        )
        if self.arm == "X":
            model.BS.T = 0
        elif self.arm == "Y":
            model.BS.T = 1

    def make(self):
        model = super().make()
        model.add(fc.Laser("_lock_laser", P=0))
        model.connect(model._lock_laser.p1, model.get(self.ETM).bk)
        return model

    def add_recycling_cavity(self, model):
        lengths = self.params.IFO.Length_m
        model.add(fc.ThickMirror(
            "SEM", **to_finesse_kwargs(self.params.IFO.SEM)
        ))

        model.connect(
            model.SEM.fr, model.BS.bk2, lengths.SEM_BS, name="SEM_BS",
        )

    def add_io(self, model):
        model.add(fc.FaradayIsolator("OFI"))
        model.connect(model.SEM.bk, model.OFI.p1)
        if self.options.add_FC:
            FilterCavity(self.params.SQZ, model=model)
            # model.connect(
            #     model.SFI.p4, model.SEM.bk, self.params.SQZ.FC_SEM_length_m,
            # )
            model.connect(
                model.SFI.p4, model.OFI.p2, self.params.SQZ.FC_SEM_length_m,
            )
            # model.add(fc.ThickMirror("_oloss1", T=1, L=0))
            # model.add(fc.ThickMirror("_oloss2", T=1, L=0))
            # model.connect(model._oloss1.bk, model._oloss2.fr)
            # model.connect(model.OFI.p3, model._oloss1.fr)

    def add_amplitude_detectors(self, model):
        super().add_amplitude_detectors(model)
        P_ARM = Constant(model.P_c0_ARM)
        P_SEC = Constant(model.P_c0_SEC)
        model.add(MathDetector("cost_SECL", P_SEC + P_ARM))

    def add_cavities(self, model):
        super().add_cavities(model)
        if self.options.add_cavities.SEC:
            model.add(fc.Cavity(
                f"cavSEC", model.SEM.fr.o,
                via=model.get(f"ITM{self.arm}").fr_sub.i,
            ))

    def set_arm_power(self, model, Parm_W):
        # itm_o = f"{self.ITM}.fr.o"
        # itm_i = f"{self.ITM}.fr.i"
        # etm_o = f"{self.ETM}.fr.o"
        # etm_i = f"{self.ETM}.fr.i"
        itm_o = f"{self.ITM}_front.p1.o"
        itm_i = f"{self.ITM}_front.p1.i"
        etm_o = f"{self.ETM}_front.p1.o"
        etm_i = f"{self.ETM}_front.p1.i"
        sol = model.run(
            fa.Series(
                fa.DCFields(name="DC"),
                fa.Operator(itm_o, etm_i, name="M1"),
                fa.Operator(etm_i, etm_o, name="M2"),
                fa.Operator(etm_o, itm_i, name="M3"),
                fa.Operator(itm_i, itm_o, name="M4"),
            )
        )
        DC = sol["DC"]
        DC[:] = 0
        M1 = sol["M1"].operator
        M2 = sol["M2"].operator
        M3 = sol["M3"].operator
        M4 = sol["M4"].operator
        DC[etm_i, :, 0] = np.sqrt(Parm_W)

        def set_dc_field(p_to, p_fr, op):
            E_fr = DC[p_fr].reshape((-1, 1))
            E_to = op @ E_fr
            DC[p_to] = E_to.reshape((1, 1, -1))

        set_dc_field(etm_o, etm_i, M2)
        set_dc_field(itm_i, etm_o, M3)
        set_dc_field(itm_o, itm_i, M4)

        return DC


def InitialLock(pseudo_lock_arms=False):
    if pseudo_lock_arms:
        arm_locks = []
        raise NotImplementedError()
    else:
        arm_locks = [
            fa.Maximize("P_c0_ARM", "DARM.DC"),
        ]
    Ttmp = 1e-15
    tmp = {
        "SEM.misaligned": True,
        "SECL.DC": 0,
        "ETMX.T": Ttmp,
        "_lock_laser.P": 1 / Ttmp,
    }
    lock_action = fa.TemporaryParameters(
        fa.Series(
            fa.Change(**tmp),
            *arm_locks,
            fa.Change({"SEM.misaligned": False}),
            fa.Maximize("cost_SECL", "SECL.DC"),
            fa.Change({"SECL.DC": 90}, relative=True),
        ),
        exclude=["DARM.DC", "SECL.DC"],
    )
    return lock_action
