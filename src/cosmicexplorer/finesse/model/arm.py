from finesse import Model
from wield.bunch import Bunch

from .. import components as fc
from .factory import IFOFactory, add_all_amplitude_detectors
from .drfpmi import ARM
from .input import PSL


class SingleArmFactory(IFOFactory):
    def reset(self, reset_options=True):
        if reset_options:
            self.set_default_options()
        self.set_default_drives()
        self.set_default_LSC()
        self.set_default_ASC()
        self.ports = Bunch()

    def set_default_options(self):
        self.arm = "X"
        self.options = Bunch()
        self.options.include_ITMlens = True
        self.options.include_input = True
        self.options.add_cavities = Bunch(ARM=True)
        self.options.LSC = Bunch(add=True)
        self.options.add_amplitude_detectors = True
        self.options.ASC = Bunch(add=False)

        self.options.suspensions = Bunch()
        self.options.suspensions.testmass_model = None
        self.options.suspensions.testmass_options = Bunch()

    @property
    def arm(self):
        return self._arm

    @arm.setter
    def arm(self, val):
        if val not in ["X", "Y"]:
            raise ValueError("arm should be either X or Y")
        self._arm = val
        self.reset(reset_options=False)

    @property
    def ETM(self):
        return f"ETM{self.arm}"

    @property
    def ITM(self):
        return f"ITM{self.arm}"

    def set_default_drives(self, L_force=False, P_torque=True, Y_torque=True):
        Ltype = "F_" if L_force else ""
        Ptype = "F_" if P_torque else ""
        Ytype = "F_" if Y_torque else ""
        self.local_drives = Bunch(
            L=Bunch({
                self.ETM: {f"{self.ETM}.mech.{Ltype}z": 1},
                self.ITM: {f"{self.ITM}.mech.{Ltype}z": 1},
            }),
            P=Bunch({
                self.ETM: {f"{self.ETM}.mech.{Ptype}pitch": 1},
                self.ITM: {f"{self.ITM}.mech.{Ptype}pitch": 1},
            }),
            Y=Bunch({
                self.ETM: {f"{self.ETM}.mech.{Ytype}yaw": 1},
                self.ITM: {f"{self.ITM}.mech.{Ytype}yaw": 1},
            }),
        )

    def set_default_LSC(self):
        self.LSC_output_matrix = Bunch()
        self.LSC_output_matrix[f"{self.arm}ARM"] = Bunch({self.ETM: +1})

        self.LSC_input_matrix = Bunch()
        self.LSC_input_matrix[f"{self.arm}ARM"] = Bunch()

        self.LSC_controller = Bunch()

    def set_default_ASC(self):
        self.ASC_output_matrix = Bunch()
        self.ASC_output_matrix.HARD_P = Bunch({self.ITM: -1, self.ETM: "+rx_P"})
        self.ASC_output_matrix.SOFT_P = Bunch({self.ITM: +1, self.ETM: "+rx_P"})
        self.ASC_output_matrix.HARD_Y = Bunch({self.ITM: -1, self.ETM: "+rx_Y"})
        self.ASC_output_matrix.SOFT_Y = Bunch({self.ITM: +1, self.ETM: "+rx_Y"})

        self.ASC_input_matrix = Bunch(
            {
                "HARD_P": Bunch(),
                "SOFT_P": Bunch(),
                "HARD_Y": Bunch(),
                "SOFT_Y": Bunch(),
            }
        )

        self.ASC_controller = Bunch()

    def make_arm(self, model):
        _, refl_port = ARM(
            params=self.params.IFO,
            arm=self.arm,
            model=model,
            include_ITMlens=self.options.include_ITMlens,
        )
        self.ports.arm_refl = refl_port

    def add_LSC(self, model):
        if self.options.LSC.add:
            self.add_LSC_DOFs(model)

    def add_ASC(self, model):
        if self.options.ASC.add:
            self.add_geometric_factors(model, self.arm)
            self.add_ASC_DOFs(model)

    def add_suspensions(self, model):
        if self.options.suspensions.testmass_model is not None:
            kw = self.options.suspensions.testmass_options
            sus = self.options.suspensions.testmass_model
            for optic in [self.ITM, self.ETM]:
                model.add(sus(f"{optic}_sus", model.get(f"{optic}.mech"), **kw))

    def add_amplitude_detectors(self, model):
        add_all_amplitude_detectors(
            model=model,
            port=model.get(self.ETM).fr.o,
            key="ARM",
            add_power_detectors=True,
        )

    def add_cavities(self, model):
        if self.options.add_cavities.ARM:
            model.add(
                fc.Cavity(
                    f"cav{self.arm}ARM", model.get(self.ETM).fr.o, priority=100,
                )
            )


class ARMFactory(SingleArmFactory):
    def make(self):
        model = Model()
        self.pre_make(model)
        self.make_arm(model)
        if self.options.include_input:
            self.add_input(model)
        self.add_suspensions(model)
        self.add_LSC(model)
        if self.options.ASC.add:
            self.add_ASC(model)
        if self.options.add_cavities:
            self.add_cavities(model)
        if self.options.add_amplitude_detectors:
            self.add_amplitude_detectors(model)
        self.post_make(model)
        # we're strict energy conservationists here in Cosmic Explorer
        model.phase_config(zero_k00=False)
        return model

    def add_input(self, model):
        PSL(self.params.Input, model=model)
        model.connect(model.IFI.p3, self.ports.arm_refl)

    def pre_make(self, model):
        pass

    def post_make(self, model):
        pass
