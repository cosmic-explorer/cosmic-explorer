import numpy as np
from abc import ABC, abstractmethod

from finesse.components import Connector, LocalDegreeOfFreedom, NodeType, NodeDirection
from finesse.components.workspace import ConnectorWorkspace
from finesse.components.mechanical import get_mechanical_port
from finesse.parameter import bool_parameter
from finesse.utilities.misc import reduce_getattr

from ... import load_parameters


# Global for storing the QUAD pendulum data so we don't keep reloading it
QUAD_DATA = dict()

# naming conventions from most LIGO suspensions
ligo_omap = {
    ".disp.P": "_pitch",
    ".disp.Y": "_yaw",
    ".disp.L": "_z",
}
ligo_imap = {
    ".drive.P": "_F_pitch",
    ".drive.Y": "_F_yaw",
    ".drive.L": "_F_z",
    ".disp.P": "_pitch",
    ".disp.Y": "_yaw",
    ".disp.L": "_z",
}


def minreal(z, p, tol=1e-14):
    """Minimum realisation for some zeros and poles by comparing absolute error in real
    and imaginary parts of zeros and poles.

    Parameters
    ----------
    z : array_like
        Zeros
    p : array_like
        Poles
    tol : float
        Tolerance on comparison

    Returns
    -------
    Filtered (zeros, poles)

    Function from finesse-ligo
    """
    z_idx = np.zeros_like(z, dtype=bool)
    p_idx = np.zeros_like(p, dtype=bool)

    for i in range(len(z)):
        idx = np.isclose(z[i], p, atol=tol)
        p_idx = np.bitwise_or(p_idx, idx)

        if not any(idx):
            z_idx[i] = True

    return z[z_idx], p[~p_idx]


def get_zpk_plant(
        *extract_nodes, data_path, zpk_path, imap=ligo_imap,
        omap=ligo_omap,
):
    """
    Adapted from finesse-ligo
    """
    import h5py
    from copy import copy
    import pathlib
    import importlib

    data_path = pathlib.Path(data_path)
    if not data_path.exists():
        data_path = (
            importlib.resources.files("cosmicexplorer.parameters") / data_path
        )

    zpk_plant = {}
    allowed_nodes = set(extract_nodes)
    with h5py.File(data_path, mode="r") as f:
        for O in f[zpk_path].keys():
            o = copy(O)
            for a, b in omap.items():
                o = o.replace(a, b)
            for I in f[zpk_path][O].keys():
                Isplit = I.split(".")
                if Isplit[0] in allowed_nodes:
                    Itest = "." + ".".join(Isplit[1:])
                    if Itest in imap:
                        z = f[zpk_path][O][I]["z"][:]
                        p = f[zpk_path][O][I]["p"][:]
                        k = f[zpk_path][O][I]["k"][()]
                        i = copy(I)
                        for a, b in imap.items():
                            i = i.replace(a, b)
                        # tight minreal tolerance to get rid of obvious
                        # zero-pole pairs
                        zpk_plant[(o, i)] = (*minreal(z, p, 1e-13), k)

    return zpk_plant


class MultiStageSuspensionWorkspace(ConnectorWorkspace):
    pass


@bool_parameter("enabled", "Whether the suspension is enabled or not")
class MultiStageSuspension(Connector, ABC):
    def __init__(self, name, connect_to, *, enabled=True):
        super().__init__(name)
        self.enabled = enabled
        self._build_ports()

        mech_port = get_mechanical_port(connect_to)
        # Add motion and force nodes to mech port.
        # Here we duplicate the already created mechanical
        # nodes in some other connector element
        self._add_port("L3", NodeType.MECHANICAL)
        self.L3._add_node("z", None, mech_port.z)
        self.L3._add_node("yaw", None, mech_port.yaw)
        self.L3._add_node("pitch", None, mech_port.pitch)
        self.L3._add_node("F_z", None, mech_port.F_z)
        self.L3._add_node("F_yaw", None, mech_port.F_yaw)
        self.L3._add_node("F_pitch", None, mech_port.F_pitch)
        # Penultimate mass port
        self._add_port("L2", NodeType.MECHANICAL)
        self.L2._add_node("F_z", NodeDirection.BIDIRECTIONAL)
        self.L2._add_node("F_yaw", NodeDirection.BIDIRECTIONAL)
        self.L2._add_node("F_pitch", NodeDirection.BIDIRECTIONAL)
        # Intermediate mass port
        self._add_port("L1", NodeType.MECHANICAL)
        self.L1._add_node("F_z", NodeDirection.BIDIRECTIONAL)
        self.L1._add_node("F_yaw", NodeDirection.BIDIRECTIONAL)
        self.L1._add_node("F_pitch", NodeDirection.BIDIRECTIONAL)
        # topmass
        self._add_port("M0", NodeType.MECHANICAL)
        self.M0._add_node("F_z", NodeDirection.BIDIRECTIONAL)
        self.M0._add_node("F_yaw", NodeDirection.BIDIRECTIONAL)
        self.M0._add_node("F_pitch", NodeDirection.BIDIRECTIONAL)
        # Suspension point ground port
        self._add_port("gnd", NodeType.MECHANICAL)
        self.gnd._add_node("z", NodeDirection.BIDIRECTIONAL)
        self.gnd._add_node("yaw", NodeDirection.BIDIRECTIONAL)
        self.gnd._add_node("pitch", NodeDirection.BIDIRECTIONAL)

        self.load_zpk()
        self.zpks = []
        self._ois = []

        for (output, input), zpk in self.zpk_plant.items():
            i = reduce_getattr(self, input.replace("_", ".", 1))
            o = reduce_getattr(self, output.replace("_", ".", 1))
            self._ois.append((o, i))
            self._register_node_coupling(
                f"{input}_to_{output}",
                i,
                o,
            )
            self.zpks.append(zpk)  # store ordered zpks

        import types

        self.dofs = types.SimpleNamespace()
        self.dofs.L3_F_z = LocalDegreeOfFreedom(
            f"{self.name}.dofs.L3_F_z",
            mech_port.component.phi,
            self.L3.F_z,
            1,
            AC_OUT=self.L3.z,
        )
        self.dofs.L3_F_pitch = LocalDegreeOfFreedom(
            f"{self.name}.dofs.L3_F_pitch",
            mech_port.component.ybeta,
            self.L3.F_pitch,
            1,
            AC_OUT=self.L3.pitch,
        )
        self.dofs.L3_F_yaw = LocalDegreeOfFreedom(
            f"{self.name}.dofs.L3_F_yaw",
            mech_port.component.xbeta,
            self.L3.F_yaw,
            1,
            AC_OUT=self.L3.yaw,
        )
        self.dofs.L2_F_z = LocalDegreeOfFreedom(
            f"{self.name}.dofs.L2_F_z",
            mech_port.component.phi,
            self.L2.F_z,
            1,
            AC_OUT=self.L3.z,
        )
        self.dofs.L2_F_pitch = LocalDegreeOfFreedom(
            f"{self.name}.dofs.L2_F_pitch",
            mech_port.component.ybeta,
            self.L2.F_pitch,
            1,
            AC_OUT=self.L3.pitch,
        )
        self.dofs.L2_F_yaw = LocalDegreeOfFreedom(
            f"{self.name}.dofs.L2_F_yaw",
            mech_port.component.xbeta,
            self.L2.F_yaw,
            1,
            AC_OUT=self.L3.yaw,
        )

    @abstractmethod
    def _build_ports(self):
        pass

    @abstractmethod
    def load_zpk(self):
        pass

    def _get_workspace(self, sim):
        if sim.signal:
            if not self.enabled.is_changing and self.enabled is False:
                return None

            refill = sim.model.fsig.f.is_changing or self.enabled.is_changing
            ws = MultiStageSuspensionWorkspace(self, sim)
            ws.signal.add_fill_function(self.fill, refill)
            zpks_to_use = []

            for zpk, (output, input) in zip(self.zpks, self._ois):
                if (
                    output.full_name in sim.signal.nodes
                    and input.full_name in sim.signal.nodes
                ):
                    # from scipy.signal import zpk2sos
                    # zpk2sos(*zpk, pairing=None, analog=True)
                    # print(output, input)
                    zpks_to_use.append(zpk)

            ws.zpks = [
                (np.asarray(z), np.asarray(p), float(k)) for z, p, k in zpks_to_use
            ]

            return ws
        else:
            return None

    def fill(self, ws):
        import warnings

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")  # ignore NaN/inf as we check for it
            if ws.values.enabled:
                s = 2j * np.pi * ws.sim.model_settings.fsig
                for i, (z, p, k) in enumerate(ws.zpks):
                    H = k * np.prod(s - z) / np.prod(s - p)
                    if not np.isfinite(H):
                        H = 0

                    with ws.sim.signal.component_edge_fill3(
                        ws.owner_id,
                        i,
                        0,
                        0,
                    ) as mat:
                        mat[:] = H
            else:
                for i, _ in enumerate(ws.zpks):
                    with ws.sim.signal.component_edge_fill3(
                        ws.owner_id,
                        i,
                        0,
                        0,
                    ) as mat:
                        mat[:] = 0


@bool_parameter("enabled", "Whether the suspension is enabled or not")
class QUADSuspension(MultiStageSuspension):
    def __init__(
        self,
        name,
        connect_to,
        *,
        extract_nodes=["L3", "L2", "L1", "M0"],
        data_path="ce_quad_zpk.h5",
        zpk_path="zpk",
        imap=ligo_imap,
        omap=ligo_omap,
        enabled=True,
    ):
        self._extract_nodes = extract_nodes
        self._load_kwargs = dict(
            data_path=data_path, zpk_path=zpk_path, imap=ligo_imap,
            omap=ligo_omap,
        )
        super().__init__(name, connect_to, enabled=True)

    def _build_ports(self):
        pass

    def load_zpk(self):
        global QUAD_DATA
        data_path = self._load_kwargs["data_path"]
        zpk_plant = QUAD_DATA.get(data_path, None)
        if zpk_plant is None:
            print("loading")
            zpk_plant = get_zpk_plant(*self._extract_nodes, **self._load_kwargs)
            QUAD_DATA[data_path] = zpk_plant
        self.zpk_plant = zpk_plant
        # return get_zpk_plant(*self._extract_nodes, **self._load_kwargs)
