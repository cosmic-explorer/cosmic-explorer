import numpy as np

import finesse
import finesse.analysis.actions as fa
from finesse.symbols import Constant
from finesse.detectors import MathDetector
from wield.bunch import Bunch

from .. import components as fc
from .factory import IFOFactory, add_all_amplitude_detectors
from . import DRFPMI, PSL, IMCs, OutputOptics, FilterCavity


class CosmicExplorerFactory(IFOFactory):
    def reset(self):
        self.set_default_options()
        self.set_default_drives()
        self.set_default_LSC()
        self.set_default_ASC()
        self.ports = Bunch()

    def set_default_options(self):
        self.options = Bunch()
        self.options.add_FC = False
        self.options.add_amplitude_detectors = True
        self.options.include_ITMlens = False
        self.options.add_input = True
        self.options.add_output = True

        self.options.LSC = Bunch()
        self.options.LSC.add_BHD = True
        self.options.LSC.add_readouts = False
        self.options.LSC.add_output_detectors = True
        self.options.LSC.close_AC_loops = False

        self.options.ASC = Bunch()
        self.options.ASC.add = False
        self.options.ASC.add_readouts = True
        self.options.ASC.add_output_detectors = True
        self.options.ASC.close_AC_loops = False

        self.options.suspensions = Bunch()
        self.options.suspensions.testmass_model = None
        self.options.suspensions.testmass_options = Bunch()

        self.options.add_cavities = Bunch(
            XARM=True,
            YARM=True,
            PRX=True,
            PRY=True,
            SEX=True,
            SEY=True,
        )
        self.options.manual_rc_gouy_phase = False
        self.options.manual_arm_gouy_phase = False

    def set_default_drives(self, L_force=False, P_torque=True, Y_torque=True):
        """Resets factory to all default drives.

        Parameters
        ----------
        L_force : bool
            Whether the length drive should be a force (True) or a
            displacement (False). Default: False.
        P_torque : bool
            Whether the pitch drive should be a torque (True) or an
            angle (False). Default: True.
        Y_torque : bool
            Whether the yaw drive should be a torque (True) or an
            angle (False). Default: True.
        """
        Ltype = "F_" if L_force else ""
        Ptype = "F_" if P_torque else ""
        Ytype = "F_" if Y_torque else ""
        self.local_drives = Bunch()
        self.local_drives.L=Bunch(
            ETMX={f"ETMX.mech.{Ltype}z": 1},
            ITMX={f"ITMX.mech.{Ltype}z": 1},
            ETMY={f"ETMY.mech.{Ltype}z": 1},
            ITMY={f"ITMY.mech.{Ltype}z": 1},
            PRM={f"PRM.mech.{Ltype}z": 1},
            SEM={f"SEM.mech.{Ltype}z": 1},
            BS={f"BS.mech.{Ltype}z": 1},
        )
        self.local_drives.P = Bunch(
            ETMX={f"ETMX.mech.{Ptype}pitch": 1},
            ITMX={f"ITMX.mech.{Ptype}pitch": 1},
            ETMY={f"ETMY.mech.{Ptype}pitch": 1},
            ITMY={f"ITMY.mech.{Ptype}pitch": 1},
            PRM={f"PRM.mech.{Ptype}pitch": 1},
            SEM={f"SEM.mech.{Ptype}pitch": 1},
            BS={f"BS.mech.{Ptype}pitch": 1},
            MX2={f"MX2.mech.{Ptype}pitch": 1},
            MY2={f"MY2.mech.{Ptype}pitch": 1},
            PR2={f"PR2.mech.{Ptype}pitch": 1},
            PR3={f"PR3.mech.{Ptype}pitch": 1},
        )
        self.local_drives.Y = Bunch(
            ETMX={f"ETMX.mech.{Ytype}yaw": 1},
            ITMX={f"ITMX.mech.{Ytype}yaw": 1},
            ETMY={f"ETMY.mech.{Ytype}yaw": 1},
            ITMY={f"ITMY.mech.{Ytype}yaw": 1},
            PRM={f"PRM.mech.{Ytype}yaw": 1},
            SEM={f"SEM.mech.{Ytype}yaw": 1},
            BS={f"BS.mech.{Ytype}yaw": 1},
            MX2={f"MX2.mech.{Ytype}yaw": 1},
            MY2={f"MY2.mech.{Ytype}yaw": 1},
            PR2={f"PR2.mech.{Ytype}yaw": 1},
            PR3={f"PR3.mech.{Ytype}yaw": 1},
        )

    def set_default_LSC(self):
        self.LSC_output_matrix = Bunch(
            XARM=Bunch(),
            YARM=Bunch(),
            DARM=Bunch(),
            CARM=Bunch(),
            PRCL=Bunch(),
            SECL=Bunch(),
            MICH=Bunch(),
            MICH2=Bunch(),
        )

        # Note that if lx, ly, Lp, and Ls are the distances between the BS and ITMX, the
        # BS and ITMY, the PRC, and the SEC, respectively, then the MICH defined in this
        # output matrix is really driving lm = lx - ly = MICH as well as
        # Lp = -MICH and Ls = +MICH and that the MICH2 defined in this output matrix is
        # driving lm = MICH2 and no other degrees of freedom
        self.LSC_output_matrix.XARM.ETMX = +1
        self.LSC_output_matrix.YARM.ETMY = +1
        self.LSC_output_matrix.CARM.ETMX = +0.5
        self.LSC_output_matrix.CARM.ETMY = +0.5
        self.LSC_output_matrix.DARM.ETMX = +1
        self.LSC_output_matrix.DARM.ETMY = -1
        self.LSC_output_matrix.PRCL.PRM = +1
        self.LSC_output_matrix.SECL.SEM = +1
        self.LSC_output_matrix.MICH.BS = +np.sqrt(2)
        self.LSC_output_matrix.MICH2.BS = +np.sqrt(2)
        self.LSC_output_matrix.MICH2.PRM = -1
        self.LSC_output_matrix.MICH2.SEM = +1

        self.LSC_input_matrix = Bunch(
            {
                "XARM": Bunch(),
                "YARM": Bunch(),
                "CARM": Bunch(),
                "DARM": Bunch(),
                "PRCL": Bunch(),
                "SECL": Bunch(),
                "MICH": Bunch(),
                "MICH2": Bunch(),
            }
        )

        self.LSC_controller = Bunch()

    def set_default_ASC(self):
        """Resets factory to all default ASC settings."""
        # See https://dcc.ligo.org/LIGO-D2200425/public for a diagram of arm modes.
        self.ASC_output_matrix = Bunch()
        self.ASC_output_matrix.CHARD_P = Bunch(
            {"ITMX": -1, "ETMX": "+rx_P", "ITMY": -1, "ETMY": "+ry_P"}
        )
        self.ASC_output_matrix.DHARD_P = Bunch(
            {"ITMX": -1, "ETMX": "+rx_P", "ITMY": +1, "ETMY": "-ry_P"}
        )
        self.ASC_output_matrix.CSOFT_P = Bunch(
            {"ITMX": "+rx_P", "ETMX": +1, "ITMY": "+ry_P", "ETMY": +1}
        )
        self.ASC_output_matrix.DSOFT_P = Bunch(
            {"ITMX": "+rx_P", "ETMX": +1, "ITMY": "-ry_P", "ETMY": -1}
        )
        self.ASC_output_matrix.CHARD_Y = Bunch(
            {"ITMX": +1, "ETMX": "+rx_Y", "ITMY": -1, "ETMY": "-ry_Y"}
        )
        self.ASC_output_matrix.DHARD_Y = Bunch(
            {"ITMX": +1, "ETMX": "+rx_Y", "ITMY": +1, "ETMY": "+ry_Y"}
        )
        self.ASC_output_matrix.CSOFT_Y = Bunch(
            {"ITMX": "+rx_Y", "ETMX": -1, "ITMY": "-ry_Y", "ETMY": +1}
        )
        self.ASC_output_matrix.DSOFT_Y = Bunch(
            {"ITMX": "+rx_Y", "ETMX": -1, "ITMY": "+ry_Y", "ETMY": -1}
        )

        self.ASC_output_matrix.PRC1_P = Bunch({"PRM": +1})
        self.ASC_output_matrix.PRC1_Y = Bunch({"PRM": +1})
        self.ASC_output_matrix.SEC1_P = Bunch({"SEM": +1})
        self.ASC_output_matrix.SEC1_Y = Bunch({"SEM": +1})
        self.ASC_output_matrix.MICH_P = Bunch({"BS": +1})
        self.ASC_output_matrix.MICH_Y = Bunch({"BS": +1})

        self.ASC_output_matrix.CMC2_P = Bunch({"MX2": +1, "MY2": +1})
        self.ASC_output_matrix.CMC2_Y = Bunch({"MX2": +1, "MY2": +1})
        self.ASC_output_matrix.DMC2_P = Bunch({"MX2": +1, "MY2": -1})
        self.ASC_output_matrix.DMC2_Y = Bunch({"MX2": +1, "MY2": -1})

        self.ASC_input_matrix = Bunch(
            {
                "CHARD_P": Bunch(),
                "DHARD_P": Bunch(),
                "CSOFT_P": Bunch(),
                "DSOFT_P": Bunch(),
                "CHARD_Y": Bunch(),
                "DHARD_Y": Bunch(),
                "CSOFT_Y": Bunch(),
                "DSOFT_Y": Bunch(),
            }
        )

        self.ASC_controller = Bunch()

    def make(self):
        model = finesse.Model()
        self.pre_make(model)
        self.add_core_ifo(model)
        if self.options.add_input:
            self.add_input(model)
            model.connect(self.ports.IFI_OUT, model.PRM.bk)
        if self.options.add_output:
            self.add_output(model)
            model.connect(model.SEM.bk, self.ports.OFI_IFO_OUT)
        if self.options.add_FC:
            self.add_filtercavity(model)
            model.connect(self.ports.SQZ_OUT, self.ports.OFI_SQZ_IN)
        if self.options.add_amplitude_detectors:
            self.add_amplitude_detectors(model)
        self.add_suspensions(model)
        self.add_LSC(model)
        if self.options.ASC.add:
            self.add_ASC(model)
        if self.options.add_cavities:
            self.add_cavities(model)
        if self.options.manual_rc_gouy_phase:
            self.set_manual_rc_gouy_phase(model)
        if self.options.manual_arm_gouy_phase:
            self.set_manual_arm_gouy_phase(model)
        self.post_make(model)
        # we're strict energy conservationists here in Cosmic Explorer
        model.phase_config(zero_k00=False)
        return model

    def add_core_ifo(self, model):
        DRFPMI(
            params=self.params.IFO,
            model=model,
            include_ITMlens=self.options.include_ITMlens,
        )

    def add_input(self, model):
        PSL(self.params.Input, model=model)
        self.ports.IFI_OUT = model.IFI.p3
        self.ports.IFI_IFO_REFL = model.IFI.p4

    def add_output(self, model):
        OutputOptics(self.params, model=model)
        self.ports.OFI_IFO_OUT = model.OFI.p1
        self.ports.OFI_OUT = model.OFI.p3
        self.ports.OFI_SQZ_IN = model.OFI.p2

    def add_filtercavity(self, model):
        FilterCavity(self.params.SQZ, model=model)
        self.ports.SQZ_OUT = model.SFI.p4

    def add_cavities(self, model):
        if self.options.add_cavities.XARM:
            model.add(fc.Cavity("cavXARM", model.ETMX.fr.o, priority=100))
        if self.options.add_cavities.YARM:
            model.add(fc.Cavity("cavYARM", model.ETMY.fr.o, priority=100))
        if self.options.add_cavities.PRX:
            model.add(fc.Cavity("cavPRX", model.PRM.fr.o, via=model.ITMX.fr_sub.i))
        if self.options.add_cavities.PRY:
            model.add(fc.Cavity("cavPRY", model.PRM.fr.o, via=model.ITMY.fr_sub.i))
        if self.options.add_cavities.SEX:
            model.add(fc.Cavity("cavSEX", model.SEM.fr.o, via=model.ITMX.fr_sub.i, priority=-50))
        if self.options.add_cavities.SEY:
            model.add(fc.Cavity("cavSEY", model.SEM.fr.o, via=model.ITMY.fr_sub.i, priority=-50))

    def add_LSC(self, model):
        self.add_LSC_DOFs(model)
        io_added = self.options.add_input and self.options.add_output
        if io_added and self.options.LSC.add_readouts:
            self.add_LSC_readouts(model)
        if io_added and self.options.LSC.add_BHD:
            self.add_BHD(model)

    def add_ASC(self, model):
        self.add_geometric_factors(model, "X")
        self.add_geometric_factors(model, "Y")
        self.add_ASC_DOFs(model)
        add_readouts = (
            self.options.ASC.add_readouts
            and self.options.add_input
            and self.options.add_output
        )
        if add_readouts:
            self.add_ASC_readouts(model)

    def add_LSC_readouts(self, model):
        output_detectors = self.options.LSC.add_output_detectors
        f1 = model.Mod1.f
        f2 = model.Mod2.f

        def add_readout(name, port, fmod):
            model.add(fc.ReadoutRF(name, port, f=fmod, output_detectors=output_detectors))

        for fkey, fmod in zip(["f1", "f2"], [f1, f2]):
            add_readout(f"REFL_{fkey}", self.ports.IFI_IFO_REFL.o, fmod)
            add_readout(f"AS_{fkey}", self.ports.OFI_OUT.o, fmod)
            add_readout(f"POX_{fkey}", model.MX2.fr1.i, fmod)
            add_readout(f"POY_{fkey}", model.MY2.fr1.i, fmod)
            add_readout(f"POP_{fkey}", model.PR2.fr1.i, fmod)

        model.add(fc.ReadoutDC(
            "REFL_DC", self.ports.IFI_IFO_REFL.o, output_detectors=output_detectors,
        ))
        model.add(fc.ReadoutDC(
            "AS_DC", self.ports.OFI_OUT.o, output_detectors=output_detectors,
        ))

    def add_BHD(self, model):
        model.add(fc.BalancedHomodyneDetector("BHD", model.OFI.p3))

    def add_ASC_readouts(self, model):
        output_detectors = self.options.ASC.add_output_detectors
        f1 = model.Mod1.f
        f2 = model.Mod2.f

        def add_WFS(name, node):
            self.prepare_WFS_readout(model, node, f"{name}_WFS")
            for direction in ["x", "y"]:
                for AB in ["A", "B"]:
                    for fkey, fmod in zip(["f1", "f2"], [f1, f2]):
                        model.add(fc.ReadoutRF(
                            f"{name}_{AB}_{fkey}_{direction}",
                            model.get(f"{name}_WFS_{AB}").p1.i,
                            f=fmod,
                            pdtype=f"{direction}split",
                        ))

        add_WFS(f"REFL", self.ports.IFI_IFO_REFL)
        # add_WFS(f"AS_{a}_WFS_{fkey}", self.ports.OFI_OUT)
        add_WFS("POX", model.MX1.bk1)
        add_WFS("POY", model.MY1.bk1)

    def add_amplitude_detectors(self, model):
        if self.options.add_input:
            f1 = model.Mod1.f
            f2 = model.Mod2.f
            freqs = [(f1, "f1"), (f2, "f2")]
        else:
            freqs = []

        def add_amplitude_detectors(port, key):
            add_all_amplitude_detectors(model, port, key, *freqs, add_power_detectors=True)

        add_amplitude_detectors(model.PRM.fr.o, "PRC")
        add_amplitude_detectors(model.SEM.fr.o, "SEC")
        add_amplitude_detectors(model.PRM.bk.i, "IN")
        add_amplitude_detectors(model.ETMX.fr.o, "XARM")
        add_amplitude_detectors(model.ETMY.fr.o, "YARM")
        add_amplitude_detectors(model.ITMX.bk.i, "BSX")
        add_amplitude_detectors(model.ITMY.bk.i, "BSY")

        P_ARM = (Constant(model.P_c0_XARM) + Constant(model.P_c0_YARM)) / 2

        model.add(MathDetector(
            "cost_SECL",
            Constant(model.P_c0_SEC) + P_ARM,
        ))

    def add_suspensions(self, model):
        if self.options.suspensions.testmass_model is not None:
            kw = self.options.suspensions.testmass_options
            sus = self.options.suspensions.testmass_model
            for optic in ["ITMX", "ETMX", "ITMY", "ETMY"]:
                model.add(sus(f"{optic}_sus", model.get(f"{optic}.mech"), **kw))

    def set_manual_rc_gouy_phase(self, model):
        avg_prx, astig_prx = self._zero_cavity_gouy_phase(model.cavPRX)
        avg_pry, astig_pry = self._zero_cavity_gouy_phase(model.cavPRY)
        avg_sex, astig_sex = self._zero_cavity_gouy_phase(model.cavSEX)
        avg_sey, astig_sey = self._zero_cavity_gouy_phase(model.cavSEY)
        prc_space = model.PRM.fr.attached_to
        sec_space = model.SEM.fr.attached_to
        assert isinstance(prc_space, fc.Space)
        assert isinstance(sec_space, fc.Space)
        # divide average by 2 for default to be the same one-way
        self._set_space_gouy_phase(
            model,
            prc_space,
            "PRC_Gouy_deg",
            (avg_prx + avg_pry) / 4,
            (astig_prx + astig_pry) / 4,
        )
        self._set_space_gouy_phase(
            model,
            sec_space,
            "SEC_Gouy_deg",
            (avg_sex + avg_sey) / 4,
            (astig_sex + astig_sey) / 4,
        )

    def set_manual_arm_gouy_phase(self, model):
        avg_xarm, astig_xarm = self._zero_cavity_gouy_phase(model.cavXARM)
        avg_yarm, astig_yarm = self._zero_cavity_gouy_phase(model.cavYARM)
        xarm_space = model.ETMX.fr.attached_to
        yarm_space = model.ETMY.fr.attached_to
        assert isinstance(xarm_space, fc.Space)
        assert isinstance(yarm_space, fc.Space)
        # divide average by 2 for default to be the same one-way
        self._set_space_gouy_phase(
            model,
            xarm_space,
            "XARM_Gouy_deg",
            avg_xarm / 2,
            astig_xarm / 2,
        )
        self._set_space_gouy_phase(
            model,
            yarm_space,
            "YARM_Gouy_deg",
            avg_yarm / 2,
            astig_yarm / 2,
        )

    def pre_make(self, model):
        pass

    def post_make(self, model):
        with finesse.symbols.simplification(allow_flagged=True):
            # some optical path lengths
            ITMX_HR = model.ITMX.fr_sub.i
            ITMY_HR = model.ITMY.fr_sub.i
            lx_path = model.path(model.BS.fr1_sub.o, ITMX_HR, symbolic=True)
            ly_path = model.path(model.BS.fr2.o, ITMY_HR, symbolic=True)
            lpx_path = model.path(model.PRM.fr.o, ITMX_HR, symbolic=True)
            lpy_path = model.path(model.PRM.fr.o, ITMY_HR, symbolic=True)
            lsx_path = model.path(model.SEM.fr.o, ITMX_HR, symbolic=True)
            lsy_path = model.path(model.SEM.fr.o, ITMY_HR, symbolic=True)
            lp_path = model.path(model.PRM.fr.o, model.BS.fr1.i, symbolic=True)
            ls_path = model.path(model.SEM.fr.o, model.BS.fr2_sub.i, symbolic=True)

            lx = model.add(fc.Variable(
                "lx", lx_path.optical_length,
                description="Optical path length from BS HR to ITMX HR",
                units="m",
            ))
            ly = model.add(fc.Variable(
                "ly", ly_path.optical_length,
                description="Optical path length from BS HR to ITMY HR",
                units="m",
            ))
            model.add(fc.Variable(
                "l_schnupp", (lx - ly).collect(),
                description="Schnupp asymmetry (optical length)",
                units="m",
            ))
            model.add(fc.Variable(
                "L_PRC",
                ((lpx_path.optical_length + lpy_path.optical_length) / 2).collect(),
                description="Optical PRC length",
                units="m",
            ))
            model.add(fc.Variable(
                "L_SEC",
                ((lsy_path.optical_length + lsy_path.optical_length) / 2).collect(),
                description="Optical SEC length",
                units="m",
            ))
            model.add(fc.Variable(
                "L_PRC_physical",
                ((lpx_path.physical_length + lpy_path.physical_length) / 2).collect(),
                description="Physical PRC length",
                units="m",
            ))
            model.add(fc.Variable(
                "L_SEC_physical",
                ((lsy_path.physical_length + lsy_path.physical_length) / 2).collect(),
                description="Physical SEC length",
                units="m",
            ))
            lp = model.add(fc.Variable(
                "lp", lp_path.optical_length,
                description="Optical path length from PRM HR to BS HR",
                units="m",
            ))
            ls = model.add(fc.Variable(
                "ls", ls_path.optical_length,
                description="Optical path length from SEM HR to BS HR",
                units="m",
            ))


def InitialLock(pseudo_lock_arms=True):
    if pseudo_lock_arms:
        arm_locks = [
            fa.PseudoLockCavity("cavXARM", lowest_loss=False, feedback="XARM.DC"),
            fa.PseudoLockCavity("cavYARM", lowest_loss=False, feedback="YARM.DC"),
        ]
    else:
        arm_locks = [
            fa.Maximize("P_c0_XARM", "XARM.DC"),
            fa.Maximize("P_c0_YARM", "YARM.DC"),
        ]

    lock_action = fa.Series(
        fa.Change({"PRM.misaligned": True, "SEM.misaligned": True}),
        *arm_locks,
        fa.Change({"PRM.misaligned": False}),
        fa.Maximize("P_c0_PRC", "PRCL.DC"),
        fa.Change({"SEM.misaligned": False}),
        fa.Maximize("cost_SECL", "SECL.DC"),
        fa.Change({"SECL.DC": 90}, relative=True),
    )
    return lock_action
