from finesse import model
from wield.bunch import Bunch

import finesse.analysis.actions as fa

from .. import components as fc
from .coupledcavity import CoupledCavityFactory
from . import to_finesse_kwargs


class CARMFactory(CoupledCavityFactory):
    def set_default_LSC(self):
        super().set_default_LSC()
        self.LSC_output_matrix.CARM = Bunch({self.ETM: +1})

        self.LSC_input_matrix["CARM"] = Bunch()

    @property
    def RM(self):
        return "PRM"

    @property
    def cavity(self):
        return "PRC"

    def add_BS(self, model):
        model.add(
            fc.ThickBeamsplitter("BS", **to_finesse_kwargs(self.params.IFO.BS))
        )
        if self.arm == "X":
            model.BS.T = 1
        elif self.arm == "Y":
            model.BS.T = 0

    def add_recycling_cavity(self, model):
        lengths = self.params.IFO.Length_m
        model.add(fc.ThickMirror(
            "PRM", **to_finesse_kwargs(self.params.IFO.PRM)
        ))

        for bs in ["PR2", "PR3"]:
            model.add(fc.ThickBeamsplitter(
                bs, **to_finesse_kwargs(self.params.IFO[bs])
            ))
        model.connect(model.PRM.fr, model.PR2.fr1, lengths.PRM_PR2, name="PRM_PR2")
        model.connect(model.PR2.fr2, model.PR3.fr1, lengths.PR2_PR3, name="PR2_PR3")
        model.connect(model.PR3.fr2, model.BS.fr1, lengths.PR3_BS, name="PR3_BS")

    def add_io(self, model):
        model.add(fc.Laser("Laser", P=self.params.Input.Pin_W))
        model.connect(model.Laser.p1, model.PRM.bk)

    def add_cavities(self, model):
        super().add_cavities(model)
        if self.options.add_cavities.PRC:
            model.add(fc.Cavity(
                "cavPRC",
                model.PRM.fr.o,
                via=model.get(f"ITM{self.arm}").fr_sub.i,
            ))

    # def set_manual_rc_gouy_phase(self, model):
    #     self._zero_cavity_gouy_phase(model.cavPRC)
    #     # if "PRM_BS" in [c.name for c in model.components]:
    #     #     space = "PRM_BS"
    #     # else:
    #     #     space = "PRM_PR2"
    #     self._set_cavity_gouy_phase(model, space, "PRC_gouy", 20)


def InitialLock(pseudo_lock_arms=False):
    if pseudo_lock_arms:
        arm_locks = []
        raise NotImplementedError()
    else:
        arm_locks = [
            fa.Maximize("P_c0_ARM", "CARM.DC"),
        ]
    lock_action = fa.Series(
        fa.Change({"PRM.misaligned": True}),
        *arm_locks,
        fa.Change({"PRM.misaligned": False}),
        fa.Maximize("P_c0_PRC", "PRCL.DC"),
    )
    return lock_action
