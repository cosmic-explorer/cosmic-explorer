"""
Cosmic Explorer filter cavity
"""
import scipy.constants as scc

from finesse import Model

from .. import components as fc
from . import to_finesse_kwargs


def FilterCavity(params, *, model=None):
    if model is None:
        model = Model()

    for mirror in ["FC1", "FC2"]:
        model.add(fc.ThickMirror(mirror, **to_finesse_kwargs(params[mirror])))
    model.connect(model.FC1.fr, model.FC2.fr, params.FC_length_m, name="L_FC")

    model.add(fc.FaradayIsolator("SFI"))
    model.connect(model.SFI.p3, model.FC1.bk)

    # set FC tuning
    FSR_Hz = scc.c / (2 * params.FC_length_m)
    dphi_deg = params.FC_detune_Hz / FSR_Hz * 180
    model.FC2.phi = dphi_deg

    return model
