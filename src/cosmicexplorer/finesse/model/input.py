"""
Cosmic Explorer laser and input optics
"""
from finesse import Model

from .. import components as fc


def PSL(params, *, model=None, modulation_order=1):
    if model is None:
        model = Model()

    # note that we do not want to make symbolic references to the modulation frequencies or add
    # them as model parameters as is done in some LIGO models because there is no set
    # relationship between f1 and f2 (and maybe even more) yet
    model.add(fc.Laser("Laser", P=params.Pin_W))
    model.add(fc.Modulator(
        "Mod1", f=params.F1_Hz, midx=params.mod_depth1, order=modulation_order, mod_type="pm",
    ))
    model.add(fc.Modulator(
        "Mod2", f=params.F2_Hz, midx=params.mod_depth2, order=modulation_order, mod_type="pm",
    ))
    model.connect(model.Laser.p1, model.Mod1.p1)
    model.connect(model.Mod1.p2, model.Mod2.p1)

    # will probably want to refactor this eventually
    model.add(fc.FaradayIsolator("IFI"))
    model.connect(model.Mod2.p2, model.IFI.p1)

    return model


def IMCs(params, *, model=None):
    pass
