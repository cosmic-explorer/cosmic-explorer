import numpy as np
from abc import ABC, abstractmethod
from collections.abc import Mapping
from more_itertools import roundrobin

import finesse.detectors as fd
from finesse.symbols import Constant
from wield.bunch import Bunch

from .. import components as fc
from ... import load_parameters, update_mapping, VERTEX_TYPES


class FinesseFactory(ABC):
    def __init__(self, params):
        self.params = Bunch()
        self.update_parameters(params)
        self.set_default_options()
        self.reset()

    def update_parameters(self, params):
        if isinstance(params, Mapping):
            update_mapping(self.params, params)
        else:
            update_mapping(self.params, load_parameters(params))

    def update_options(self, options):
        update_mapping(self.options, options)

    @abstractmethod
    def reset(self):
        pass

    @abstractmethod
    def make(self):
        pass

    @abstractmethod
    def set_default_options(self):
        pass


class IFOFactory(FinesseFactory):
    def _add_DOFs(
            self,
            model,
            drives,
            output_matrix,
            dc_attr,
            ac_attr,
    ):
        """General function for adding degrees of freedom.

        Parameters
        ----------
        model : finesse model
        drives : Bunch
          The local drives for these degrees of freedom
        output_matrix : Bunch
          The output matrix for these degrees of freedom
        dc_attr : str
          The DC attribute for these local degrees of freedom
        ac_attr : str
          The AC attribute for these local degress of freedom
 
        Condensed from a few finesse-ligo functions.
        """
        vars = {p.name: p.ref for p in model.parameters}
        drives = drives.copy()
        ldofs = {}
        for optic_name in drives.keys():
            # Setup local DOF for reading out optic motion but driving
            # whatever point has been requested
            ldofs[optic_name] = fc.LocalDegreeOfFreedom(
                f"{optic_name}.ldof.{ac_attr}",
                DC=model.get(f"{optic_name}.{dc_attr}"),
                AC_OUT=model.get(f"{optic_name}.mech.{ac_attr}"),
                # No AC IN connections, these are made later with the
                # input/output matrices and blending
                AC_IN=None,
            )

        for dof_name in output_matrix:
            # Add in DC drives for the DOFs
            dof_drives = [
                ldofs[k] for k, v in output_matrix[dof_name].items() if v != 0
            ]
            amplitudes = [
                eval(str(v), vars)
                for v in output_matrix[dof_name].values()
                if v != 0
            ]
            assert len(dof_drives) == len(amplitudes)
            model.add(fc.DegreeOfFreedom(dof_name, *roundrobin(dof_drives, amplitudes)))

    def _add_AC_loops(
        self, model, drives, input_matrix, output_matrix, controller, options
    ):
        """General function for adding AC loops and connecting drives

        Parameters
        ----------
        model : finesse model
        drives : Bunch
          The local drives for these degrees of freedom
        input_matrix : Bunch
          The output matrix for these degrees of freedom
        output_matrix : Bunch
          The output matrix for these degrees of freedom
        controller : Bunch
          Controllers for these degrees of freedom
        options : Bunch
          Options these degrees of freedom
 
        Adapted from finesse-ligo.
        """
        vars = {p.name: p.ref for p in model.parameters}

        def make_filter_name(name_to, name_fr, description):
            name = "___{:s}__{:s}__{:s}".format(
                name_to.replace(".", "_"),
                name_fr.replace(".", "_"),
                description,
            )
            return name

        # First we go and add any ZPK filters requested for driving the
        # suspension points
        for optic_name, sus_drives in drives.items():
            assert optic_name in model.elements
            for sus_node_name, v in sus_drives.items():
                sus_node = model.get(sus_node_name)
                if hasattr(v, "__len__"):
                    if len(v) != 4:
                        raise ValueError(
                            "Drives must either be a scalar or a "
                            "four tuple (filter_name, z, p, k), but "
                            f"{sus_node_name} has length {len(v)}"
                        )
                    name, z, p, k = v
                else:
                    name = make_filter_name(sus_node_name, optic_name, "drive_filter")
                    z = []
                    p = []
                    k = v
                ZPK = model.add(fc.ZPKFilter(name, z, p, k))
                model.connect(ZPK.p2.o, sus_node)

        # now connect the DOF drives to the local drives
        for dof_name in output_matrix:
            DOF = model.get(dof_name)
            for optic_name, amplitude in output_matrix[dof_name].items():
                amplitude = eval(str(amplitude), vars)
                sus_drives = drives[optic_name]
                for sus_node_name, v in sus_drives.items():
                    sus_node = model.get(sus_node_name)
                    if hasattr(v, "__len__"):
                        if len(v) != 4:
                            raise ValueError(
                                "Output matrix must either be a scalar or a "
                                "four tuple (filter_name, z, p, k), but "
                                f"{optic_name} has length {len(v)}"
                            )
                        name = v[0]
                    else:
                        name = make_filter_name(
                            sus_node_name, optic_name, "drive_filter"
                        )
                    ZPK = model.get(name)
                    model.connect(DOF.AC.i, ZPK.p1, gain=amplitude)

        added_controllers = {}
        for dof_name in controller:
            DOF = model.get(dof_name)
            name, z, p, k = controller[dof_name]
            ZPK = model.add(fc.ZPKFilter(name, z, p, k))
            model.connect(ZPK.p2.o, DOF.AC.i)
            added_controllers[dof_name] = ZPK.p1.i

        # now connect the DOF drives to the local drives
        for dof_name in input_matrix:
            DOF = model.get(dof_name)
            connect_to = added_controllers.get(dof_name, DOF.AC.i)

            for readout_comp_name, v in input_matrix[dof_name].items():
                node = model.get(readout_comp_name)
                # make the connection
                if hasattr(v, "__len__"):
                    if len(v) != 4:
                        raise ValueError(
                            "Input matrix must either be a scalar or a "
                            "four tuple (filter_name, z, p, k), but "
                            f"{readout_comp_name} has length {len(v)}"
                        )
                    name, z, p, k = v
                else:
                    z = []
                    p = []
                    k = v
                    name = make_filter_name(readout_comp_name, dof_name, "input_filter")
                ZPK = model.add(fc.ZPKFilter(name, z, p, k))
                model.connect(node, ZPK.p1)
                if options.close_AC_loops:
                    model.connect(ZPK.p2.o, connect_to)

    def add_LSC_DOFs(self, model):
        self._add_DOFs(
            model=model,
            drives=self.local_drives.L,
            output_matrix=self.LSC_output_matrix,
            dc_attr="phi",
            ac_attr="z",
        )
        self._add_AC_loops(
            model=model,
            drives=self.local_drives.L,
            input_matrix=self.LSC_input_matrix,
            output_matrix=self.LSC_output_matrix,
            controller=self.LSC_controller,
            options=self.options.LSC,
        )

    def add_geometric_factors(self, model, arm):
        if arm not in ["X", "Y"]:
            raise ValueError("Arm should either be be X or Y")
        Larm = model.get(f"L{arm}").L.ref

        def Rc(tm, dof):
            return model.get(f"{tm}.{'Rcx' if dof == 'Y' else 'Rcy'}").ref

        for dof in ["P", "Y"]:
            g_ETM = model.add_parameter(
                f"g_ETM{arm}_{dof}", 1 - Larm / Rc(f"ETM{arm}", dof)
            ).ref
            g_ITM = model.add_parameter(
                f"g_ITM{arm}_{dof}", 1 - Larm / Rc(f"ITM{arm}", dof)
            ).ref
            r = 2 / ((g_ITM - g_ETM) + np.sqrt((g_ETM - g_ITM)**2 + 4))
            model.add_parameter(f"r{arm.lower()}_{dof}", r)

    def add_ASC_DOFs(self, model):
        def dof_filt(drives, dof):
            return dict(filter(lambda x: x[0].endswith(f"_{dof}"), drives.items()))

        self._add_DOFs(
            model=model,
            drives=self.local_drives.P,
            output_matrix=dof_filt(self.ASC_output_matrix, "P"),
            dc_attr="ybeta",
            ac_attr="pitch",
        )
        self._add_DOFs(
            model=model,
            drives=self.local_drives.Y,
            output_matrix=dof_filt(self.ASC_output_matrix, "Y"),
            dc_attr="xbeta",
            ac_attr="yaw",
        )
        self._add_AC_loops(
            model=model,
            drives=self.local_drives.P,
            input_matrix=dof_filt(self.ASC_input_matrix, "P"),
            output_matrix=dof_filt(self.ASC_output_matrix, "P"),
            controller=dof_filt(self.ASC_controller, "P"),
            options=self.options.ASC,
        )
        self._add_AC_loops(
            model=model,
            drives=self.local_drives.Y,
            input_matrix=dof_filt(self.ASC_input_matrix, "Y"),
            output_matrix=dof_filt(self.ASC_output_matrix, "Y"),
            controller=dof_filt(self.ASC_controller, "Y"),
            options=self.options.ASC,
        )

    @property
    def vertex_type(self):
        raise DeprecationWarning("vertex_type is no longer relevant")

    @vertex_type.setter
    def vertex_type(self, val):
        raise DeprecationWarning("vertex_type is no longer relevant")

    @staticmethod
    def _zero_cavity_gouy_phase(cavity):
        return zero_cavity_gouy_phase(cavity=cavity)

    @staticmethod
    def _set_space_gouy_phase(
            model,
            space,
            key,
            default_deg,
            default_astig_deg=0,
    ):
        set_space_gouy_phase(
            model=model,
            space=space,
            key=key,
            default_deg=default_deg,
            default_astig_deg=default_astig_deg,
        )

    @staticmethod
    def add_all_amplitude_detectors(
            model, port, key, *freqs, add_power_detectors=False,
    ):
        add_all_amplitude_detectors(
            model, port, key, *freqs, add_power_detectors=False,
        )

    @staticmethod
    def prepare_WFS_readout(model, node, name):
        prepare_WFS_readout(model, node, name)


def zero_cavity_gouy_phase(cavity):
    """Return the Gouy phase of a cavity and then zero all of the user_gouy

    Note that the round-trip Gouy phase is returned

    Parameters
    ----------
    cavity : Cavity
      The cavity that should be zeroed

    Returns
    -------
    avg_gouy_deg : float
      Average round-trip Gouy phase [deg]
    astig_deg : float
      Astigmatism [deg]
    """
    avg_gouy_deg = np.average(cavity.gouy)
    astig_deg = -np.diff(cavity.gouy)[0]
    for space in cavity.path.spaces:
        space.user_gouy_x = 0
        space.user_gouy_y = 0
    return avg_gouy_deg, astig_deg


def set_space_gouy_phase(
        model,
        space,
        key,
        default_deg,
        default_astig_deg=0,
):
    """Manually set the one-way Gouy phase of a space

    The Gouy phase of the space is overridden by setting the user_gouy and
    is controlled by two variables added to the model: one controlling the
    average Gouy phase and another the astigmatism.

    Parameters
    ----------
    model : finesse model
    space : Space
      The Gouy phase of this space is controlled
    key : str
      Name of the parameter which sets the Gouy phase. A variable 'key' will
      be added to set the average Gouy phase and a variable 'key_astig' will
      be added to set the astigmatism.
    default_deg : float
      Default value of the Gouy phase [deg]
    defualt_astig_deg : float
      Default value of the astigmatism [deg]
    """
    gouy = model.add_parameter(key, default_deg).ref
    gouy_astig = model.add_parameter(f"{key}_astig", default_astig_deg).ref
    space.user_gouy_x = gouy + gouy_astig / 2
    space.user_gouy_y = gouy - gouy_astig / 2


def add_all_amplitude_detectors(model, port, key, *freqs, add_power_detectors=False):
    """Add amplitude detectors for the carrier and specified sidebands to a node.

    FIXME: this should allow specification of carrier HOMs as well, but that doesn't easily
    comport with the current naming conventions used in the existing aLIGO model

    Parameters
    ----------
    model : :class:`finesse.Model`
        Model to add the detectors to
    port : str, :class:`Node`
        Node to probe
    key : str
        kdkd
    *freqs : list of (frequency reference, str) tuples or (frequency reference, str, int, int)
    tuples, optional
        If a tuple (f, key), list of frequencies to probe along with the detector name to add
        to the model. Both upper and lower sidebands are probed.
        If a tuple (f, key, n, m), list of frequencies and HOMs to probe along wit the detector
        name to add to the model
    add_power_detectors : bool, optional; default: False
        If True, adds a detector for the power of each amplituded detector added to the model

    Examples
    --------
    To probe the carrier and 9 and 45 MHz sidebands
    >>> freqs = [(9, "9"), (45, "45")]
    >>> _add_all_amplitude_detectors(model, model.PRM.p1.o, "PRC", *freqs)
    adds the amplitude detectors a_c0_PRC probing the carrier power in the PRC and
    a_u9_PRC, a_l9_PRC, a_u45_PRC, and a_l45_PRC probing the upper and lower 9 and 45 MHz
    sidebands in the PRC. If called with add_power_detectors=True, the detectors P_c0_PRC,
    P_u9_PRC, P_l9_PRC, ... would also be added probing the powers of those fields.

    To probe the carrier 9 and 45 MHz sidebands as well as the 45 MHz 00, 01, and 10 using
    as symbolic reference to frequencies
    >>> f1 = model.f1.ref
    >>> f2 = model.f2.ref
    >>> freqs = [
        (f1, "9"),
        (f2, "45"),
        (f2, "45", 0, 0),
        (f2, "45", 1, 0),
        (f2, "45", 0, 1),
    ]
    >>> _add_all_amplitude_detectors(model, model.PRM.p1.o, "PRC", *freqs)
    in addition to the above, a_u45_00_PRC, a_u45_10_PRC, a_l45_10_PRC, etc. will be added

    function taken from finesse-ligo factory
    """
    AD_c = Constant(model.add(fd.AmplitudeDetector(f"a_c0_{key}", port, f=0)))
    if add_power_detectors:
        model.add(fd.MathDetector(f"P_c0_{key}", np.abs(AD_c) ** 2))
    for det_options in freqs:
        if len(det_options) == 2:
            f, sb_key = det_options
            nm = dict(n=None, m=None)
            postfix = f"{sb_key}_{key}"
        elif len(det_options) == 4:
            f, sb_key, n, m = det_options
            nm = dict(n=n, m=m)
            postfix = f"{sb_key}_{n}{m}_{key}"
        else:
            raise finesse.exceptions.FinesseException(
                "Incorrect number of detector options"
            )
        AD_u = Constant(
            model.add(fd.AmplitudeDetector(f"a_u{postfix}", port, f=+f, **nm))
        )
        AD_l = Constant(
            model.add(fd.AmplitudeDetector(f"a_l{postfix}", port, f=-f, **nm))
        )
        if add_power_detectors:
            model.add(fd.MathDetector(f"P_u{postfix}", np.abs(AD_u) ** 2))
            model.add(fd.MathDetector(f"P_l{postfix}", np.abs(AD_l) ** 2))


def prepare_WFS_readout(model, node, name):
    gouy_phase = model.add_parameter(f"{name}_gouy_phase", 0).ref
    BS = model.add(fc.Beamsplitter(f"{name}_BS", T=0.5, L=0, alpha=0.5))
    nA = model.add(fc.Nothing(f"{name}_A"))
    nB = model.add(fc.Nothing(f"{name}_B"))
    sA = model.add(fc.Space(f"{name}_space_A", BS.fr2, nA.p1))
    sB = model.add(fc.Space(f"{name}_space_B", BS.bk1, nB.p1))
    model.connect(node, BS.fr1)
    sA.user_gouy_x = gouy_phase
    sA.user_gouy_y = gouy_phase
    sB.user_gouy_x = gouy_phase + 90
    sB.user_gouy_y = gouy_phase + 90
