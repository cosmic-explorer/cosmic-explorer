"""
"""

def to_finesse_kwargs(optic_params):
    keymap = dict(
        T="T", L="L", Rc_m="Rc", thickness_m="thickness", aoi_deg="alpha",
        nr="nr", Rc_AR_m="Rc_AR", wedge="wedge", wedge_angle_deg="wedge_angle",
    )
    par = {}
    for k, v in optic_params.items():
        try:
            par[keymap[k]] = v
        except KeyError:
            continue
    return par


from .drfpmi import DRFPMI
from .input import PSL, IMCs
from .output import OutputOptics
from .filtercavity import FilterCavity
from .cosmicexplorer import CosmicExplorerFactory, InitialLock
from .arm import ARMFactory
from .darm import DARMFactory
from .carm import CARMFactory
