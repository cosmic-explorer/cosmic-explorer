"""
Cosmic Explorer output optics and readout
"""
from finesse import Model

from .. import components as fc


def OutputOptics(params, *, model=None):
    if model is None:
        model = Model()

    model.add(fc.FaradayIsolator("OFI"))

    return model
