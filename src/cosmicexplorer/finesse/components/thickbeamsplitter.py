"""
Thick beamsplitter with HR and AR surfaces

FIXME: fix the RTL setter
"""
import numpy as np

from finesse.element import ModelElement
from finesse.components import Beamsplitter as _Beamsplitter
from finesse.components import Space as _Space
# from finesse.paths import OpticalPath
from finesse.parameter import float_parameter, bool_parameter
from finesse.freeze import Freezable

from . import _link_all_mechanical_dofs


@float_parameter("R", "HR reflectivity")
@float_parameter("T", "HR transmission")
@float_parameter("L", "HR loss")
@float_parameter("R_AR", "AR reflectivity")
@float_parameter("thickness", "Substrate thickness", units="m")
@float_parameter("nr", "Substrate refractive index")
@float_parameter(
    "phi", "Microscopic tuning (360 degrees = 1 default wavelength)", units="degrees"
)
@float_parameter(
    "alpha",
    "Angle of incidence (-90 <= alpha <= 90)",
    units="degrees",
    is_geometric=False,
    changeable_during_simulation=False,
)
@float_parameter(
    "Rcx",
    "Radius of curvature (x) of the HR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcy",
    "Radius of curvature (y) of the HR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcx_AR",
    "Radius of curvature (x) of the AR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcy_AR",
    "Radius of curvature (y) of the AR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter("xbeta", "Yaw misalignment", units="radians")
@float_parameter("ybeta", "Pitch misalignment", units="radians")
class ThickBeamsplitter(ModelElement):
    def __init__(
        self,
        name,
        R=0.5,
        T=0.5,
        L=0,
        R_AR=0,
        thickness=0,
        nr=1.45,
        phi=0,
        alpha=0,
        Rc=np.inf,
        Rc_AR=np.inf,
        xbeta=0,
        ybeta=0,
    ):
        super().__init__(name)
        # self.R.value = 1 - T - L
        self.T.value = T
        self.L.value = L
        self.R = 1 - self.T.ref - self.L.ref
        self.R_AR.value = R_AR
        self.thickness.value = thickness
        self.nr.value = nr
        self.phi.value = phi
        self.alpha.value = alpha
        self.Rc = Rc
        self.Rc_AR = Rc_AR
        self.xbeta.value = xbeta
        self.ybeta.value = ybeta
        self.components = Freezable()

    def _check_R(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Reflectivity must satisfy 0 <= R <= 1")

        return value

    def _check_T(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Transmissivity must satisfy 0 <= T <= 1")

        return value

    def _check_L(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Loss must satisfy 0 <= L <= 1")

        return value

    def _check_Rc(self, value):
        if value == 0:
            raise ValueError("Radius of curvature must be non-zero.")

        return value

    @property
    def Rc(self):
        return np.array([self.Rcx, self.Rcy])

    @Rc.setter
    def Rc(self, val):
        try:
            self.Rcx.value = val[0]
            self.Rcy.value = val[1]
        except (IndexError, TypeError):
            self.Rcx.value = val
            self.Rcy.value = val

    @property
    def Rc_AR(self):
        return np.array([self.Rcx_AR, self.Rcy_AR])

    @Rc_AR.setter
    def Rc_AR(self, val):
        try:
            self.Rcx_AR.value = val[0]
            self.Rcy_AR.value = val[1]
        except (IndexError, TypeError):
            self.Rcx_AR.value = val
            self.Rcy_AR.value = val

    @property
    def fr1(self):
        return getattr(self.components, f"{self.name}_HR").p1

    @property
    def fr2(self):
        return getattr(self.components, f"{self.name}_HR").p2

    @property
    def bk1(self):
        return getattr(self.components, f"{self.name}_AR1").p3

    @property
    def bk2(self):
        return getattr(self.components, f"{self.name}_AR2").p4

    @property
    def fr1_sub(self):
        return getattr(self.components, f"{self.name}_HR").p3

    @property
    def fr2_sub(self):
        return getattr(self.components, f"{self.name}_HR").p4

    @property
    def bk1_sub(self):
        return getattr(self.components, f"{self.name}_AR1").p1

    @property
    def bk2_sub(self):
        return getattr(self.components, f"{self.name}_AR2").p2

    @property
    def HR1(self):
        return self.fr1

    @property
    def HR2(self):
        return self.fr2

    @property
    def AR1(self):
        return self.bk1

    @property
    def AR2(self):
        return self.bk2

    @property
    def mech(self):
        return getattr(self.components, f"{self.name}_HR").mech

    @property
    def dofs(self):
        return getattr(self.components, f"{self.name}_HR").dofs

    @property
    def physical_thickness(self):
        """physical path length through the substrate"""
        return self._substrate_length.eval()

    @property
    def optical_thickness(self):
        """optical path length through the substrate"""
        return self.physical_thickness * self.nr

    @property
    def reduced_thickness(self):
        return self.physical_thickness / self.nr

    @property
    def p1(self):
        raise AttributeError("wtf is p1? did you mean fr1?")

    @property
    def p2(self):
        raise AttributeError("wtf is p2? did you mean fr2?")

    @property
    def p3(self):
        raise AttributeError("wtf is p3? did you mean bk1?")

    @property
    def p4(self):
        raise AttributeError("wtf is p4? did you mean bk2?")

    def _on_add(self, model):
        BS = _Beamsplitter(
            f"{self.name}_HR",
            R=self.R.ref,
            T=self.T.ref,
            L=self.L.ref,
            phi=self.phi.ref,
            alpha=self.alpha.ref,
            Rc=(self.Rcx.ref, self.Rcy.ref),
            xbeta=self.xbeta.ref,
            ybeta=self.ybeta.ref,
        )
        alpha_AR_rad = np.arcsin(1 / self.nr.ref * np.sin(np.deg2rad(BS.alpha.ref)))
        self._substrate_length = self.thickness.ref / np.cos(alpha_AR_rad)
        BS_AR1 = _Beamsplitter(
            f"{self.name}_AR1",
            R=0,
            T=1-self.R_AR.ref,
            L=self.R_AR.ref,
            phi=self.phi.ref,
            alpha=np.rad2deg(alpha_AR_rad),
            Rc=(self.Rcx_AR.ref, self.Rcy_AR.ref),
            xbeta=self.xbeta.ref,
            ybeta=self.ybeta.ref,
        )
        BS_AR2 = _Beamsplitter(
            f"{self.name}_AR2",
            R=0,
            T=1-self.R_AR.ref,
            L=self.R_AR.ref,
            phi=self.phi.ref,
            alpha=np.rad2deg(alpha_AR_rad),
            Rc=(self.Rcx_AR.ref, self.Rcy_AR.ref),
            xbeta=self.xbeta.ref,
            ybeta=self.ybeta.ref,
        )
        BS._namespace = (f".{self.name}.components", )
        BS_AR1._namespace = (f".{self.name}.components", )
        BS_AR2._namespace = (f".{self.name}.components", )
        model.add(BS, unremovable=True)
        model.add(BS_AR1, unremovable=True)
        model.add(BS_AR2, unremovable=True)

        substrate1 = _Space(
            f"{self.name}_substrate1",
            BS.p3,
            BS_AR1.p1,
            L=self._substrate_length,
            nr=self.nr.ref,
        )
        substrate2 = _Space(
            f"{self.name}_substrate2",
            BS.p4,
            BS_AR2.p2,
            L=self._substrate_length,
            nr=self.nr.ref,
        )
        model.add(substrate1, unremovable=True)
        model.add(substrate2, unremovable=True)
        # self._substrate1_path = OpticalPath([(BS.p3.o, BS_AR1)])
        # self._substrate2_path = OpticalPath([(BS.p4.o, BS_AR2)])
        _link_all_mechanical_dofs(BS, BS_AR1)
        _link_all_mechanical_dofs(BS, BS_AR2)
