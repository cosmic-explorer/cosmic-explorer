"""
Balanced homodyne detector
"""
from types import SimpleNamespace

from finesse.element import ModelElement
import finesse.components as _fc
from finesse.detectors import MathDetector
from finesse.symbols import Constant
from finesse.parameter import float_parameter
from finesse.freeze import Freezable


@float_parameter("Plo", "LO power", units="W")
@float_parameter("phase", "LO angle", units="deg")
class BalancedHomodyneDetector(ModelElement):
    def __init__(self, name, node, Plo=1, phase=0):
        super().__init__(name)
        self.Plo.value = Plo
        self.phase.value = phase
        self.components = Freezable()
        self.outputs = SimpleNamespace()
        self.outputs.DC = f"{self.name}_DC"
        self._node = node

    @property
    def node(self):
        return self._node

    @property
    def DC(self):
        return getattr(self.components, f"{self.name}_amp").p2

    def _on_add(self, model):
        BS = _fc.Beamsplitter(f"{self.name}_BS", T=0.5, L=0, alpha=0.5)
        LO = _fc.Laser(f"{self.name}_LO", P=self.Plo.ref, phase=self.phase.ref)
        BS._namespace = (f".{self.name}.components", )
        LO._namespace = (f".{self.name}.components", )
        model.add(BS, unremovable=True)
        model.add(LO, unremovable=True)
        model.connect(LO.p1, BS.p4)
        model.connect(self._node, BS.p1)

        BHD1 = _fc.ReadoutDC(f"{self.name}_BHD1", BS.p2.o, output_detectors=True)
        BHD2 = _fc.ReadoutDC(f"{self.name}_BHD2", BS.p3.o, output_detectors=True)
        BHD1._namespace = (f".{self.name}.components", )
        BHD2._namespace = (f".{self.name}.components", )
        model.add(BHD1, unremovable=True)
        model.add(BHD2, unremovable=True)

        amp = _fc.Amplifier(f"{self.name}_amp", gain=+1)
        amp._namespace = (f".{self.name}.components", )
        model.add(amp, unremovable=True)
        model.connect(BHD1.DC.o, amp.p1, gain=+1)
        model.connect(BHD2.DC.o, amp.p1, gain=-1)

        BHD1_DC = Constant(model.get(f"{self.name}_BHD1_DC"))
        BHD2_DC = Constant(model.get(f"{self.name}_BHD2_DC"))
        BHD_DC = MathDetector(f"{self.name}_DC", BHD1_DC - BHD2_DC)
        BHD_DC._namespace = (f".{self.name}.components", )
        model.add(BHD_DC, unremovable=True)
