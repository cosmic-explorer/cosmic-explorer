"""
Standard thin mirror with the port names fixed

FIXME: fix RTL setter
"""
import numpy as np

from finesse.element import ModelElement
from finesse.components import Mirror as _Mirror
from finesse.parameter import float_parameter, bool_parameter
from finesse.freeze import Freezable
from finesse.utilities.misc import calltracker


# @float_parameter("R", "Reflectivity", validate="_check_R", setter="set_RTL")
# @float_parameter("T", "Transmission", validate="_check_T", setter="set_RTL")
# @float_parameter("L", "Loss", validate="_check_L", setter="set_RTL")
@float_parameter("R", "Reflectivity")
@float_parameter("T", "Transmission")
@float_parameter("L", "Loss")
@float_parameter(
    "phi", "Microscopic tuning (360 degrees = 1 default wavelength)", units="degrees"
)
@float_parameter(
    "Rcx",
    "Radius of curvature (x)",
    units="m",
    validate="_check_Rc",
    is_geometric=False,  # Rcx here can't be geometric!
)
@float_parameter(
    "Rcy",
    "Radius of curvature (y)",
    units="m",
    validate="_check_Rc",
    is_geometric=False,  # Rcy here can't be geometric!
)
@float_parameter("xbeta", "Yaw misalignment", units="radians")
@float_parameter("ybeta", "Pitch misalignment", units="radians")
@bool_parameter("misaligned", "Misaligns mirror reflection (R=0 when True)")
class Mirror(ModelElement):
    def __init__(
            self,
            name,
            R=1,
            T=0,
            L=0,
            phi=0,
            Rc=np.inf,
            xbeta=0,
            ybeta=0,
            misaligned=False,
    ):
        super().__init__(name)
        # self.R.value = 1 - T - L
        self.T.value = T
        self.L.value = L
        self.R = 1 - self.T.ref - self.L.ref
        # self.set_RTL(R, T, L)
        self.phi.value = phi
        self.Rc = Rc  # don't set value here because it's done in the setter
        self.xbeta.value = xbeta
        self.ybeta.value = ybeta
        self.misaligned.value = misaligned
        self.components = Freezable()

    # @calltracker
    # def set_RTL(self, R=None, T=None, L=None):
    #     # FIXME: this should be smarter
    #     self.R.value = R
    #     self.T.value = T
    #     self.L.value = L

    def _check_R(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Reflectivity must satisfy 0 <= R <= 1")

        return value

    def _check_T(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Transmissivity must satisfy 0 <= T <= 1")

        return value

    def _check_L(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Loss must satisfy 0 <= L <= 1")

        return value

    def _check_Rc(self, value):
        if value == 0:
            raise ValueError("Radius of curvature must be non-zero.")

        return value

    @property
    def Rc(self):
        return np.array([self.Rcx, self.Rcy])

    @Rc.setter
    def Rc(self, val):
        try:
            self.Rcx.value = val[0]
            self.Rcy.value = val[1]
        except (IndexError, TypeError):
            self.Rcx.value = val
            self.Rcy.value = val

    @property
    def fr(self):
        return getattr(self.components, f"_{self.name}").p1

    @property
    def bk(self):
        return getattr(self.components, f"_{self.name}").p2

    @property
    def mech(self):
        return getattr(self.components, f"_{self.name}").mech

    @property
    def dofs(self):
        return getattr(self.components, f"_{self.name}").dofs

    @property
    def p1(self):
        raise AttributeError("wtf is p1? did you mean fr?")

    @property
    def p2(self):
        raise AttributeError("wtf is p2? did you mean bk?")

    def _on_add(self, model):
        mirror = _Mirror(
            f"_{self.name}",
            R=self.R.ref,
            T=self.T.ref,
            L=self.L.ref,
            phi=self.phi.ref,
            Rc=(self.Rcx.ref, self.Rcy.ref),
            # Rc=self.Rc.ref,
            xbeta=self.xbeta.ref,
            ybeta=self.ybeta.ref,
            misaligned=self.misaligned.ref,
        )
        mirror._namespace = (f".{self.name}.components", )
        model.add(mirror, unremovable=True)
