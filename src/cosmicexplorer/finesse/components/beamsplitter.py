"""
Standard thin beamsplitter with the port names fixed
"""
import numpy as np

from finesse.element import ModelElement
from finesse.components import Beamsplitter as _Beamsplitter
from finesse.parameter import float_parameter, bool_parameter
from finesse.freeze import Freezable
from finesse.utilities.misc import calltracker


# @float_parameter("R", "Reflectivity", validate="_check_R", setter="set_RTL")
# @float_parameter("T", "Transmission", validate="_check_T", setter="set_RTL")
# @float_parameter("L", "Loss", validate="_check_L", setter="set_RTL")
@float_parameter("R", "Reflectivity")
@float_parameter("T", "Transmission")
@float_parameter("L", "Loss")
@float_parameter("phi", "Phase", units="degrees")
@float_parameter(
    "alpha",
    "Angle of incidence (-90 <= alpha <= 90)",
    units="degrees",
    is_geometric=False,
    changeable_during_simulation=False,
)
@float_parameter(
    "Rcx",
    "Radius of curvature (x)",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcy",
    "Radius of curvature (y)",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter("xbeta", "Yaw misalignment", units="radians")
@float_parameter("ybeta", "Pitch misalignment", units="radians")
class Beamsplitter(ModelElement):
    def __init__(
        self, name, R=1, T=0, L=0, phi=0, alpha=0, Rc=np.inf, xbeta=0, ybeta=0
    ):
        super().__init__(name)
        # self.R.value = 1 - T - L
        self.T.value = T
        self.L.value = L
        self.R = 1 - self.T.ref - self.L.ref
        self.phi.value = phi
        self.alpha.value = alpha
        self.Rc = Rc  # don't set value here because it's done in the setter
        self.xbeta.value = xbeta
        self.ybeta.value = ybeta
        self.components = Freezable()

    # @calltracker
    # def set_RTL(self, R=None, T=None, L=None):
    #     # FIXME: this should be smarter
    #     self.R.value = R
    #     self.T.value = T
    #     self.L.value = L

    def _check_R(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Reflectivity must satisfy 0 <= R <= 1")

        return value

    def _check_T(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Transmissivity must satisfy 0 <= T <= 1")

        return value

    def _check_L(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Loss must satisfy 0 <= L <= 1")

        return value

    def _check_Rc(self, value):
        if value == 0:
            raise ValueError("Radius of curvature must be non-zero.")

        return value

    @property
    def Rc(self):
        return np.array([self.Rcx, self.Rcy])

    @Rc.setter
    def Rc(self, val):
        try:
            self.Rcx.value = val[0]
            self.Rcy.value = val[1]
        except (IndexError, TypeError):
            self.Rcx.value = val
            self.Rcy.value = val

    @property
    def fr1(self):
        return getattr(self.components, f"_{self.name}").p1

    @property
    def fr2(self):
        return getattr(self.components, f"_{self.name}").p2

    @property
    def bk1(self):
        return getattr(self.components, f"_{self.name}").p3

    @property
    def bk2(self):
        return getattr(self.components, f"_{self.name}").p4

    @property
    def mech(self):
        return getattr(self.components, f"_{self.name}").mech

    @property
    def dofs(self):
        return getattr(self.components, f"_{self.name}").dofs

    @property
    def p1(self):
        raise AttributeError("wtf is p1? did you mean fr1?")

    @property
    def p2(self):
        raise AttributeError("wtf is p2? did you mean fr2?")

    @property
    def p3(self):
        raise AttributeError("wtf is p3? did you mean bk1?")

    @property
    def p4(self):
        raise AttributeError("wtf is p4? did you mean bk2?")

    def _on_add(self, model):
        BS = _Beamsplitter(
            f"_{self.name}",
            R=self.R.ref,
            T=self.T.ref,
            L=self.L.ref,
            phi=self.phi.ref,
            alpha=self.alpha.ref,
            Rc=(self.Rcx.ref, self.Rcy.ref),
            xbeta=self.xbeta.ref,
            ybeta=self.ybeta.ref,
        )
        BS._namespace = (f".{self.name}.components", )
        model.add(BS, unremovable=True)
