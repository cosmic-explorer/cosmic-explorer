"""
Faraday isolator
"""
import numpy as np

from finesse.components.general import Connector, InteractionType
from finesse.components.node import NodeDirection, NodeType
from finesse.components.modal.workspace import KnmConnectorWorkspace
from finesse.utilities import refractive_index
from finesse.parameter import float_parameter


class FaradayIsolatorWorkspace(KnmConnectorWorkspace):
    pass


# AttributeError about no portA if geometric parameters included
@float_parameter(
    "isolation",
    "isolation",
    validate="_check_isolation",
    units="dB",
)
@float_parameter(
    "L31",
    "Loss from p1 to p3",
    validate="_check_loss",
)
@float_parameter(
    "L43",
    "Loss from p3 to p4",
    validate="_check_loss",
)
@float_parameter(
    "L24",
    "Loss from p4 to p2",
    validate="_check_loss",
)
@float_parameter(
    "L12",
    "Loss from p2 to p1",
    validate="_check_loss",
)
# @float_parameter(
#     "L",
#     "Length",
#     validate="_check_L",
#     is_geometric=True,
#     units="m",
# )
# @float_parameter(
#     "nr",
#     "Refractive index",
#     validate="_check_nr",
#     is_geometric=True,
#     changeable_during_simulation=False,
# )
class FaradayIsolator(Connector):
    """Faraday isolator with finite isolation and loss

    Signals flow in the forward direction:
    * from port 1 to port 3
    * from port 3 to port 4
    * from port 4 to port 2
    * from port 2 to port 1

    Signals flow in the backward direction:
    * from port 3 to port 1
    * from port 4 to port 3
    * from port 2 to port 4
    * from port 1 to port 2

    Parameters
    ----------
    name : str
        Name of component
    isolation : float
        Isolation in the backwards directions
    """

    def __init__(self, name, isolation=np.inf, L31=0, L43=0, L24=0, L12=0):
        super().__init__(name)
        self.isolation = isolation
        self.L31 = L31
        self.L43 = L43
        self.L24 = L24
        self.L12 = L12
        # self.L = L
        # self.nr = nr

        self._add_port("p1", NodeType.OPTICAL)
        self.p1._add_node("i", NodeDirection.INPUT)
        self.p1._add_node("o", NodeDirection.OUTPUT)

        self._add_port("p2", NodeType.OPTICAL)
        self.p2._add_node("i", NodeDirection.INPUT)
        self.p2._add_node("o", NodeDirection.OUTPUT)

        self._add_port("p3", NodeType.OPTICAL)
        self.p3._add_node("i", NodeDirection.INPUT)
        self.p3._add_node("o", NodeDirection.OUTPUT)

        self._add_port("p4", NodeType.OPTICAL)
        self.p4._add_node("i", NodeDirection.INPUT)
        self.p4._add_node("o", NodeDirection.OUTPUT)

        # forward couplings
        self._register_node_coupling(
            "P1i_P3o",
            self.p1.i,
            self.p3.o,
            interaction_type=InteractionType.TRANSMISSION,
        )
        self._register_node_coupling(
            "P3i_P4o",
            self.p3.i,
            self.p4.o,
            interaction_type=InteractionType.TRANSMISSION,
        )
        self._register_node_coupling(
            "P4i_P2o",
            self.p4.i,
            self.p2.o,
            interaction_type=InteractionType.TRANSMISSION,
        )
        self._register_node_coupling(
            "P2i_P1o",
            self.p2.i,
            self.p1.o,
            interaction_type=InteractionType.TRANSMISSION,
        )

        # backward couplings
        self._register_node_coupling(
            "P3i_P1o",
            self.p3.i,
            self.p1.o,
            interaction_type=InteractionType.TRANSMISSION,
        )
        self._register_node_coupling(
            "P4i_P3o",
            self.p4.i,
            self.p3.o,
            interaction_type=InteractionType.TRANSMISSION,
        )
        self._register_node_coupling(
            "P2i_P4o",
            self.p2.i,
            self.p4.o,
            interaction_type=InteractionType.TRANSMISSION,
        )
        self._register_node_coupling(
            "P1i_P2o",
            self.p1.i,
            self.p2.o,
            interaction_type=InteractionType.TRANSMISSION,
        )

    def _check_isolation(self, value):
        if value < 0:
            raise ValueError("Isolation cannot be negative")

        return value

    def _check_loss(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Loss must satisfy 0 <= L <= 1")

        return value

    # def _check_L(self, value):
    #     if value < 0:
    #         raise ValueError("Length cannot be negative")

    #     return value

    # def _check_nr(self, value):
    #     if value < 1:
    #         raise ValueError("Index of refraction must be >= 1")

    #     return value

    def _get_workspace(self, sim):
        ws = FaradayIsolatorWorkspace(self, sim)
        ws.I = np.eye(sim.model_settings.num_HOMs, dtype=np.complex128)
        ws.carrier.add_fill_function(self._fill_carrier, False)
        if sim.signal:
            ws.signal.add_fill_function(self._fill_signal, False)

        ws.nr1 = refractive_index(self.p1)
        ws.nr2 = refractive_index(self.p2)
        ws.nr3 = refractive_index(self.p3)
        ws.nr4 = refractive_index(self.p4)

        if sim.is_modal:
            ws.set_knm_info(
                "P1i_P3o", nr_from=ws.nr1, nr_to=ws.nr3, is_transmission=True
            )
            ws.set_knm_info(
                "P3i_P4o", nr_from=ws.nr3, nr_to=ws.nr4, is_transmission=True
            )
            ws.set_knm_info(
                "P4i_P2o", nr_from=ws.nr4, nr_to=ws.nr2, is_transmission=True
            )
            ws.set_knm_info(
                "P2i_P1o", nr_from=ws.nr2, nr_to=ws.nr1, is_transmission=True
            )
            ws.set_knm_info(
                "P3i_P1o", nr_from=ws.nr3, nr_to=ws.nr1, is_transmission=True
            )
            ws.set_knm_info(
                "P4i_P3o", nr_from=ws.nr4, nr_to=ws.nr3, is_transmission=True
            )
            ws.set_knm_info(
                "P2i_P4o", nr_from=ws.nr2, nr_to=ws.nr4, is_transmission=True
            )
            ws.set_knm_info(
                "P1i_P2o", nr_from=ws.nr1, nr_to=ws.nr2, is_transmission=True
            )

        return ws

    def _fill_carrier(self, ws):
        self._fill_optical_matrix(ws, ws.sim.carrier, ws.carrier.connections)

    def _fill_signal(self, ws):
        self._fill_optical_matrix(ws, ws.sim.signal, ws.signal.connections)

    def _fill_optical_matrix(self, ws, mtx, connections):
        frwd = 1
        bkwd = 10**(-self.isolation / 20)
        trans_eff = np.sqrt(np.array(
            [1 - self.L31, 1 - self.L43, 1 - self.L24, 1 - self.L12]
        ))

        for freq in mtx.optical_frequencies.frequencies:
            # forward couplings
            for idx, K, eff in zip(
                (
                    connections.P1i_P3o_idx,
                    connections.P3i_P4o_idx,
                    connections.P4i_P2o_idx,
                    connections.P2i_P1o_idx,
                ),
                (ws.K13, ws.K34, ws.K42, ws.K21),
                trans_eff,
            ):
                with mtx.component_edge_fill3(
                    ws.owner_id,
                    idx,
                    freq.index,
                    freq.index,
                ) as mat:
                    mat[:] = frwd * K.data * eff

            # backward couplings
            for idx, K in zip(
                (
                    connections.P3i_P1o_idx,
                    connections.P4i_P3o_idx,
                    connections.P2i_P4o_idx,
                    connections.P1i_P2o_idx,
                ),
                (ws.K31, ws.K43, ws.K24, ws.K12),
            ):
                with mtx.component_edge_fill3(
                    ws.owner_id,
                    idx,
                    freq.index,
                    freq.index,
                ) as mat:
                    mat[:] = bkwd * K.data
