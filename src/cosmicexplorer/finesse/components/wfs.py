"""
Wavefront sensor
"""
from types import SimpleNamespace

from finesse.element import ModelElement
import finesse.components as _fc
from finesse.parameter import float_parameter
from finesse.freeze import Freezable


@float_parameter("f", "Frequency", units="Hz")
@float_parameter("demod_phase", "Demod phase", units="deg")
@float_parameter("gouy_phase", "Gouy phase", units="deg")
class WFS(ModelElement):
    def __init__(
            self,
            name,
            node,
            f,
            demod_phase=0,
            gouy_phase=0,
    ):
        super().__init__(name)
        self.f.value = f
        self.demod_phase.value = demod_phase
        self.gouy_phase.value = gouy_phase
        self._node = node

    @property
    def node(self):
        return self._node

    def _on_add(self, model):
        BS = _fc.Beamsplitter(f"{self.name}_BS", T=0.5, L=0, alpha=0.5)
        nA = _fc.Nothing(f"{self.name}_A")
        nB = _fc.Nothing(f"{self.name}_B")
        BS._namespace = (f".{self.name}.components", )
        nA._namespace = (f".{self.name}.components", )
        nB._namespace = (f".{self.name}.components", )
        model.add(BS, unremovable=True)
        model.add(nA, unremovable=True)
        model.add(nB, unremovable=True)
        sA = _fc.Space(f"{self.name}_space_A", BS.p2, nA.p1)
        sB = _fc.Space(f"{self.name}_space_B", BS.p3, nB.p1)
        model.add(sA, unremovable=True)
        model.add(sB, unremovable=True)
        model.connect(self.node, BS.p1)
        sA.user_gouy_x = self.gouy_phase.ref
        sA.user_gouy_y = self.gouy_phase.ref
        sB.user_gouy_x = self.gouy_phase.ref + 90
        sB.user_gouy_y = self.gouy_phase.ref + 90

        def add_wfs(AB, direction, optical_node):
            assert direction in ["x", "y"]
            WFS =  _fc.ReadoutRF(
                f"{self.name}_{AB}{direction}",
                optical_node=optical_node,
                f=self.f.ref,
                phase=self.demod_phase.ref,
                pdtype=f"{direction}split",
                output_detectors=True,
            )
            WFS._namespace = (f"{self.name}.components", )
            model.add(WFS, unremovable=True)

        add_wfs("A", "x", nA.p1.i)
        add_wfs("A", "y", nA.p1.i)
        add_wfs("B", "x", nB.p1.i)
        add_wfs("B", "y", nB.p1.i)
