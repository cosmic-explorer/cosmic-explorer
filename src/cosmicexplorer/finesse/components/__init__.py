"""
Import our own Mirror and Beamsplitter with the correct node names and then import all the other
standard finesse components
"""
from finesse.components import (
    Connector,
    FrequencyGenerator,
    LocalDegreeOfFreedom,
    Variable,
    Node,
    NodeType,
    NodeDirection,
    Port,
    Surface,
    Cavity,
    Gauss,
    DirectionalBeamsplitter,
    OpticalBandpassFilter,
    Isolator,
    Laser,
    Lens,
    Modulator,
    Nothing,
    FrequencyLoss,
    ReadoutDC,
    ReadoutRF,
    SignalGenerator,
    Space,
    Squeezer,
    Wire,
    Amplifier,
    Filter,
    ZPKFilter,
    ButterFilter,
    Cheby1Filter,
    DegreeOfFreedom,
    SuspensionTFPlant,
    FreeMass,
    Pendulum,
    SuspensionZPK,
    # TestPoint,
)

try:
    from finesse.components import TestPoint
except ImportError:
    pass
try:
    from finesse.components import AstigmaticLens
except ImportError:
    pass


def _link_all_mechanical_dofs(comp1, comp2):
    """Connects signal flow for the mechanical motion of component 1 to component 2.
    Doesn't create a bi-directional connection.

    Parameters
    ----------
    comp1, comp2 : ModelElement
        Components to connect

    Function taken from finesse-ligo factory
    """
    if comp1._model is not comp2._model:
        raise RuntimeError(f"{comp1} and {comp2} are not part of the same model")

    model = comp1._model

    if (comp1.p1.attached_to == comp2.p1.attached_to) or (
        comp1.p2.attached_to == comp2.p2.attached_to
    ):
        # Surfaces pointing towards each other
        z_gain = -1
        # Pitch sign flips when surfaces pointing towards each other
        pitch_gain = -1
    elif (comp1.p1.attached_to == comp2.p2.attached_to) or (
        comp1.p2.attached_to == comp2.p1.attached_to
    ):
        # Surfaces pointing towards each other
        z_gain = +1
        pitch_gain = +1
    else:
        raise NotImplementedError()

    model.connect(comp1.mech.z, comp2.mech.z, gain=z_gain)
    model.connect(comp1.mech.pitch, comp2.mech.pitch, gain=pitch_gain)
    model.connect(comp1.mech.yaw, comp2.mech.yaw)  # yaw_gain always == 1


# this needs to be imported after _link_all_mechanical_dofs to avoid circular imports
from .mirror import Mirror
from .beamsplitter import Beamsplitter
from .thickmirror import ThickMirror
from .thickbeamsplitter import ThickBeamsplitter
from .bhd import BalancedHomodyneDetector
from .mismatch_interface import MismatchInterface
from .faraday_isolator import FaradayIsolator
from .wfs import WFS
