"""
Thick mirror with HR and AR surfaces

FIXME: fix RTL setter
"""
import numpy as np

from finesse.element import ModelElement
from finesse.components import Mirror as _Mirror
from finesse.components import Space as _Space
from finesse.components import Beamsplitter as _Beamsplitter
# from finesse.paths import OpticalPath
from finesse.parameter import float_parameter, bool_parameter
from finesse.freeze import Freezable

from . import _link_all_mechanical_dofs


@float_parameter("R", "HR reflectivity")
@float_parameter("T", "HR transmission")
@float_parameter("L", "HR loss")
@float_parameter("R_AR", "AR reflectivity")
@float_parameter("thickness", "Substrate thickness", units="m")
@float_parameter("nr", "Substrate refractive index")
@float_parameter(
    "phi", "Microscopic tuning (360 degrees = 1 default wavelength)", units="degrees"
)
@float_parameter(
    "Rcx",
    "Radius of curvature (x) of the HR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcy",
    "Radius of curvature (y) of the HR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcx_AR",
    "Radius of curvature (x) of the AR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter(
    "Rcy_AR",
    "Radius of curvature (y) of the AR surface",
    units="m",
    validate="_check_Rc",
    is_geometric=False,
)
@float_parameter("xbeta", "Yaw misalignment", units="radians")
@float_parameter("ybeta", "Pitch misalignment", units="radians")
@bool_parameter("misaligned", "Misaligns mirror reflection (R=0 when True)")
@float_parameter(
    "wedge_angle",
    "Wedge angle",
    units="degrees",
    is_geometric=False,
)
class ThickMirror(ModelElement):
    def __init__(
        self,
        name,
        R=None,
        T=None,
        L=None,
        R_AR=0,
        thickness=0,
        nr=1.45,
        phi=0,
        Rc=np.inf,
        Rc_AR=np.inf,
        xbeta=0,
        ybeta=0,
        misaligned=False,
        wedge=None,
        wedge_angle=0,
    ):
        super().__init__(name)
        # self.R.value = 1 - T - L
        self.T.value = T
        self.L.value = L
        self.R = 1 - self.T.ref - self.L.ref
        self.R_AR.value = R_AR
        self.thickness.value = thickness
        self.nr.value = nr
        self.phi.value = phi
        self.Rc = Rc
        self.Rc_AR = Rc_AR
        self.xbeta.value = xbeta
        self.ybeta.value = ybeta
        self.misaligned.value = misaligned
        self.components = Freezable()

        if wedge is not None:
            if wedge not in ["horiz", "vert"]:
                raise ValueError("wedge must either be horiz or vert")
            self.__wedge = wedge
        else:
            self.__wedge = None
        self.wedge_angle = wedge_angle

    def _check_R(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Reflectivity must satisfy 0 <= R <= 1")

        return value

    def _check_T(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Transmissivity must satisfy 0 <= T <= 1")

        return value

    def _check_L(self, value):
        if not 0 <= value <= 1:
            raise ValueError("Loss must satisfy 0 <= L <= 1")

        return value

    def _check_Rc(self, value):
        if value == 0:
            raise ValueError("Radius of curvature must be non-zero.")

        return value

    @property
    def Rc(self):
        return np.array([self.Rcx, self.Rcy])

    @Rc.setter
    def Rc(self, val):
        try:
            self.Rcx.value = val[0]
            self.Rcy.value = val[1]
        except (IndexError, TypeError):
            self.Rcx.value = val
            self.Rcy.value = val

    @property
    def Rc_AR(self):
        return np.array([self.Rcx_AR, self.Rcy_AR])

    @Rc_AR.setter
    def Rc_AR(self, val):
        try:
            self.Rcx_AR.value = val[0]
            self.Rcy_AR.value = val[1]
        except (IndexError, TypeError):
            self.Rcx_AR.value = val
            self.Rcy_AR.value = val

    @property
    def fr(self):
        return getattr(self.components, f"{self.name}_front").p1

    @property
    def bk(self):
        if self.wedge:
            return getattr(self.components, f"{self.name}_back").p3
        else:
            return getattr(self.components, f"{self.name}_back").p2

    @property
    def fr_sub(self):
        return getattr(self.components, f"{self.name}_front").p2

    @property
    def bk_sub(self):
        return getattr(self.components, f"{self.name}_back").p1

    @property
    def pi(self):
        if self.wedge:
            return getattr(self.components, f"{self.name}_back").p4
        else:
            raise AttributeError("There are no pickoffs without a wedge")

    @property
    def po(self):
        if self.wedge:
            return getattr(self.components, f"{self.name}_back").p2
        else:
            raise AttributeError("There are no pickoffs without a wedge")

    @property
    def HR(self):
        return self.fr

    @property
    def AR(self):
        return self.bk

    @property
    def HR_sub(self):
        return self.fr_sub

    @property
    def AR_sub(self):
        return self.bk_sub

    @property
    def mech(self):
        return getattr(self.components, f"{self.name}_front").mech

    @property
    def dofs(self):
        return getattr(self.components, f"{self.name}_front").dofs

    @property
    def optical_thickness(self):
        """optical path length through the substrate"""
        # return self._substrate_path.optical_length.eval()
        return self.thickness * self.nr

    @property
    def reduced_thickness(self):
        return self.thickness / self.nr

    @property
    def fr_surface_map(self):
        return getattr(self.components, f"{self.name}_front").surface_map

    @fr_surface_map.setter
    def fr_surface_map(self, val):
        getattr(self.components, f"{self.name}_front").surface_map = val

    @property
    def bk_surface_map(self):
        return getattr(self.components, f"{self.name}_back").surface_map

    @fr_surface_map.setter
    def bk_surface_map(self, val):
        getattr(self.components, f"{self.name}_back").surface_map = val

    @property
    def wedge(self):
        return self.__wedge

    @property
    def p1(self):
        raise AttributeError("wtf is p1? did you mean fr?")

    @property
    def p2(self):
        raise AttributeError("wtf is p2? did you mean bk?")

    def _on_add(self, model):
        front = _Mirror(
            f"{self.name}_front",
            R=self.R.ref,
            T=self.T.ref,
            L=self.L.ref,
            phi=self.phi.ref,
            Rc=(self.Rcx.ref, self.Rcy.ref),
            xbeta=self.xbeta.ref,
            ybeta=self.ybeta.ref,
            misaligned=self.misaligned.ref,
        )
        if self.wedge is None:
            back = _Mirror(
                f"{self.name}_back",
                R=0,
                T=1 - self.R_AR.ref,
                L=self.R_AR.ref,
                phi=self.phi.ref,
                Rc=(self.Rcx_AR.ref, self.Rcy_AR.ref),
                xbeta=self.xbeta.ref,
                ybeta=self.ybeta.ref,
                misaligned=self.misaligned.ref,
            )
        else:
            back = _Beamsplitter(
                f"{self.name}_back",
                R=0,
                T=1 - self.R_AR.ref,
                L=self.R_AR.ref,
                phi=self.phi.ref,
                alpha=self.wedge_angle.ref,
                Rc=(self.Rcx_AR.ref, self.Rcy_AR.ref),
                xbeta=self.xbeta.ref,
                ybeta=self.ybeta.ref,
                plane="xz" if self.wedge == "horiz" else "yz",
                misaligned=self.misaligned.ref,
            )
        front._namespace = (f".{self.name}.components", )
        back._namespace = (f".{self.name}.components", )
        model.add(front, unremovable=True)
        model.add(back, unremovable=True)

        substrate = _Space(
            f"{self.name}_substrate", back.p1, front.p2, L=self.thickness.ref,
            nr=self.nr.ref,
        )
        substrate._namespace = (".spaces", f".{self.name}.components")
        model.add(substrate, unremovable=True)
        # self._substrate_path = OpticalPath([(back.p1.o, substrate)])
        _link_all_mechanical_dofs(front, back)
