"""
Interface for setting a phenomenological GWINC-like mismatch
"""
import numpy as np
from finesse.components import Nothing
from finesse.parameter import float_parameter, int_parameter, bool_parameter
from finesse.components.modal.workspace import KnmConnectorWorkspace
from finesse.utilities import refractive_index


class MismatchInterfaceWorkspace(KnmConnectorWorkspace):
    pass


@float_parameter("loss", "Mismatch loss", units="", is_geometric=False)
@float_parameter("phase", "Mismatch phase", units="deg", is_geometric=False)
@int_parameter("n", "Tangential mode index", units="", is_geometric=False)
@int_parameter("m", "Sagittal mode idex", units="", is_geometric=False)
@bool_parameter("is_OPD", "Mismatch is OPD if True")
# @bool_parameter("do_mismatch", "Make the mismatch transformation if True")
class MismatchInterface(Nothing):
    def __init__(
            self,
            name,
            loss=0,
            phase=0,
            mismatch_mode=[0, 2],
            is_OPD=False,
    ):
        super().__init__(name)
        self.loss = loss
        self.phase = phase
        # self.n = n
        # self.m = m
        self.mismatch_mode = mismatch_mode
        self.is_OPD = is_OPD
        # self.do_mismatch = do_mismatch

    @property
    def mismatch_mode(self):
        return [self.n, self.m]

    @mismatch_mode.setter
    def mismatch_mode(self, mode_idx):
        self.n = mode_idx[0]
        self.m = mode_idx[1]

    @property
    def mismatch_index(self):
        return self._model.mode_index_map[self.n.value, self.m.value]

    def mismatch_matrix(self):
        # mode_idx = self._model.mode_index_map[self.n.value, self.m.value]
        mode_idx = self.mismatch_index
        phase_rad = self.phase * np.pi / 180
        e2p = np.exp(1j * phase_rad)
        MM = np.eye(self._model.Nhoms, dtype=complex)
        MM[0, 0] = np.sqrt(1 - self.loss.value)
        MM[mode_idx, mode_idx] = np.sqrt(1 - self.loss.value)
        MM[0, mode_idx] = -e2p * np.sqrt(self.loss.value)
        MM[mode_idx, 0] = e2p.conj() * np.sqrt(self.loss.value)
        return MM

    def _get_workspace(self, sim):
        from finesse.simulations.sparse.simulation import SparseMatrixSimulation

        if isinstance(sim, SparseMatrixSimulation):
            _, is_changing = self._eval_parameters()
            refill = sim.is_component_in_mismatch_couplings(self) or len(is_changing)

            ws = MismatchInterfaceWorkspace(self, sim)
            # This assumes that nr1/nr2 cannot change during a simulation
            ws.nr1 = refractive_index(self.p1)
            ws.nr2 = refractive_index(self.p2)

            ws.carrier.add_fill_function(self._fill_carrier, refill)
            ws.signal.add_fill_function(self._fill_signal, refill)

            if sim.is_modal:
                # Set the coupling matrix information
                # ABCDs are same in each direction
                ws.set_knm_info(
                    "P1i_P2o",
                    abcd_x=np.eye(2),
                    abcd_y=np.eye(2),
                    nr_from=ws.nr1,
                    nr_to=ws.nr2,
                    is_transmission=True,
                )
                ws.set_knm_info(
                    "P2i_P1o",
                    abcd_x=np.eye(2),
                    abcd_y=np.eye(2),
                    nr_from=ws.nr2,
                    nr_to=ws.nr1,
                    is_transmission=True,
                )
            return ws
        else:
            raise Exception(f"MismatchInterface does not handle a simulation of type {sim}")

    def _fill_optical_matrix(self, ws, matrix, connections):
        K12 = self.mismatch_matrix()
        if self.is_OPD.value:
            K21 = K12
        else:
            K21 = K12.conj().T

        for freq in matrix.optical_frequencies.frequencies:
            with matrix.component_edge_fill3(
                ws.owner_id,
                connections.P1i_P2o_idx,
                freq.index,
                freq.index,
            ) as mat:
                mat[:] = K12

            with matrix.component_edge_fill3(
                ws.owner_id,
                connections.P2i_P1o_idx,
                freq.index,
                freq.index,
            ) as mat:
                mat[:] = K21
