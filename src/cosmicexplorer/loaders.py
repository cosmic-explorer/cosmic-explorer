import os
import importlib
from collections.abc import Mapping

from wield.utilities.file_io import load
from wield.bunch import Bunch


def load_parameters(param_path, unset_delete=True):
    """
    Loads parameters from a parameter file into a Bunch.

    Allows inheritance through the +inherit keyword in a parameter file

    Parameters
    ----------
    param_path : str
      Path to file. If the path doesn't exist the file is assumed to be in the
      cosmicexplorer/parameters directory
    unset_delete : bool, default: True
      If True, fields in an inherited file will be deleted if the <unset> value
      is given

    Returns
    -------
    params : Bunch
      Parameter bunch
    """
    if os.path.exists(param_path):
        path = param_path
    else:
        path = importlib.resources.files("cosmicexplorer").joinpath(
            "parameters", param_path,
        )
        if path.is_file():
            path = str(path)
        else:
            raise FileNotFoundError(f"Could not find {param_path}")
    params = Bunch(load(path))
    inherit_path = params.pop("+inherit", None)
    if inherit_path is not None:
        head = os.path.split(path)[0]
        inherit_path = os.path.join(head, inherit_path)
        inherit_params = load_parameters(inherit_path, unset_delete=unset_delete)
        update_mapping(inherit_params, params, unset_delete=unset_delete)
        return inherit_params
    else:
        return params


def update_mapping(old, new, unset_delete=False):
    """
    Update a mapping

    Parameters
    ----------
    old : Mapping
      Mapping to be updated
    new : Mapping
      Mapping to update old with
    unset_delete : bool, default: False
      If True, any key with value <unset> will be deleted in old
    """
    for k, v in new.items():
        if unset_delete:
            if isinstance(v, str) and v == "<unset>":
                try:
                    del old[k]
                except KeyError:
                    pass
                continue
        if isinstance(v, Mapping):
            if k in old:
                update_mapping(old[k], v, unset_delete=unset_delete)
            else:
                old[k] = v
        else:
            old[k] = v
