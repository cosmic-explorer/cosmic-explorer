# Cosmic Explorer Models

Models for the Cosmic Explorer interferometer

## Installation

To install, clone this repository. Then in the project directory run
```
pip install -e .
```
