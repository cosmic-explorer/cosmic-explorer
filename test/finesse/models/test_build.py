import pytest

import cosmicexplorer.finesse.model as ce
from cosmicexplorer import load_parameters, VERTEX_TYPES
import finesse.analysis.actions as fa


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_build(vertex_type):
    params = load_parameters(f"{vertex_type}.yaml")
    model = ce.DRFPMI(params.IFO)
    ce.PSL(params.Input, model=model)
    ce.OutputOptics(params, model=model)
    ce.FilterCavity(params.SQZ, model=model)
    model.connect(model.IFI.p3, model.PRM.bk)
    model.connect(model.SEM.bk, model.OFI.p1)
    model.connect(model.SFI.p4, model.OFI.p2)


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_build_full_factory(vertex_type):
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    factory.options.LSC.add_readouts = True
    model = factory.make()
    lx = model.lx.eval()
    ly = model.ly.eval()
    ls = model.ls.eval()
    lp = model.lp.eval()
    l_plus = (lx + ly) / 2
    print("")
    print(vertex_type)
    print("Schnupp", model.l_schnupp.eval(), lx - ly)
    print("PRCL", model.L_PRC.eval(), lp + l_plus)
    print("physical", model.L_PRC_physical.eval())
    print("SECL", model.L_SEC.eval(), ls + l_plus)
    print("physical", model.L_SEC_physical.eval())
    print("lx", lx)
    print("ly", ly)
    print("lp", lp)
    print("ls", ls)


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_build_arm_factory(vertex_type):
    factory = ce.ARMFactory(f"{vertex_type}.yaml")
    factory.options.LSC.add = True
    factory.options.ASC.add = True
    model = factory.make()


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_build_darm_factory(vertex_type):
    factory = ce.DARMFactory(f"{vertex_type}.yaml")
    factory.options.LSC.add = True
    factory.options.ASC.add = True
    model = factory.make()
    print("")
    print(vertex_type)
    print("SECL", model.L_SEC.eval())
    print("physical", model.L_SEC_physical.eval())


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_build_carm_factory(vertex_type):
    factory = ce.CARMFactory(f"{vertex_type}.yaml")
    factory.options.LSC.add = True
    model = factory.make()
    print("")
    print(vertex_type)
    print("PRL", model.L_PRC.eval())
    print("physical", model.L_PRC_physical.eval())


TEST_OPTIONS = dict(
    opt0={},
    opt1=dict(add_input=False, add_output=False),
    opt2={
        "ASC": dict(add=True, add_readouts=True),
        "LSC": dict(add_readouts=True),
    },
)

@pytest.mark.parametrize("opt", TEST_OPTIONS.keys())
@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_make_and_lock(vertex_type, opt):
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    factory.update_options(TEST_OPTIONS[opt])
    model = factory.make()
    model.run(ce.InitialLock())


@pytest.mark.xfail(reason="need to fix the names")
@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_build_asc(vertex_type):
    factory = ce.cosmicexplorer.CosmicExplorerFactory(f"{vertex_type}.yaml")
    factory.options.ASC.add = True
    factory.options.ASC.add_DOFs = True
    model = factory.make()
    print("")
    print(model.g_ETMX_P.eval())
    print(model.rx_P.eval())


def T_build_suspension(tpath_join):
    from cosmicexplorer.finesse.model.suspension import QUADSuspension
    vertex_type = "crab"
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    factory.options.suspensions.testmass_model = QUADSuspension
    model = factory.make()


@pytest.mark.skip()
@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_mm(vertex_type, tpath_join, makegrid):
    # from finesse import BeamParam
    print("")
    factory = ce.cosmicexplorer.CosmicExplorerFactory(f"{vertex_type}.yaml")
    # for k in ["PRX", "PRY", "SEX", "SEY"]:
    #     factory.options.add_cavities[k] = False
    model = factory.make()
    print(model.mismatches_table())
    ps_sec = model.propagate_beam(model.ITMY.bk.o, model.SEM.fr.i)
    ps_prc = model.propagate_beam(model.ITMY.bk.o, model.PRM.fr.i)
    # q0 = BeamParam(w=0.12, Rc=30e3)
    # ps_sec = model.propagate_beam(model.ITMY.bk.o, model.SEM.fr.i, q_in=q0)
    # ps_src = model.propagate_beam(model.ITMY.bk.o, model.PRM.fr.i, q_in=q0)
    print(ps_sec.q(model.SEM.fr.i))
    print(ps_prc.q(model.PRM.fr.i))
    print(ps_sec)
    print(ps_prc)
    ps_opts = dict(
        radius_scale=1e3, radius_pref="m",
        defocus_scale=1e3, defocus_pref="m",
    )
    save_opts = dict(bbox_inches="tight", pad_inches=0.05)
    fig, ax = ps_sec.plot_wield(**ps_opts)
    fig.savefig(tpath_join("sec.pdf"), **save_opts)
    fig, ax = ps_prc.plot_wield(**ps_opts)
    fig.savefig(tpath_join("prc.pdf"), **save_opts)
