import numpy as np

import finesse.components as fc
import finesse.analysis.actions as fa
from cosmicexplorer.finesse.model import darm
from cosmicexplorer.qlib import to_2p, to_2p_vec
from wield.control.plotting import plotTF


def T_darm_factory_plant(tpath_join):
    print("")
    vertex_type = "jacknife"
    factory = darm.DARMFactory(f"{vertex_type}.yaml")
    factory.options.add_ITMlens = True
    factory.options.LSC.add = True
    model = factory.make()
    model.modes("off")
    fsig = model.fsig.f.ref
    F_Hz = np.geomspace(5, 10e3, 200)
    attrs = ["ETMX.T", "DARM.DC", "SECL.DC", "_lock_laser.P"]
    sol = model.run(
        fa.Series(
            darm.InitialLock(),
            fa.FrequencyResponse3(
                F_Hz,
                [
                    ("ETMX.fr.o", +fsig),
                    ("ETMX.bk.o", -fsig),
                ],
                [
                    ("SEM.bk.o", +fsig),
                    ("SEM.bk.o", -fsig),
                ],
                name="fresp3",
            ),
        )
    )

    fig = plotTF(F_Hz, sol["fresp3"].out[..., 0, 0, 0, 0])
    fig.savefig(tpath_join("fresp3_plant.pdf"))


def T_set_arm_power(tpath_join):
    print("")
    vertex_type = "crab"
    factory = darm.DARMFactory(f"{vertex_type}.yaml")
    factory.options.suspensions.testmass_model = fc.FreeMass
    factory.options.suspensions.testmass_options = dict(mass=320)
    model = factory.make()
    model.modes("even", maxtem=2)
    # model.modes("off")
    DC = factory.set_arm_power(model, 1.5e6)
    print(DC["ETMX_front.p1.o"].shape)
    print(DC["ETMX_front.p1.o"])
    F_Hz = np.geomspace(1, 5e3, 100)
    fsig = model.fsig.f.ref
    model.SECL.DC = 90
    sol = model.run(
        fa.Series(
            fa.FrequencyResponse3(
                F_Hz,
                [
                    ("SEM.bk.i", +fsig),
                    ("SEM.bk.i", -fsig),
                ],
                [
                    ("SEM.bk.o", +fsig),
                    ("SEM.bk.o", -fsig),
                    ("ETMX.fr.o", +fsig),
                    ("ETMX.fr.o", -fsig),
                ],
                carrier_solution=DC,
                name="fresp3",
            ),
            fa.FrequencyResponse4(
                F_Hz,
                ["ETMX.mech.z"],
                [
                    ("SEM.bk.o", +fsig),
                    ("SEM.bk.o", -fsig),
                ],
                carrier_solution=DC,
                name="fresp4",
            ),
        )
    )

    refl, _ = to_2p(sol["fresp3"].out[..., :2, :, :, :])
    trans_field, _ = to_2p(sol["fresp3"].out[..., 2:, :, :, :])
    trans_mech, _ = to_2p_vec(sol["fresp4"].out)
    fig = plotTF(F_Hz, refl[..., 0, 0], label="qq")
    plotTF(F_Hz, refl[..., 1, 1], *fig.axes, label="pp", ls="--")
    plotTF(F_Hz, refl[..., 1, 0], *fig.axes, label="pq")
    fig.axes[0].legend()
    fig.axes[0].set_title("IFO reflection")
    fig.savefig(tpath_join("refl.pdf"))

    fig = plotTF(F_Hz, trans_field[..., 0, 0], label="pp")
    plotTF(F_Hz, trans_field[..., 1, 1], *fig.axes, ls="--", label="qq")
    plotTF(F_Hz, trans_field[..., 1, 0], *fig.axes, label="pq")
    fig.axes[0].legend()
    fig.axes[0].set_title("ETM fields to SEM fields")
    fig.savefig(tpath_join("trans_field.pdf"))

    fig = plotTF(F_Hz, trans_mech[..., 1, 0])
    fig.axes[0].set_title("ETM mirror motion to SEM fields")
    fig.savefig(tpath_join("trans_mech.pdf"))
