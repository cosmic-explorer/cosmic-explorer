import numpy as np
from scipy.constants import c as c_light
import pytest

import finesse.analysis.actions as fa
from cosmicexplorer.finesse.model import carm
from cosmicexplorer.finesse import PortHelper
from wield.control.plotting import plotTF
from cosmicexplorer import VERTEX_TYPES


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_carm_factory_plant(vertex_type, tpath_join):
    print("")
    factory = carm.CARMFactory(f"{vertex_type}.yaml")
    factory.options.LSC.add = True
    model = factory.make()
    model.modes("off")
    fsig = model.fsig.f.ref
    F_Hz = np.geomspace(0.001, 100, 200)
    tp_to = PortHelper()
    tp_to["refl"] = "PRM.bk.o"
    sol = model.run(
        fa.Series(
            carm.InitialLock(),
            fa.FrequencyResponse4(
                F_Hz, ["CARM.AC.i"], tp_to(fsig), name="fresp",
            ),
        )
    )
    La_m = factory.params.IFO.Length_m.ARM
    Fa_Hz = c_light * model.get(factory.ITM).T.value / (8 * np.pi * La_m)
    rp = np.sqrt(1 - model.get(factory.RM).T.value)
    Fcc_Hz = (1 - rp) / (1 + rp) * Fa_Hz
    print(f"Arm pole {Fa_Hz:0.2f} Hz")
    print(f"CC pole {Fcc_Hz * 1e3:0.0f} mHz")
    fig = plotTF(F_Hz, sol["fresp"].out[..., 0, 0, 0])
    plotTF(F_Hz, sol["fresp"].out[..., 1, 0, 0], *fig.axes, ls="--")
    for ax in fig.axes:
        ax.axvline(Fcc_Hz, ls=":", c="xkcd:blood red")
    fig.savefig(tpath_join("CARM.pdf"))
