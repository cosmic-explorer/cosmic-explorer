from wield.bunch import Bunch
from cosmicexplorer.finesse.model import CosmicExplorerFactory


def T_update_parameters():
    factory = CosmicExplorerFactory("reverse_ligo.yaml")
    update_bunch = Bunch(a=Bunch(w=1, x=2))
    update_dict = dict(b=dict(y=3, z=4))
    factory.update_parameters(update_bunch)
    factory.update_parameters(update_dict)
    assert factory.params.a == update_bunch.a
    assert factory.params.b == update_dict["b"]
