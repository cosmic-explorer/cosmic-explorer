import numpy as np
import matplotlib.pyplot as plt
import pytest

import cosmicexplorer.finesse.model as ce
import cosmicexplorer.finesse.components as fc
import finesse.analysis.actions as fa
from wield.control.plotting import plotTF
from wield.bunch import Bunch
# from cosmicexplorer import VERTEX_TYPES

VERTEX_TYPES = [
    "crab",
    "jacknife",
]


def sum_hom_power(homs, vals):
    assert len(homs) == vals.shape[-1]
    total = {}
    for h_idx, hom in enumerate(homs):
        order = np.sum(hom)
        if order in total:
            total[order] += vals[..., h_idx]
        else:
            total[order] = vals[..., h_idx]
    return total


def assert_cavity_user_gouy(cavity, ctrl_space):
    for space in cavity.path.spaces:
        if space is ctrl_space:
            continue
        assert space.user_gouy_x == 0
        assert space.user_gouy_y == 0


factory_map = {
    "CARM": ce.carm.CARMFactory,
    "DARM": ce.darm.DARMFactory,
}


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
@pytest.mark.parametrize("cav", factory_map.keys())
def T_manual_gouy_coupled_cavity(cav, vertex_type):
    factory = factory_map[cav](f"{vertex_type}.yaml")
    if vertex_type == "jacknife":
        factory.options.include_ITMlens = True
    factory.options.manual_rc_gouy_phase = True
    factory.options.manual_arm_gouy_phase = True
    factory.options.zero_all_aoi = True
    model = factory.make()
    rc_cavity = model.get(f"cav{factory.cavity}")
    rc_space = model.get(factory.RM).fr.o.port.attached_to
    arm_cavity = model.get(f"cav{factory.arm}ARM")
    arm_space = model.get(factory.ETM).fr.o.port.attached_to

    def assert_gouy(cavity, space, key):
        avg_gouy = np.average(cavity.gouy)
        astig = -np.diff(cavity.gouy)[0]
        gx, gy = space.user_gouy_x, space.user_gouy_y
        assert gx.eval() == (avg_gouy + astig / 2) / 2
        assert gy.eval() == (avg_gouy - astig / 2) / 2
        model.get(f"{key}_Gouy_deg_astig").value = 0
        assert gx.eval() == avg_gouy / 2
        assert gy.eval() == avg_gouy / 2
        model.get(f"{key}_Gouy_deg").value = 20
        assert gx.eval() == 20
        assert gy.eval() == 20

    assert_cavity_user_gouy(rc_cavity, rc_space)
    assert_cavity_user_gouy(arm_cavity, arm_space)
    assert_gouy(rc_cavity, rc_space, factory.cavity)
    assert_gouy(arm_cavity, arm_space, "ARM")


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_manual_gouy_drfpmi(vertex_type):
    factory = ce.cosmicexplorer.CosmicExplorerFactory(f"{vertex_type}.yaml")
    if vertex_type == "jacknife":
        factory.options.include_ITMlens = True
    factory.options.manual_rc_gouy_phase = True
    factory.options.manual_arm_gouy_phase = True
    model = factory.make()
    sec_space = model.SEM.fr.attached_to
    prc_space = model.PRM.fr.attached_to
    xarm_space = model.ETMX.fr.attached_to
    yarm_space = model.ETMY.fr.attached_to
    gsx, gsy = sec_space.user_gouy_x, sec_space.user_gouy_y
    gpx, gpy = prc_space.user_gouy_x, prc_space.user_gouy_y
    gxarm_x, gxarm_y = xarm_space.user_gouy_x, xarm_space.user_gouy_y
    gyarm_x, gyarm_y = yarm_space.user_gouy_x, yarm_space.user_gouy_y

    assert_cavity_user_gouy(model.cavPRX, prc_space)
    assert_cavity_user_gouy(model.cavPRY, prc_space)
    assert_cavity_user_gouy(model.cavSEX, sec_space)
    assert_cavity_user_gouy(model.cavSEY, sec_space)
    assert_cavity_user_gouy(model.cavXARM, xarm_space)
    assert_cavity_user_gouy(model.cavYARM, yarm_space)
    sec_gouy = (model.cavSEX.gouy + model.cavSEY.gouy) / 2
    prc_gouy = (model.cavPRX.gouy + model.cavPRY.gouy) / 2
    sec_avg = np.average(sec_gouy) / 2
    prc_avg = np.average(prc_gouy) / 2
    sec_astig = -np.diff(sec_gouy)[0] / 2
    prc_astig = -np.diff(prc_gouy)[0] / 2
    xarm_avg = np.average(model.cavXARM.gouy) / 2
    yarm_avg = np.average(model.cavYARM.gouy) / 2
    xarm_astig = -np.diff(model.cavXARM.gouy)[0] / 2
    yarm_astig = -np.diff(model.cavYARM.gouy)[0] / 2

    def assert_gouy(
            s_avg, s_astig, p_avg, p_astig, x_avg, x_astig, y_avg, y_astig,
    ):
        np.testing.assert_allclose(gsx.eval(), (s_avg + s_astig / 2))
        np.testing.assert_allclose(gsy.eval(), (s_avg - s_astig / 2))
        np.testing.assert_allclose(gpx.eval(), (p_avg + p_astig / 2))
        np.testing.assert_allclose(gpy.eval(), (p_avg - p_astig / 2))
        np.testing.assert_allclose(gxarm_x.eval(), (x_avg + x_astig / 2))
        np.testing.assert_allclose(gxarm_y.eval(), (x_avg - x_astig / 2))
        np.testing.assert_allclose(gyarm_x.eval(), (y_avg + y_astig / 2))
        np.testing.assert_allclose(gyarm_y.eval(), (y_avg - y_astig / 2))

    assert_gouy(
        sec_avg, sec_astig, prc_avg, prc_astig, xarm_avg, xarm_astig,
        yarm_avg, yarm_astig,
    )
    model.SEC_Gouy_deg_astig = 0
    model.PRC_Gouy_deg_astig = 0
    model.XARM_Gouy_deg_astig = 0
    model.YARM_Gouy_deg_astig = 0
    assert_gouy(sec_avg, 0, prc_avg, 0, xarm_avg, 0, yarm_avg, 0)
    model.SEC_Gouy_deg = 20
    model.PRC_Gouy_deg = 30
    model.XARM_Gouy_deg = 40
    model.YARM_Gouy_deg = 50
    assert_gouy(20, 0, 30, 0, 40, 0, 50, 0)


def T_manual_prc_gouy(tpath_join, makegrid):
    print("")
    vertex_type = "crab"
    factory = ce.carm.CARMFactory(f"{vertex_type}.yaml")
    factory.options.manual_rc_gouy_phase = True
    model = factory.make()
    # model.modes("off")
    model.modes("even", maxtem=6)
    F_Hz = np.geomspace(0.001, 10, 10)
    fsig = model.fsig.f.ref
    # gouy_deg = np.array([0, 30, 45, 60, 90])
    # gouy_deg = np.array([30, 60, 80, 90])
    gouy_deg = np.linspace(0, 90, 500)
    print(model.PRC_Gouy_deg)
    model.PRC_Gouy_deg_astig = 0.5
    sol = model.run(
        fa.Series(
            ce.carm.InitialLock(),
            fa.Noxaxis(),
            fa.For(
                "PRC_Gouy_deg",
                gouy_deg,
                fa.Change({"PRCL.DC": 0}),
                ce.carm.InitialLock(),
                fa.DCFields(name="DC"),
                # fa.FrequencyResponse4(
                #     F_Hz, ["CARM.AC.i"], [("PRM.bk.o", +fsig)], name="fresp",
                # ),
                name="gouy_sweep",
            )
        )
    )
    print(sol["gouy_sweep", "DC"].fields.shape)
    prm_fr = model.PRM.fr.o.full_name
    idx = sol["gouy_sweep", "DC"].solutions[0].nodes.index(prm_fr)
    Pp_W = np.abs(sol["gouy_sweep", "DC"].fields[:, idx, 0, :])**2
    hom_power = sum_hom_power(model.homs, Pp_W)
    print(hom_power.keys())
    fig, ax = plt.subplots()
    print(Pp_W.shape)
    # for idx in range(model.Nhoms):
    #     ax.semilogy(gouy_deg, Pp_W[:, idx], label=model.hom_labels[idx])
    for k, v in hom_power.items():
        ax.semilogy(gouy_deg, v, label=k)
    ax.legend()
    makegrid(ax, gouy_deg)
    fig.savefig(tpath_join("PRC_power.pdf"))

    # fresp = sol["gouy_sweep", "fresp"].out[..., 0, 0, 0]
    # print(fresp.shape)
    # fig = plotTF(F_Hz, fresp[0])
    # for idx in range(1, model.Nhoms):
    #     plotTF(F_Hz, fresp[idx], *fig.axes)
    # # fig = plotTF(F_Hz, sol["fresp"].out[..., 0, 0, 0])
    # fig.savefig(tpath_join("CARM.pdf"))
