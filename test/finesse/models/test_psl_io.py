import ipdb
import numpy as np
import finesse.analysis.actions as fa
import matplotlib.pyplot as plt

import cosmicexplorer.finesse.model as ce
import cosmicexplorer.finesse.components as fc
from cosmicexplorer.finesse.model import input, CosmicExplorerFactory #inputoptics # InputOpticsFactory
from cosmicexplorer.finesse.model.inputoptics import InputOpticsFactory

from cosmicexplorer import load_parameters
from wield.bunch import Bunch
from finesse.analysis.actions import Xaxis

import scipy.constants as scc


def T_pmc_input(tpath_join):
    params = Bunch()
    params = load_parameters("crab_io.yaml")

    # model = input.PSL_IO(params)
    factory = InputOpticsFactory("crab_io.yaml")
    factory.options.LSC.add_readouts = True
    model = factory.make()
    # model = InputOpticsFactory(params)

    # Turn off modes because we don't yet have mode matching telescopes
    model.modes("off") # fixes bug?

    sol = model.run(fa.DCFields(name="dcfields"))

    # ipdb.set_trace()
    print()
    
    print("PMC INPUT POWER, carrier")
    print(np.abs(sol['PM1_HR.p4.i', 0])**2)
    print("PMC INPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol['PM1_HR.p4.i', 5])**2)
    print("PMC INPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol['PM1_HR.p4.i', 3])**2)
    print("PMC INPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol['PM1_HR.p4.i', 7])**2)
    print("PMC INPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol['PM1_HR.p4.i', 1])**2)
    # print("PMC CAV")
    # print(np.abs(sol["PM1_HR.p2.o"])**2)

    print("PMC REFL POWER, carrier")
    print(np.abs(sol[model.PM1.bk1.o, 0])**2)
    print("PMC REFL POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol[model.PM1.bk1.o, 5])**2)
    print("PMC REFL POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol[model.PM1.bk1.o, 3])**2)
    print("PMC REFL POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol[model.PM1.bk1.o, 7])**2)
    print("PMC REFL POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol[model.PM1.bk1.o, 1])**2)

    print("PMC TRANS POWER, carrier")
    print(np.abs(sol["PM2_HR.p3.o", 0])**2)
    print("PMC TRANS POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["PM2_HR.p3.o", 5])**2)
    print("PMC TRANS POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["PM2_HR.p3.o", 3])**2)
    print("PMC TRANS POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["PM2_HR.p3.o", 7])**2)
    print("PMC TRANS POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["PM2_HR.p3.o", 1])**2)

    print("PMC carrier gain")
    print(np.abs(sol["PM1_HR.p2.o",0,0][0])**2 / np.abs(sol["PM1_HR.p4.i",0,0][0])**2)

    print()

    print("IFI1 INPUT POWER, carrier")
    print(np.abs(sol["IFI1.p1.i", 0])**2)
    print("IFI1 INPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["IFI1.p1.i", 5])**2)
    print("IFI1 INPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["IFI1.p1.i", 3])**2)
    print("IFI1 INPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["IFI1.p1.i", 7])**2)
    print("IFI1 INPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["IFI1.p1.i", 1])**2)

    print("IFI1 OUTPUT POWER, carrier")
    print(np.abs(sol["IFI1.p3.o", 0])**2)
    print("IFI1 OUTPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["IFI1.p3.o", 5])**2)
    print("IFI1 OUTPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["IFI1.p3.o", 3])**2)
    print("IFI1 OUTPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["IFI1.p3.o", 7])**2)
    print("IFI1 OUTPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["IFI1.p3.o", 1])**2)
    # print("IFI1 RETURN POWER FROM IMC1")
    # print(np.abs(sol["IFI1.p4.o"])**2)
    # print("IFI1 RETURN POWER BACK TO PMC")
    # print(np.abs(sol["IFI1.p1.o"])**2)

    print()

    print("IMC1 INPUT POWER, carrier")
    print(np.abs(sol["MC1_back.p2.i", 0])**2)
    print("IMC1 INPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["MC1_back.p2.i", 5])**2)
    print("IMC1 INPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["MC1_back.p2.i", 3])**2)
    print("IMC1 INPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["MC1_back.p2.i", 7])**2)
    print("IMC1 INPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["MC1_back.p2.i", 1])**2)

    # print("IMC1 CAV")
    # print(np.abs(sol["MC1_front.p1.o"])**2)
    # print("IMC1 REFL")
    # print(np.abs(sol["MC1_back.p2.o"])**2)

    print("IMC1 TRANS POWER, carrier")
    print(np.abs(sol["MC2_back.p2.o", 0])**2)
    print("IMC1 TRANS POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["MC2_back.p2.o", 5])**2)
    print("IMC1 TRANS POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["MC2_back.p2.o", 3])**2)
    print("IMC1 TRANS POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["MC2_back.p2.o", 7])**2)
    print("IMC1 TRANS POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["MC2_back.p2.o", 1])**2)

    print("IMC1 carrier gain")
    print(np.abs(sol["MC1_front.p1.o",0,0][0])**2 / np.abs(sol["MC1_back.p2.i",0,0][0])**2)

    print()

    print("IFI2 INPUT POWER, carrier")
    print(np.abs(sol["IFI2.p1.i", 0])**2)
    print("IFI2 INPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["IFI2.p1.i", 5])**2)
    print("IFI2 INPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["IFI2.p1.i", 3])**2)
    print("IFI2 INPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["IFI2.p1.i", 7])**2)
    print("IFI2 INPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["IFI2.p1.i", 1])**2)

    print("IFI2 OUTPUT POWER, carrier")
    print(np.abs(sol["IFI2.p3.o", 0])**2)
    print("IFI2 OUTPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    print(np.abs(sol["IFI2.p3.o", 5])**2)
    print("IFI2 OUTPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    print(np.abs(sol["IFI2.p3.o", 3])**2)
    print("IFI2 OUTPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    print(np.abs(sol["IFI2.p3.o", 7])**2)
    print("IFI2 OUTPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["IFI2.p3.o", 1])**2)

    print()

    # print("IMC2 INPUT POWER, carrier")
    # print(np.abs(sol["MC3_back.p2.i", 0])**2)
    # print("IMC2 INPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol["MC3_back.p2.i", 5])**2)
    # print("IMC2 INPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol["MC3_back.p2.i", 3])**2)
    # print("IMC2 INPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol["MC3_back.p2.i", 7])**2)
    print("IMC2 INPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["MC3_back.p2.i", 1])**2)

    # print("IMC2 CAV")
    # print(np.abs(sol["MC3_front.p1.o"])**2)
    # print("IMC2 CAV POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    # print(np.abs(sol["MC3_front.p1.o", 1])**2)
    # print("IMC2 REFL")
    # print(np.abs(sol["MC3_back.p2.o"])**2)

    # print("IMC2 TRANS POWER, carrier")
    # print(np.abs(sol["MC4_back.p2.o", 0])**2)
    # print("IMC2 TRANS POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol["MC4_back.p2.o", 5])**2)
    # print("IMC2 TRANS POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol["MC4_back.p2.o", 3])**2)
    # print("IMC2 TRANS POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol["MC4_back.p2.o", 7])**2)
    print("IMC2 TRANS POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol["MC4_back.p2.o", 1])**2)

    # print("IMC2 carrier gain")
    # print(np.abs(sol["MC3_front.p1.o",0,0][0])**2 / np.abs(sol["MC3_back.p2.i",0,0][0])**2)

    # print()

    # print("IFI3 INPUT POWER, carrier")
    # print(np.abs(sol["IFI3.p1.i", 0])**2)
    # print("IFI3 INPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol["IFI3.p1.i", 5])**2)
    # print("IFI3 INPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol["IFI3.p1.i", 3])**2)
    # print("IFI3 INPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol["IFI3.p1.i", 7])**2)
    # print("IFI3 INPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    # print(np.abs(sol["IFI3.p1.i", 1])**2)

    # print("IFI3 REFL POWER, carrier")
    # print(np.abs(sol["IFI3.p4.o", 0])**2)
    # print("IFI3 REFL POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol["IFI3.p4.o", 5])**2)
    # print("IFI3 REFL POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol["IFI3.p4.o", 3])**2)
    # print("IFI3 REFL POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol["IFI3.p4.o", 7])**2)
    # print("IFI3 REFL POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    # print(np.abs(sol["IFI3.p4.o", 1])**2)

    # print("IFI3 OUTPUT POWER, carrier")
    # print(np.abs(sol["IFI3.p3.o", 0])**2)
    # print("IFI3 OUTPUT POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol["IFI3.p3.o", 5])**2)
    # print("IFI3 OUTPUT POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol["IFI3.p3.o", 3])**2)
    # print("IFI3 OUTPUT POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol["IFI3.p3.o", 7])**2)
    # print("IFI3 OUTPUT POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    # print(np.abs(sol["IFI3.p3.o", 1])**2)

    print()

    # print("PRC POWER, carrier")
    # print(np.abs(sol[model.PRM.fr.o, 0])**2)
    # print("PRC POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol[model.PRM.fr.o, 5])**2)
    # print("PRC POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol[model.PRM.fr.o, 3])**2)
    # print("PRC POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol[model.PRM.fr.o, 7])**2)
    print("PRC POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol[model.PRM.fr.o, 1])**2)
    print((np.abs(sol[model.PRM.fr.o, 1])**2)/(np.abs(sol[model.PRM.bk.i, 1])**2))


    print()

    # print("ARM CAV POWER, carrier")
    # print(np.abs(sol[model.ITMX.fr.o, 0])**2)
    # print("ARM CAV POWER, PMC Sideband: ", [sol.frequencies[5]])
    # print(np.abs(sol[model.ITMX.fr.o, 5])**2)
    # print("ARM CAV POWER, IMC1 Sideband: ", [sol.frequencies[3]])
    # print(np.abs(sol[model.ITMX.fr.o, 3])**2)
    # print("ARM CAV POWER, IMC2 Sideband: ", [sol.frequencies[7]])
    # print(np.abs(sol[model.ITMX.fr.o, 7])**2)
    print("ARM CAV POWER, Interferometer Sideband 1: ", [sol.frequencies[1]])
    print(np.abs(sol[model.ITMX.fr.o, 1])**2)

    
    # pmc_fsr = scc.c / (params.PMC.Length_m.PM1_PM2+params.PMC.Length_m.PM2_PM3+params.PMC.Length_m.PM3_PM1)
    pmc_fsr = model.cavPMC.FSR
    imc1_fsr = model.cavIMC1.FSR
    imc2_fsr = model.cavIMC2.FSR
    prc_fsr = model.cavPRC.FSR
    arm_fsr = model.cavXARM.FSR
    # print()
    # print("PMC FSR")
    # print(pmc_fsr)
    # print()

    # print("IMC1 FSR")
    # print(imc1_fsr)
    # print()

    # print("IMC2 FSR")
    # print(imc2_fsr)
    # print()

    # print("PRC FSR")
    # print(prc_fsr)
    # print()

    # print("ARM FSR")
    # print(arm_fsr)
    # print()

    # ipdb.set_trace()

    # freq_min = -pmc_fsr
    # freq_max = pmc_fsr
    # nsteps = 1000
    # xaxis = Xaxis(model.Laser.f, "lin", freq_min, freq_max, nsteps, name="xaxis")

    phase_deg_min = -270
    phase_deg_max = 270
    nsteps = 10000
    xaxis = Xaxis(model.PM3.phi, "lin", phase_deg_min, phase_deg_max, nsteps, name="xaxis")

    out = model.run(xaxis)

    phase_deg_min = -270
    phase_deg_max = 270
    nsteps = 10000
    xaxis = Xaxis(model.MC2.phi, "lin", phase_deg_min, phase_deg_max, nsteps, name="xaxis")

    out2 = model.run(xaxis)

    phase_deg_min = -270
    phase_deg_max = 270
    nsteps = 10000
    xaxis = Xaxis(model.MC4.phi, "lin", phase_deg_min, phase_deg_max, nsteps, name="xaxis")

    out3 = model.run(xaxis)

    # ipdb.set_trace()

    phase_deg_min = -0.5
    phase_deg_max = 0.5
    nsteps = 10000
    xaxis = Xaxis("CARM.DC", "lin", phase_deg_min, phase_deg_max, nsteps, name="xaxis")

    model.modes("off")

    out4 = model.run(xaxis)

    fig, (s1, s2) = plt.subplots(2, sharex=True, figsize=(12,8))

    s1.plot(out.x[0], out["PMC_refl_demod_I"], label="PMC Refl Power")
    s2.plot(out.x[0], out["PMC_refl_demod_Q"], label="PMC Refl Power")
    s1.plot(out2.x[0], out2["IMC1_refl_demod_I"], label="IMC1 Refl Power")
    s2.plot(out2.x[0], out2["IMC1_refl_demod_Q"], label="IMC1 Refl Power")
    s1.plot(out3.x[0], out3["IMC2_refl_demod_I"], label="IMC2 Refl Power")
    s2.plot(out3.x[0], out3["IMC2_refl_demod_Q"], label="IMC2 Refl Power")
    s1.plot(out4.x[0], out4["C_refl_demod_I"], label="Cavity Refl Power")
    s2.plot(out4.x[0], out4["C_refl_demod_Q"], label="Cavity Refl Power")


    s1.set_title(f"IO PDH Error Signals (RF PDs I and Q output)")
    s1.set_ylabel("Power in I-quad $P_I$ [W]")
    s2.set_ylabel("Power in Q-quad $P_Q$ [W]")
    s2.set_xlabel("DC End Mirror Phase offset $\\Delta \\phi$ [degree]")
    
    # s1.axvline(x=-pmc_fsr, label=f"PMC FSR = {pmc_fsr*1e-6:.0f} MHz", color="k", ls="--", alpha=0.5)
    # s2.axvline(x=-pmc_fsr, label=f"PMC FSR = {pmc_fsr*1e-6:.0f} MHz", color="k", ls="--", alpha=0.5)
    # s1.axvline(x=pmc_fsr, color="k", ls="--", alpha=0.5)
    # s2.axvline(x=pmc_fsr, color="k", ls="--", alpha=0.5)

    # ipdb.set_trace()

    s1.grid()
    s1.grid(which="minor", ls="--", alpha=0.7)
    s2.grid()
    s2.grid(which="minor", ls="--", alpha=0.7)

    s1.legend(bbox_to_anchor=(1, 0.5))

    # plt.show()
    # fig.savefig(tpath_join("CARM_sweep.pdf"))

    
