import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pytest
import finesse.analysis.actions as fa
# import cosmicexplorer.finesse as ce
import cosmicexplorer.finesse.model.cosmicexplorer as ce

from finesse import Model
import finesse.components as _fc
from wield.control.plotting import plotTF


@pytest.mark.xfail(reason="need to fix names")
@pytest.mark.parametrize("vertex_type", ["reverse_ligo", "split_telescope"])
def T_ASC(vertex_type, tpath_join, makegrid):
    # vertex_type = "crab"
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    factory.vertex_type = vertex_type
    factory.options.LSC.add_readouts = True
    factory.options.ASC.add = True
    factory.options.ASC.add_DOFs = True
    factory.set_default_drives(P_torque=False, Y_torque=False)
    model = factory.make()
    model.modes(maxtem=2)

    dofs = [f"{dof}.AC" for dof in ["DARM", "DHARD_P"]]
    detectors = [f"{det}.{phase}" for det in ["AS_f2", "AS_A_WFS_f2y"] for phase in ["I", "Q"]]
    detectors += ["BHD.DC.o"]
    print("")
    print(dofs)
    print(detectors)
    F_Hz = np.geomspace(1, 5e3, 300)
    sol = model.run(
        fa.Series(
            ce.InitialLock(),
            fa.Xaxis("DHARD_P.DC", "lin", -100e-9, 100e-9, 200, relative=True, name="DHARD_P"),
            fa.Xaxis("DARM.DC", "lin", -0.1, 0.1, 200, relative=True, name="DARM"),
            fa.FrequencyResponse(F_Hz, dofs, detectors, name="fresp"),
            fa.SensingMatrixDC(
                ["DHARD_P"],
                ["AS_A_WFS_f2y", "AS_B_WFS_f2y"],
                name="ASC_sensing",
            ),
        )
        )

    def plot_sweep(dof, *sig):
        assert len(sig) in [1, 2]
        fig, ax = plt.subplots()
        x_rad = np.pi / 180 * sol[f"{dof}"].x[0]
        ax.plot(
            x_rad, sol[f"{dof}", f"{sig[0]}_I"], label=f"{sig[0]} I")
        ax.plot(
            x_rad, sol[f"{dof}", f"{sig[0]}_Q"], label=f"{sig[0]} Q", ls="--")
        if len(sig) == 2:
            ax.plot(
                x_rad, sol[f"{dof}", sig[1]], label=sig[1])
        ax.legend()
        makegrid(ax, x_rad)
        ax.set_xlabel(f"Detuning [rad]")
        ax.set_ylabel(f"rf power [W]")
        ax.set_title(f"{dof} Sweep")
        fig.savefig(tpath_join(f"{dof}_{sig[0]}_sweep.pdf"))

    plot_sweep("DHARD_P", "AS_A_WFS_f2y")
    plot_sweep("DHARD_P", "AS_B_WFS_f2y")
    plot_sweep("DARM", "AS_f2")
    # plot_sweep("DARM", "AS_f2", "BHD_DC")

    tfs = sol["fresp"]
    fig = plotTF(F_Hz, tfs["DARM.AC", "BHD.DC.o"], label="BHD")
    plotTF(F_Hz, tfs["DARM.AC", "AS_f2.Q"], *fig.axes, label="AS f2 Q")
    plotTF(F_Hz, tfs["DARM.AC", "AS_f2.I"], *fig.axes, label="AS f2 I")
    fig.axes[0].legend(loc="lower left")
    fig.savefig(tpath_join("DARM_fresp.pdf"))

    fig = plotTF(F_Hz, tfs["DHARD_P.AC", "AS_A_WFS_f2y.I"], label="I")
    plotTF(F_Hz, tfs["DHARD_P.AC", "AS_A_WFS_f2y.Q"], *fig.axes, label="Q")
    fig.axes[0].legend()
    fig.savefig(tpath_join("DHARD_P_fresp.pdf"))

    print(sol["ASC_sensing"])
    # fig, _ = sol["ASC_sensing"].plot(1, 1)
    # fig.savefig(tpath_join("DHARD_P_sensing_matrix.pdf"))
