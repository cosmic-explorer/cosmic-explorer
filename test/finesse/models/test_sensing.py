import numpy as np
import matplotlib.pyplot as plt
import pytest
import finesse.analysis.actions as fa
import cosmicexplorer.finesse.model as ce
from cosmicexplorer import VERTEX_TYPES

from finesse import Model
import finesse.components as _fc
from wield.control.plotting import plotTF

@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_tuning_sb_resonance(vertex_type, tpath_join, makegrid):
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    model = factory.make()
    model.modes("off")

    sol = model.run(
        fa.Series(
            ce.InitialLock(),
            # fa.Change({"SECL.DC": 90}),
            fa.Xaxis("SECL.DC", "lin", -180, 180, 200, relative=True, name="SECL"),
            fa.Xaxis("PRCL.DC", "lin", -180, 180, 200, relative=True, name="PRCL"),
            fa.Xaxis("XARM.DC", "lin", -180, 180, 200, relative=True, name="XARM"),
            fa.Xaxis("YARM.DC", "lin", -180, 180, 200, relative=True, name="YARM"),
        )
    )

    def plot_sweep(dof, sb_key, plot_carrier=True):
        fig, ax = plt.subplots()
        x_deg = sol[f"{dof}"].x[0]
        ax.semilogy(x_deg, sol[f"{dof}", f"P_uf1_{sb_key}"], label="+f1")
        ax.semilogy(x_deg, sol[f"{dof}", f"P_lf1_{sb_key}"], label="-f1", ls="--")
        ax.semilogy(x_deg, sol[f"{dof}", f"P_uf2_{sb_key}"], label="+f2")
        ax.semilogy(x_deg, sol[f"{dof}", f"P_lf2_{sb_key}"], label="-f2", ls="--")
        if plot_carrier:
            ax.semilogy(x_deg, sol[f"{dof}", f"P_c0_{sb_key}"], label="carrier")
        ax.legend()
        makegrid(ax, x_deg)
        ax.set_xlabel(f"Change in {sb_key.upper()} length from nominal [deg]")
        ax.set_ylabel(f"Power in {sb_key.upper()} [W]")
        ax.set_title(f"{dof} Sweep")
        fig.savefig(tpath_join(f"{dof}_sweep.pdf"))

    plot_sweep("PRCL", "PRC")
    plot_sweep("SECL", "SEC", plot_carrier=False)
    plot_sweep("XARM", "XARM")
    plot_sweep("YARM", "YARM")


@pytest.mark.parametrize("vertex_type", VERTEX_TYPES)
def T_PDH_sweep(vertex_type, tpath_join, makegrid):
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    factory.options.LSC.add_readouts = True
    model = factory.make()
    sol = model.run(
        fa.Series(
            fa.Xaxis(model.DARM.DC, "lin", -0.5, 0.5, 250, relative=True, name="DARM"),
            fa.Xaxis(model.CARM.DC, "lin", -0.5, 0.5, 250, relative=True, name="CARM"),
        )
    )

    print(sol["DARM"].outputs)
    x_deg = sol["DARM"].x[0]
    bhd1 = sol["DARM", "BHD_BHD1_DC"]
    bhd2 = sol["DARM", "BHD_BHD2_DC"]
    fig, ax = plt.subplots()
    ax.plot(x_deg, bhd1, label="BHD1")
    ax.plot(x_deg, bhd2, label="BHD2")
    ax.plot(x_deg, bhd1 - bhd2, label="DIFF")
    ax.plot(x_deg, sol["DARM", "BHD_DC"], ls="--", label="DC")
    ax.legend()
    makegrid(ax, x_deg)
    fig.savefig(tpath_join("DARM.pdf"))

    x_deg = sol["CARM"].x[0]
    fig, ax = plt.subplots()
    ax.plot(x_deg, sol["CARM", "REFL_f1_I"], label="I")
    ax.plot(x_deg, sol["CARM", "REFL_f1_Q"], label="Q")
    ax.legend()
    makegrid(ax, x_deg)
    fig.savefig(tpath_join("CARM.pdf"))

    assert np.all(sol["DARM", "BHD_DC"] == bhd1 - bhd2)

def T_fresp(tpath_join):
    vertex_type = "jacknife"
    factory = ce.CosmicExplorerFactory(f"{vertex_type}.yaml")
    model = factory.make()
    model.modes("off")
    F_Hz = np.geomspace(0.5, 10e3, 200)
    sol = model.run(
        fa.Series(
            ce.InitialLock(),
            # fa.Change({"SECL.DC": 90}),
            fa.FrequencyResponse(F_Hz, ["DARM.AC"], ["BHD.DC.o"], name="fresp"),
        ),
    )
    fig = plotTF(F_Hz, sol["fresp"].out[..., 0, 0])
    fig.savefig(tpath_join("DARM.pdf"))
