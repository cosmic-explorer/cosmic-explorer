import numpy as np

from finesse import Model
import cosmicexplorer.finesse.components as fc
import finesse.analysis.actions as fa


def T_couplings(tpath_join):
    model = Model()
    isolation_dB = 20
    L31 = 0.01
    L12 = 0.02
    model.add(fc.FaradayIsolator("FI", isolation=isolation_dB, L31=L31, L12=L12))
    fsig = model.fsig.f.ref
    ports = [
        (model.FI.p1, +fsig),
        (model.FI.p2, +fsig),
        (model.FI.p3, +fsig),
        (model.FI.p4, +fsig),
    ]
    F_Hz = np.array([1])
    sol = model.run(fa.FrequencyResponse3(F_Hz, ports, ports))
    cpl = sol.out.squeeze()
    bkwd = 10**(-isolation_dB / 20)
    print(cpl.shape)
    print(cpl.real)
    assert np.all(
        cpl == np.array([
            [0, 1 * np.sqrt(1 - L12), bkwd, 0],
            [bkwd, 0, 0, 1],
            [1 * np.sqrt(1 - L31), 0, 0, bkwd],
            [0, bkwd, 1, 0],
        ])
    )
