import numpy as np
import matplotlib.pyplot as plt
import pytest

from finesse import Model
import cosmicexplorer.finesse.components as fc
import finesse.analysis.actions as fa
from wield.control.plotting import plotTF
from cosmicexplorer import qlib


def mismatch_cavity():
    model = Model()
    model.add(fc.Mirror("ITM", T=0.014, L=0, Rc=3e3))
    model.add(fc.Mirror("ETM", T=0, L=0, Rc=3e3))
    model.connect(model.ITM.fr, model.ETM.fr, L=4e3)
    model.add(fc.Cavity("cavARM", model.ETM.fr.o))
    model.add(fc.MismatchInterface("MM", loss=0.01, phase=0))
    model.add(fc.Laser("Laser", P=1))
    # model.connect(model.Laser.p1, model.ITM.bk)
    model.connect(model.Laser.p1, model.MM.p1)
    model.connect(model.MM.p2, model.ITM.bk)
    model.phase_config(zero_k00=False)
    model.beam_trace()
    return model

def T_MM_loss(tpath_join, makegrid):
    print("")
    model = mismatch_cavity()
    q1 = model.ITM.bk.i.qx
    # q2 = q1.overlap_contour(q1, 0.01, 0 * np.pi / 180)[()]
    # model.ITM.bk.i.q = q2
    model.MM.mismatch_mode = [2, 0]
    model.MM.phase = 0
    model.MM.is_OPD = True
    model.MM.loss = 0.01
    F_Hz = np.geomspace(1, 2e3, 200)
    fsig = model.fsig.f.ref
    model.modes("even", maxtem=2)
    print(model.homs)
    # cav_port = "ITM.bk"
    cav_port = "MM.p1"
    sol = model.run(
        fa.FrequencyResponse3(
            F_Hz,
            [
                (f"{cav_port}.i", +fsig),
                (f"{cav_port}.i", -fsig),
            ],
            [
                (f"{cav_port}.o", +fsig),
                (f"{cav_port}.o", -fsig),
            ],
            name="fresp",
        )
    )
    print("")
    tfs = sol.out
    print(tfs.shape)
    fig = plotTF(F_Hz, tfs[..., 0, 0, 0, 0])
    plotTF(F_Hz, tfs[..., 0, 0, 1, 1], *fig.axes, ls="--")
    plotTF(F_Hz, tfs[..., 0, 0, 1, 0], *fig.axes, ls="-.")
    fig.savefig(tpath_join("REFL.pdf"))

    tfs_2p, mlib = qlib.to_2p(tfs)
    LOa = mlib.adjoint(mlib.LO(0))
    LOdotAS = LOa @ tfs_2p
    mq = LOdotAS[..., 0, 0]
    mp = LOdotAS[..., 0, 1]
    metrics = qlib.thetaXietaGamma(mq, mp)
    MM_loss = mlib.Vnorm_sq(mlib.adjoint(LOdotAS)) - metrics.etaGamma
    fig, ax = plt.subplots()
    ax.loglog(F_Hz, MM_loss)
    makegrid(ax, F_Hz)
    fig.savefig(tpath_join("loss.pdf"))
