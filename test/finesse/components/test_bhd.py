import numpy as np
import pytest

from finesse import Model
import cosmicexplorer.finesse.components as fc
import finesse.components as _fc
import finesse.detectors as fd
import finesse.analysis.actions as fa
from wield.control.plotting import plotTF


def FP_cav():
    model = Model()
    model.add(fc.ThickMirror("ITM", T=0.014, L=0))
    model.add(fc.ThickMirror("ETM", T=1e-6, L=0))
    model.add(fc.Laser("Laser", P=1e-6))
    model.connect(model.ITM.fr, model.ETM.fr, 4e3)
    model.connect(model.Laser.p1, model.ITM.bk)
    model.add(fd.PowerDetector("P_CAV", model.ETM.fr.o))
    model.add(fd.PowerDetector("P_TRANS", model.ETM.bk.o))

    return model


@pytest.fixture
def BHD_explicit():
    model = FP_cav()
    ##################################################
    model.add(_fc.Laser("BHD_LO", P=1, phase=0))
    BHD_BS = model.add(_fc.Beamsplitter("BHD_BS", T=0.5, L=0, alpha=0.5))
    model.connect(model.BHD_LO.p1, BHD_BS.p4)
    model.add(_fc.ReadoutDC("BHD1", BHD_BS.p2.o))
    model.add(_fc.ReadoutDC("BHD2", BHD_BS.p3.o))
    model.add(_fc.Amplifier("BHD_AMP", gain=+1))
    model.connect(model.BHD1.DC.o, model.BHD_AMP.p1, gain=+1)
    model.connect(model.BHD2.DC.o, model.BHD_AMP.p1, gain=-1)
    ##################################################

    model.connect(model.ETM.bk, BHD_BS.p1)

    return model


@pytest.fixture
def BHD_component():
    model = FP_cav()
    model.add(fc.BalancedHomodyneDetector("BHD", model.ETM.bk))
    return model


def T_compare_bhd(BHD_component, BHD_explicit, tpath_join):
    model1 = BHD_component
    model2 = BHD_explicit
    F_Hz = np.geomspace(.5, 5e3, 100)
    model1.fsig.f = 1
    model2.fsig.f = 1
    fsig1 = model1.fsig.f.ref
    fsig2 = model2.fsig.f.ref
    sol1 = model1.run(
        fa.FrequencyResponse(
            F_Hz,
            ["ITM.mech.z"],
            ["BHD.DC.o"],
        )
    )
    sol2 = model2.run(
        fa.FrequencyResponse(
            F_Hz,
            ["ITM.mech.z"],
            ["BHD1.DC", "BHD2.DC", "BHD_AMP.p2.o"],
        )
    )

    BHD1 = sol2.out[..., 0, 0]
    BHD2 = sol2.out[..., 1, 0]
    BHD = sol2.out[..., 2, 0]
    fig = plotTF(F_Hz, sol1.out[..., 0, 0], label="BHD component")
    plotTF(F_Hz, BHD, *fig.axes, label="by hand", ls="--")
    plotTF(F_Hz, BHD1, *fig.axes, label="BHD1")
    plotTF(F_Hz, BHD2, *fig.axes, label="BHD2", ls="--")
    fig.axes[0].legend()
    fig.savefig(tpath_join("BHD.pdf"))
    assert np.allclose(sol1.out[..., 0, 0], BHD)
    assert np.allclose(BHD1, -BHD2)
    assert np.allclose(BHD, BHD1 - BHD2)
