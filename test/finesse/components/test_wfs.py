import numpy as np

from finesse import Model
import cosmicexplorer.finesse.components as fc
import finesse.analysis.actions as fa
from wield.control.plotting import plotTF


def FP_cav():
    model = Model()
    model.add(fc.Mirror("ETM", T=0, L=0, Rc=30e3))
    model.add(fc.Mirror("ITM", T=0.014, L=0, Rc=30e3))
    model.connect(model.ETM.fr, model.ITM.fr, 40e3)
    model.add(fc.Cavity("cav", model.ETM.fr.o))
    model.add(fc.Laser("Laser", P=1))
    model.add(fc.Modulator("Mod", f=9e6, midx=0.2, mod_type="pm"))
    model.connect(model.Laser.p1, model.Mod.p1)
    model.add(fc.DirectionalBeamsplitter("FI"))
    model.connect(model.Mod.p2, model.FI.p1)
    model.connect(model.FI.p3, model.ITM.bk)
    # model.connect(model.Mod.p2, model.ITM.bk)
    model.add(fc.ReadoutRF("REFL", model.ITM.bk.o, f=9e6))
    model.add(fc.WFS("REFL_WFS", model.FI.p4, f=9e6, gouy_phase=40, demod_phase=10))
    return model


def T_sens(tpath_join):
    print("")
    F_Hz = np.geomspace(1, 1e3, 100)
    model = FP_cav()
    model.modes(maxtem=1)
    sol = model.run(
        fa.FrequencyResponse(
            F_Hz,
            ["ETM.mech.z", "ETM.mech.yaw"],
            ["REFL.I", "REFL.Q", "REFL_WFS_Ax.I", "REFL_WFS_Ax.Q", "REFL_WFS_Bx.I", "REFL_WFS_Bx.Q"],
        )
    )
    print(np.count_nonzero(sol.out[..., 0]))
    print(np.count_nonzero(sol.out[..., 1]))
    fig = plotTF(F_Hz, sol.out[..., 0, 0])
    plotTF(F_Hz, sol.out[..., 1, 0], *fig.axes)
    fig.savefig(tpath_join("LSC.pdf"))
    fig = plotTF(F_Hz, sol.out[..., 2, 1])
    plotTF(F_Hz, sol.out[..., 3, 1], *fig.axes)
    plotTF(F_Hz, sol.out[..., 4, 1], *fig.axes)
    plotTF(F_Hz, sol.out[..., 5, 1], *fig.axes)
    fig.savefig(tpath_join("ASC.pdf"))
