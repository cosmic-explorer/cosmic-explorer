import numpy as np
import pytest

from finesse import Model
import cosmicexplorer.finesse.components as fc
import finesse.components as _fc
import finesse.detectors as fd
import finesse.analysis.actions as fa
from wield.control.plotting import plotTF
from wield.bunch import Bunch


def finesse_thin_mirror(params):
    model = Model()
    M = model.add(_fc.Mirror("M", T=params.T, L=params.L))
    M.R = 1 - M.T.ref - M.L.ref
    model.add(_fc.Laser("Laser_fr", P=params.Pfr_W))
    model.add(_fc.Laser("Laser_bk", P=params.Pbk_W, phase=-params.phase_deg))
    model.connect(model.Laser_fr.p1, M.p1)
    model.connect(model.Laser_bk.p1, M.p2)
    model.add(_fc.Pendulum("M_susp", M.mech, mass=params.M_kg, fz=params.f0_Hz, Qz=params.Q))
    return model


def ce_thin_mirror(params):
    model = Model()
    M = model.add(fc.Mirror("M", T=params.T, L=params.L))
    model.add(fc.Laser("Laser_fr", P=params.Pfr_W))
    model.add(fc.Laser("Laser_bk", P=params.Pbk_W, phase=-params.phase_deg))
    model.connect(model.Laser_fr.p1, M.fr)
    model.connect(model.Laser_bk.p1, M.bk)
    model.add(fc.Pendulum("M_susp", M.mech, mass=params.M_kg, fz=params.f0_Hz, Qz=params.Q))
    return model


FP_PARAMS = Bunch(
    Pin_W=18,
    Ri_m=1940,
    Re_m=2245,
    Ri_AR_m=3000,
    Lcav_m=3995,
    Ti=0.014,
    Te=5e-6,
    L=40e-6,
    thickness_m=100,  # yes, this is ridiculous, but it's a good test
    nr=150,  # this is also ridiculous
    R_AR=5e-6,
)


WEDGE_PARAMS = Bunch(
    T=0.014,
    L=40e-6,
    Rc_m=3000,
    Rc_AR_m=(100, 200),
    thickness_m=10,
    nr=1.45,
    wedge_angle=10,
    R_AR=5e-6,
)


@pytest.fixture
def finesse_FP_laser():
    model = Model()
    model.add(_fc.Mirror("ITM", T=FP_PARAMS.Ti, L=FP_PARAMS.L, Rc=FP_PARAMS.Ri_m))
    model.add(_fc.Mirror("ETM", T=FP_PARAMS.Te, L=FP_PARAMS.L, Rc=FP_PARAMS.Re_m))
    model.ITM.R = 1 - model.ITM.T.ref - model.ITM.L.ref
    model.connect(model.ITM.p1, model.ETM.p1, L=FP_PARAMS.Lcav_m)
    model.add(_fc.Cavity("CAV", model.ETM.p1.o))
    model.add(_fc.Laser("Laser", P=FP_PARAMS.Pin_W))
    model.connect(model.Laser.p1, model.ITM.p2)
    model.add(_fc.DegreeOfFreedom("ARM", model.ETM.dofs.z, +1))
    return model


@pytest.fixture
def ce_FP_laser():
    model = Model()
    model.add(fc.Mirror("ITM", T=FP_PARAMS.Ti, L=FP_PARAMS.L, Rc=FP_PARAMS.Ri_m))
    model.add(fc.Mirror("ETM", T=FP_PARAMS.Te, L=FP_PARAMS.L, Rc=FP_PARAMS.Re_m))
    model.connect(model.ITM.fr, model.ETM.fr, L=FP_PARAMS.Lcav_m)
    model.add(fc.Cavity("CAV", model.ETM.fr.o))
    model.add(fc.Laser("Laser", P=FP_PARAMS.Pin_W))
    model.connect(model.Laser.p1, model.ITM.bk)
    model.add(fc.DegreeOfFreedom("ARM", model.ETM.dofs.z, +1))
    model.add(fd.PowerDetector("P_CAV", model.ETM.fr.o))
    model.add(fd.PowerDetector("P_IN", model.ITM.bk.i))
    return model


@pytest.fixture
def finesse_ThickFP_laser():
    model = Model()
    model.add(_fc.Mirror("ITM", T=FP_PARAMS.Ti, L=FP_PARAMS.L, Rc=FP_PARAMS.Ri_m))
    model.add(_fc.Mirror(
        "ITM_AR", T=1-FP_PARAMS.R_AR, L=FP_PARAMS.R_AR, Rc=FP_PARAMS.Ri_AR_m,
    ))
    model.ITM.R = 1 - model.ITM.T.ref - model.ITM.L.ref
    model.connect(
        model.ITM_AR.p1, model.ITM.p2, L=FP_PARAMS.thickness_m, nr=FP_PARAMS.nr,
    )
    model.add(_fc.Mirror("ETM", T=FP_PARAMS.Te, L=FP_PARAMS.L, Rc=FP_PARAMS.Re_m))
    model.connect(model.ITM.p1, model.ETM.p1, L=FP_PARAMS.Lcav_m)
    model.add(_fc.Cavity("CAV", model.ETM.p1.o))
    model.add(_fc.Laser("Laser", P=FP_PARAMS.Pin_W))
    model.connect(model.Laser.p1, model.ITM_AR.p2)
    model.add(_fc.DegreeOfFreedom("ARM", model.ETM.dofs.z, +1))
    return model


@pytest.fixture
def ce_ThickFP_laser():
    model = Model()
    model.add(fc.ThickMirror(
        "ITM", T=FP_PARAMS.Ti, L=FP_PARAMS.L, R_AR=FP_PARAMS.R_AR, thickness=FP_PARAMS.thickness_m,
        nr=FP_PARAMS.nr, Rc=FP_PARAMS.Ri_m, Rc_AR=FP_PARAMS.Ri_AR_m,
    ))
    model.add(fc.ThickMirror("ETM", T=FP_PARAMS.Te, L=FP_PARAMS.L, Rc=FP_PARAMS.Re_m))
    model.connect(model.ITM.fr, model.ETM.fr, L=FP_PARAMS.Lcav_m)
    model.add(fc.Cavity("CAV", model.ETM.fr.o))
    model.add(fc.Laser("Laser", P=FP_PARAMS.Pin_W))
    model.connect(model.Laser.p1, model.ITM.bk)
    model.ETM.phi = 0  # need to set it again for some reason
    model.add(fc.DegreeOfFreedom("ARM", model.ETM.dofs.z, +1))
    model.add(fd.PowerDetector("P_CAV", model.ETM.fr.o))
    model.add(fd.PowerDetector("P_IN", model.ITM.bk.i))
    return model


def T_change_thin_mirror_params(ce_FP_laser, finesse_FP_laser, tpath_join):
    model1 = ce_FP_laser
    model2 = finesse_FP_laser

    def assert_beam_params():
        model1.beam_trace()
        model2.beam_trace()
        assert model1.ETM.fr.o.q == model2.ETM.p1.o.q
        assert model1.ITM.bk.o.q == model2.ITM.p2.o.q

    print("")
    assert_beam_params()
    model1.ETM.Rc = 2250
    model2.ETM.Rc = 2250
    assert_beam_params()
    model1.ETM.Rcx = 2255
    model2.ETM.Rcx = 2255
    assert_beam_params()
    model1.ITM.Rc = [1944, 1936]
    model2.ITM.Rc = [1944, 1936]
    assert_beam_params()
    F_Hz = np.geomspace(5, 5e3, 200)
    model1.ITM.T = 0.01
    model2.ITM.T = 0.01

    def run_model(model, itm_bk):
        model.modes("off")
        model.fsig.f = 1
        fsig = model.fsig.f.ref
        itm_in = [(f"ITM.{itm_bk}.i", +fsig)]
        itm_refl = [(f"ITM.{itm_bk}.o", +fsig)]

        def fresp_actions(key):
            return [
                fa.FrequencyResponse3(
                    F_Hz, itm_in, itm_refl, name=f"REFL_{key}",
                ),
                fa.FrequencyResponse4(
                    F_Hz, ["ETM.mech.z"], itm_refl, name=f"TRANS_{key}",
                ),
            ]

        sol = model.run(
            fa.Series(
                *fresp_actions("tuned"),
                # fa.Change({"ITM.phi": -18, "ETM.phi": 20, "ITM.T": 0.01}),
                # fa.Change({"ITM.phi": -18, "ETM.phi": 20}),
                # since ETM is now controlled by a DOF can't change phi directly
                fa.Change({"ITM.phi": -18, "ARM.DC": 20}),
                *fresp_actions("detuned"),
            ),
        )
        return sol

    sol1 = run_model(model1, "bk")
    sol2 = run_model(model2, "p2")

    fig = plotTF(F_Hz, sol1["REFL_tuned"].out[..., 0, 0, 0, 0])
    plotTF(F_Hz, sol2["REFL_tuned"].out[..., 0, 0, 0, 0], *fig.axes, ls="--")
    plotTF(F_Hz, sol1["REFL_detuned"].out[..., 0, 0, 0, 0], *fig.axes)
    plotTF(F_Hz, sol2["REFL_detuned"].out[..., 0, 0, 0, 0], *fig.axes, ls="--")
    fig.savefig(tpath_join("REFL.pdf"))

    fig = plotTF(F_Hz, sol1["TRANS_tuned"].out[..., 0, 0, 0])
    plotTF(F_Hz, sol2["TRANS_tuned"].out[..., 0, 0, 0], *fig.axes, ls="--")
    plotTF(F_Hz, sol1["TRANS_detuned"].out[..., 0, 0, 0], *fig.axes)
    plotTF(F_Hz, sol2["TRANS_detuned"].out[..., 0, 0, 0], *fig.axes, ls="--")
    fig.savefig(tpath_join("TRANS.pdf"))

    for key in ["REFL_tuned", "REFL_detuned", "TRANS_tuned", "TRANS_detuned"]:
        assert np.allclose(sol1[key].out, sol2[key].out)


def T_change_thick_mirror_params(ce_ThickFP_laser, finesse_ThickFP_laser, tpath_join):
    model1 = ce_ThickFP_laser
    model2 = finesse_ThickFP_laser
    model1._settings.phase_config.v2_transmission_phase = True
    model2._settings.phase_config.v2_transmission_phase = True

    def assert_beam_params():
        model1.beam_trace()
        model2.beam_trace()
        assert model1.ETM.fr.o.q == model2.ETM.p1.o.q
        assert model1.ITM.bk.o.q == model2.ITM_AR.p2.o.q

    print("")
    assert_beam_params()
    model1.ETM.Rc = 2250
    model2.ETM.Rc = 2250
    assert_beam_params()
    model1.ETM.Rcx = 2255
    model2.ETM.Rcx = 2255
    assert_beam_params()
    model1.ITM.Rc = [1944, 1936]
    model2.ITM.Rc = [1944, 1936]
    assert_beam_params()
    F_Hz = np.geomspace(5, 5e3, 100)
    model1.ITM.T = 0.01
    model2.ITM.T = 0.01

    def run_model(model, itm_bk):
        model.modes("off")
        model.fsig.f = 1
        fsig = model.fsig.f.ref
        itm_in = [(f"{itm_bk}.i", +fsig)]
        itm_refl = [(f"{itm_bk}.o", +fsig)]

        def fresp_actions(key):
            return [
                fa.FrequencyResponse3(
                    F_Hz, itm_in, itm_refl, name=f"REFL_{key}",
                ),
                fa.FrequencyResponse4(
                    F_Hz, ["ETM.mech.z"], itm_refl, name=f"TRANS_{key}",
                ),
            ]

        sol = model.run(
            fa.Series(
                *fresp_actions("tuned"),
                # fa.Change({"ITM.phi": -18, "ETM.phi": 20, "ITM.T": 0.01}),
                # fa.Change({"ITM.phi": -18, "ETM.phi": 20}),
                fa.Change({"ITM.phi": -18, "ARM.DC": 20}),
                *fresp_actions("detuned"),
            ),
        )
        return sol

    sol1 = run_model(model1, "ITM.bk")
    sol2 = run_model(model2, "ITM_AR.p2")

    fig = plotTF(F_Hz, sol1["REFL_tuned"].out[..., 0, 0, 0, 0])
    plotTF(F_Hz, sol2["REFL_tuned"].out[..., 0, 0, 0, 0], *fig.axes, ls="--")
    plotTF(F_Hz, sol1["REFL_detuned"].out[..., 0, 0, 0, 0], *fig.axes)
    plotTF(F_Hz, sol2["REFL_detuned"].out[..., 0, 0, 0, 0], *fig.axes, ls="--")
    fig.savefig(tpath_join("REFL.pdf"))

    fig = plotTF(F_Hz, sol1["TRANS_tuned"].out[..., 0, 0, 0])
    plotTF(F_Hz, sol2["TRANS_tuned"].out[..., 0, 0, 0], *fig.axes, ls="--")
    plotTF(F_Hz, sol1["TRANS_detuned"].out[..., 0, 0, 0], *fig.axes)
    plotTF(F_Hz, sol2["TRANS_detuned"].out[..., 0, 0, 0], *fig.axes, ls="--")
    fig.savefig(tpath_join("TRANS.pdf"))

    for key in ["REFL_tuned", "REFL_detuned", "TRANS_tuned", "TRANS_detuned"]:
        assert np.allclose(sol1[key].out, sol2[key].out)


def T_optical_spring(tpath_join):
    params = Bunch(
        T=0.2,
        L=1e-2,
        Pfr_W=120,
        Pbk_W=90,
        phase_deg=30,
        M_kg=1e-6,
        f0_Hz=1,
        Q=100,
    )

    model1 = ce_thin_mirror(params)
    model2 = finesse_thin_mirror(params)
    F_Hz = np.geomspace(10, 1e4, 200)

    def calculate_response(model, fr_port):
        model.fsig.f = 1
        fsig = model.fsig.f.ref
        sol = model.run(
            fa.FrequencyResponse4(
                F_Hz,
                ["M.mech.z", "M.mech.F_z"],
                [
                    (f"M.{fr_port}.o", +fsig),
                    (f"M.{fr_port}.o", -fsig),
                ],
            ),
        )
        return Bunch(
            displacement=sol.out[..., 0],
            force=sol.out[..., 0],
        )

    tfs1 = calculate_response(model1, "fr")
    tfs2 = calculate_response(model2, "p1")

    fig = plotTF(F_Hz, tfs1.force[..., 0, 1])
    plotTF(F_Hz, tfs2.force[..., 0, 1], *fig.axes, ls="--")
    fig.savefig(tpath_join("spring.pdf"))
    assert np.all(tfs1.force == tfs2.force)
    assert np.all(tfs1.displacement == tfs2.displacement)


@pytest.mark.parametrize(
    "model",
    [
        "ce_FP_laser",
        "finesse_FP_laser",
        "ce_ThickFP_laser",
        "finesse_ThickFP_laser",
    ]
)
def T_dofs(model, tpath_join, request):
    model_type = model.split("_")[0]
    model = request.getfixturevalue(model)
    model.fsig.f = 1
    F_Hz = np.geomspace(0.5, 5e3, 200)
    fsig = model.fsig.f.ref
    if model_type == "finesse":
        refl_port = "ITM.p2.o"
        exc_port = "ETM.p1.o"
    elif model_type == "ce":
        refl_port = "ITM.bk.o"
        exc_port = "ETM.fr.o"

    def actions(key):
        return [
            fa.FrequencyResponse3(
                F_Hz, [(exc_port, +fsig)], [(refl_port, +fsig)],
                name=f"optical_{key}",
            ),
            fa.FrequencyResponse4(
                F_Hz, ["ETM.mech.z"], [(refl_port, +fsig)],
                name=f"mechanical_{key}",
            ),
            fa.FrequencyResponse4(
                F_Hz, ["ARM.AC.i"], [(refl_port, +fsig)],
                name=f"dof_{key}",
            ),
        ]

    sol = model.run(
        fa.Series(
            *actions("tuned"),
            fa.Change({"ARM.DC": 10}),
            *actions("detuned"),
        )
    )

    fig = plotTF(F_Hz, sol["optical_tuned"].out[..., 0, 0, 0, 0], label="tuned")
    plotTF(
        F_Hz, sol["optical_detuned"].out[..., 0, 0, 0, 0],
        *fig.axes, label="detuned")
    fig.axes[0].legend()
    fig.savefig(tpath_join("optical.pdf"))

    fig = plotTF(
        F_Hz, sol["mechanical_tuned"].out[..., 0, 0, 0], label="tuned mech")
    plotTF(
        F_Hz, sol["dof_tuned"].out[..., 0, 0, 0], *fig.axes, ls="--",
        label="tuned DOF")
    plotTF(
        F_Hz, sol["mechanical_detuned"].out[..., 0, 0, 0], *fig.axes,
        label="detuned mech")
    plotTF(
        F_Hz, sol["dof_detuned"].out[..., 0, 0, 0], *fig.axes, ls="--",
        label="detuned DOF")
    fig.axes[0].legend()
    fig.savefig(tpath_join("mechanical.pdf"))
    assert np.allclose(sol["mechanical_tuned"].out, sol["dof_tuned"].out)
    assert np.allclose(sol["mechanical_detuned"].out, sol["dof_detuned"].out)


def test_thickness(ce_ThickFP_laser):
    ITM = ce_ThickFP_laser.ITM
    assert ITM.thickness == FP_PARAMS.thickness_m
    assert ITM.optical_thickness == FP_PARAMS.thickness_m * FP_PARAMS.nr


@pytest.mark.parametrize("model", ["ce_FP_laser", "ce_ThickFP_laser"])
def T_misalign(model, request):
    model = request.getfixturevalue(model)
    sol = model.run(
        fa.Series(
            fa.Noxaxis(name="aligned"),
            fa.Change({"ITM.misaligned": True}),
            fa.Noxaxis(name="misaligned"),
        )
    )
    assert sol["misaligned", "P_CAV"] / sol["aligned", "P_CAV"] < 1e-3


@pytest.mark.parametrize("model", ["ce_FP_laser", "ce_ThickFP_laser"])
def test_ports(model, request):
    model = request.getfixturevalue(model)
    with pytest.raises(AttributeError):
        print(model.ETM.p1)
    with pytest.raises(AttributeError):
        print(model.ETM.p2)


def finesse_mirror_wedge(wedge):
    assert wedge in ["horiz", "vert"]
    model = Model()
    model.add(_fc.Mirror(
        "HR", T=WEDGE_PARAMS.T, L=WEDGE_PARAMS.L, Rc=WEDGE_PARAMS.Rc_m,
    ))
    model.add(_fc.Beamsplitter(
        "AR", T=1 - WEDGE_PARAMS.R_AR, L=WEDGE_PARAMS.R_AR,
        Rc=WEDGE_PARAMS.Rc_AR_m, alpha=WEDGE_PARAMS.wedge_angle,
        plane="xz" if wedge == "horiz" else "yz",
    ))
    model.connect(
        model.AR.p1, model.HR.p2, L=WEDGE_PARAMS.thickness_m, nr=WEDGE_PARAMS.nr,
    )
    return model


def ce_mirror_wedge(wedge):
    model = Model()
    model.add(fc.ThickMirror(
        "M", T=WEDGE_PARAMS.T, L=WEDGE_PARAMS.L, Rc=WEDGE_PARAMS.Rc_m,
        Rc_AR=WEDGE_PARAMS.Rc_AR_m, thickness=WEDGE_PARAMS.thickness_m,
        wedge=wedge, wedge_angle=WEDGE_PARAMS.wedge_angle,
    ))
    return model


@pytest.mark.parametrize("wedge", ["horiz", "vert"])
def test_wedge_abcd(wedge):
    model1 = ce_mirror_wedge(wedge)
    model2 = finesse_mirror_wedge(wedge)

    def assert_abcd_equality(p1_fr, p1_to, p2_fr, p2_to):
        for direction in ["x", "y"]:
            assert np.allclose(
                model1.ABCD(
                    model1.get(p1_fr), model1.get(p1_to), direction=direction,
                ).M,
                model2.ABCD(
                    model2.get(p2_fr), model2.get(p2_to), direction=direction,
                ).M,
            )
    assert_abcd_equality("M.fr.i", "M.bk.o", "HR.p1.i", "AR.p3.o")
    assert_abcd_equality("M.bk.i", "M.fr.o", "AR.p3.i", "HR.p1.o")
    assert_abcd_equality("M.bk.i", "M.pi.o", "AR.p3.i", "AR.p4.o")
    assert_abcd_equality("M.fr.i", "M.po.o", "HR.p1.i", "AR.p2.o")


def test_wedge_options():
    model = Model()
    with pytest.raises(ValueError):
        model.add(fc.ThickMirror("M", T=0, L=0, wedge="vertical"))
    with pytest.raises(AttributeError):
        model.add(fc.ThickMirror("M", T=0, L=0))
        model.M.wedge = "vert"
    model.add(fc.ThickMirror("M2", T=0, L=0))
    with pytest.raises(AttributeError):
        print(model.M2.pi)
    with pytest.raises(AttributeError):
        print(model.M2.po)
