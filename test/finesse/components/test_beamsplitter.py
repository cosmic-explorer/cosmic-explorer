import numpy as np
import pytest

from finesse import Model
import cosmicexplorer.finesse.components as fc
import finesse.components as _fc
from finesse.detectors import PowerDetector
import finesse.analysis.actions as fa
from wield.bunch import Bunch


PARAMS = Bunch(
    P_W=12,
    T=0.5,
    L=1e-2,
    Rc=10,
    Rc_AR=400,
    Lprop_m=1e3,
    thickness_m=100,
    nr=15,
    R_AR=10e-6,
    aoi_deg=30,
)


@pytest.fixture
def finesse_thin_BS():
    model = Model()
    BS = model.add(_fc.Beamsplitter(
        "BS", T=PARAMS.T, L=PARAMS.L, Rc=PARAMS.Rc, alpha=PARAMS.aoi_deg,
    ))
    BS.R = 1 - BS.T.ref - BS.L.ref
    model.add(_fc.Laser("Laser", P=PARAMS.P_W))
    model.connect(model.Laser.p1, BS.p1)
    model.add(_fc.Mirror("REFL", T=0, L=0))
    model.add(_fc.Nothing("TRANS1"))
    model.add(_fc.Nothing("TRANS2"))
    model.connect(BS.p3, model.TRANS1.p1, L=PARAMS.Lprop_m)
    model.connect(BS.p4, model.TRANS2.p1, L=PARAMS.Lprop_m)
    model.connect(BS.p2, model.REFL.p1, L=PARAMS.Lprop_m)
    model.add(PowerDetector("P_TRANS1", model.TRANS1.p1.i))
    model.add(PowerDetector("P_REFL", model.REFL.p1.i))
    return model


@pytest.fixture
def ce_thin_BS():
    model = Model()
    BS = model.add(fc.Beamsplitter(
        "BS", T=PARAMS.T, L=PARAMS.L, Rc=PARAMS.Rc, alpha=PARAMS.aoi_deg,
    ))
    model.add(fc.Laser("Laser", P=PARAMS.P_W))
    model.connect(model.Laser.p1, BS.fr1)
    model.add(fc.Mirror("REFL", T=0, L=0))
    model.add(fc.Nothing("TRANS1"))
    model.add(fc.Nothing("TRANS2"))
    model.connect(BS.bk1, model.TRANS1.p1, L=PARAMS.Lprop_m)
    model.connect(BS.bk2, model.TRANS2.p1, L=PARAMS.Lprop_m)
    model.connect(BS.fr2, model.REFL.fr, L=PARAMS.Lprop_m)
    model.add(PowerDetector("P_TRANS1", model.TRANS1.p1.i))
    model.add(PowerDetector("P_REFL", model.REFL.fr.i))
    return model


@pytest.fixture
def finesse_thick_BS():
    # FIXME: if new tests check any tunings or fresp, links and references need to be
    # added between the HR and AR surfaces here
    model = Model()
    nr = PARAMS.nr
    BS = model.add(_fc.Beamsplitter(
        "BS", T=PARAMS.T, L=PARAMS.L, Rc=PARAMS.Rc, alpha=PARAMS.aoi_deg,
    ))
    BS.R = 1 - BS.T.ref - BS.L.ref
    alpha_AR_rad = np.arcsin(1 / nr * np.sin(np.deg2rad(BS.alpha.ref)))
    substrate_length = PARAMS.thickness_m / np.cos(alpha_AR_rad)
    BS_AR1 = model.add(_fc.Beamsplitter(
        "BS_AR1", R=0, L=PARAMS.R_AR, alpha=np.rad2deg(alpha_AR_rad),
        Rc=PARAMS.Rc_AR,
    ))
    BS_AR2 = model.add(_fc.Beamsplitter(
        "BS_AR2", R=0, L=PARAMS.R_AR, alpha=np.rad2deg(alpha_AR_rad),
        Rc=PARAMS.Rc_AR,
    ))
    model.connect(BS.p3, BS_AR1.p1, L=substrate_length, nr=nr)
    model.connect(BS.p4, BS_AR2.p2, L=substrate_length, nr=nr)

    model.add(_fc.Laser("Laser", P=PARAMS.P_W))
    model.connect(model.Laser.p1, BS.p1)
    model.add(_fc.Mirror("REFL", T=0, L=0))
    model.add(_fc.Nothing("TRANS1"))
    model.add(_fc.Nothing("TRANS2"))
    model.connect(BS_AR1.p3, model.TRANS1.p1, L=PARAMS.Lprop_m)
    model.connect(BS_AR2.p4, model.TRANS2.p1, L=PARAMS.Lprop_m)
    model.connect(BS.p2, model.REFL.p1, L=PARAMS.Lprop_m)
    model.add(PowerDetector("P_TRANS1", model.TRANS1.p1.i))
    model.add(PowerDetector("P_REFL", model.REFL.p1.i))
    return model


@pytest.fixture
def ce_thick_BS():
    model = Model()
    BS = model.add(fc.ThickBeamsplitter(
        "BS", T=PARAMS.T, L=PARAMS.L, R_AR=PARAMS.R_AR, Rc=PARAMS.Rc,
        thickness=PARAMS.thickness_m, nr=PARAMS.nr, alpha=PARAMS.aoi_deg,
        Rc_AR=PARAMS.Rc_AR,
    ))
    model.add(fc.Laser("Laser", P=PARAMS.P_W))
    model.connect(model.Laser.p1, BS.fr1)
    model.add(fc.Mirror("REFL", T=0, L=0))
    model.add(fc.Nothing("TRANS1"))
    model.add(fc.Nothing("TRANS2"))
    model.connect(BS.bk1, model.TRANS1.p1, L=PARAMS.Lprop_m)
    model.connect(BS.bk2, model.TRANS2.p1, L=PARAMS.Lprop_m)
    model.connect(BS.fr2, model.REFL.fr, L=PARAMS.Lprop_m)
    model.add(PowerDetector("P_TRANS1", model.TRANS1.p1.i))
    model.add(PowerDetector("P_REFL", model.REFL.fr.i))
    return model


@pytest.mark.parametrize(
    "ce_model, finesse_model",
    [
        ("ce_thin_BS", "finesse_thin_BS"),
        ("ce_thick_BS", "finesse_thick_BS"),
    ]
)
def T_change_bs_params(ce_model, finesse_model, request):
    model1 = request.getfixturevalue(ce_model)
    model2 = request.getfixturevalue(finesse_model)

    def assert_powers():
        sol1 = model1.run(fa.Noxaxis())
        sol2 = model2.run(fa.Noxaxis())
        assert sol1["P_TRANS1"] == sol2["P_TRANS1"]
        assert sol1["P_REFL"] == sol2["P_REFL"]

    def assert_abcd():
        for direction in ["x", "y"]:
            refl1 = model1.ABCD(model1.Laser.p1.o, model1.REFL.fr.i, direction=direction)
            refl2 = model2.ABCD(model2.Laser.p1.o, model2.REFL.p1.i, direction=direction)
            trans1a = model1.ABCD(model1.Laser.p1.o, model1.TRANS1.p1.i, direction=direction)
            trans1b = model1.ABCD(model1.Laser.p1.o, model1.TRANS2.p1.i, direction=direction)
            trans2a = model2.ABCD(model2.Laser.p1.o, model2.TRANS1.p1.i, direction=direction)
            trans2b = model2.ABCD(model2.Laser.p1.o, model2.TRANS2.p1.i, direction=direction)
            assert np.all(refl1.M == refl2.M)
            assert np.all(trans1a.M == trans2a.M)
            assert np.all(trans1b.M == trans2b.M)

    assert_powers()
    model1.BS.T = 0.4
    model2.BS.T = 0.4
    assert_powers()
    assert_abcd()
    model1.BS.alpha = 45
    model2.BS.alpha = 45
    assert_abcd()
    model1.BS.alpha = 78
    model2.BS.alpha = 78
    model1.BS.Rc = (90, 110)
    model2.BS.Rc = (90, 110)
    assert_abcd()
    model1.BS.Rcx = 950
    model1.BS.Rcy = 210
    model2.BS.Rcx = 950
    model2.BS.Rcy = 210
    assert_abcd()
    if "thick" in ce_model:
        model1.BS.Rc_AR = [600, 900]
        model2.BS_AR1.Rc = [600, 900]
        model2.BS_AR2.Rc = [600, 900]
        assert_abcd()


def test_thickness(ce_thick_BS):
    model = ce_thick_BS
    BS = ce_thick_BS.BS
    nr = PARAMS.nr
    BS.alpha = 30
    aoi_rad = BS.alpha * np.pi / 180
    aoi_AR_rad = np.arcsin(np.sin(aoi_rad) / nr)
    substrate_thickness = PARAMS.thickness_m / np.cos(aoi_AR_rad)
    assert BS.physical_thickness == substrate_thickness
    assert BS.optical_thickness == substrate_thickness * nr


@pytest.mark.parametrize("model", ["ce_thin_BS", "ce_thick_BS"])
def test_ports(model, request):
    model = request.getfixturevalue(model)
    with pytest.raises(AttributeError):
        print(model.BS.p1)
    with pytest.raises(AttributeError):
        print(model.BS.p2)
    with pytest.raises(AttributeError):
        print(model.BS.p3)
    with pytest.raises(AttributeError):
        print(model.BS.p4)
