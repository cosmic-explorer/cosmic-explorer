import numpy as np
import matplotlib.pyplot as plt
import importlib
import pytest

from wield.control.plotting import plotTF
from wield.utilities.file_io import load, save
from wield.bunch import Bunch
import gwinc.noise.quantum2 as q2
from gwinc import load_budget as gwinc_load

from cosmicexplorer.gwinc import load_budget
from cosmicexplorer.gwinc.CEFinesseQuantum import finesse_quantum as qf
from cosmicexplorer.finesse import sqz_metrics_from_finesse
from cosmicexplorer import qlib



def T_budget(tpath_join):
    print("")
    F_Hz = np.geomspace(5, 5000, 300)
    budget = load_budget("CEFinesseQuantum")
    traces = budget.run()
    fig = traces.plot()
    fig.savefig(tpath_join("budget.pdf"))


def T_Quantum(tpath_join, makegrid):
    F_Hz = np.geomspace(5, 5000, 300)
    budget = load_budget("CEFinesseQuantum", bname="Quantum")
    traces = budget.run(freq=F_Hz)
    fig = traces.plot()
    fig.savefig(tpath_join("budget.pdf"))

    budget2 = gwinc_load("CE2silica", bname="Quantum")
    print(budget2.ifo.Optics.SRM.CavityLength)
    budget2.ifo.Optics.SRM.CavityLength = 59.952
    traces2 = budget2.run(freq=F_Hz)
    fig = traces2.plot()
    fig.savefig(tpath_join("budget2.pdf"))

    sr = q2.shotrad_debug(F_Hz, budget.ifo)
    sr2 = q2.shotrad_debug(F_Hz, budget2.ifo)
    fig = plotTF(F_Hz, sr["d_sense"])
    plotTF(F_Hz, sr2["d_sense"], *fig.axes, ls="--")
    fig.savefig(tpath_join("d_sense.pdf"))


def T_print_finesse_params():
    print("")
    from cosmicexplorer.finesse import CosmicExplorerFactory, InitialLock
    import finesse.analysis.actions as fa

    factory = CosmicExplorerFactory("reverse_ligo.yaml")
    model = factory.make()
    sol = model.run(fa.Series(
        InitialLock(),
        fa.Noxaxis(name="DC"),
        )
    )
    print(sol["DC", "P_c0_XARM"] * 1e-6)
    print(sol["DC", "P_c0_YARM"] * 1e-6)


@pytest.fixture
def quantum_fresp(fpath_join):
    import os
    force_calc = True
    fname = fpath_join("quantum_fresp.h5")
    if os.path.exists(fname) and not force_calc:
        print("loading")
        data = Bunch(load(fname))
    else:
        print("running")
        F_Hz = np.geomspace(5, 5000, 300)
        sol = qf.run_fresp(F_Hz)
        data = Bunch(
            F_Hz=F_Hz,
            fresp=sol["fresp"].out,
            fresp3=sol["fresp3"].out,
            fresp4=sol["fresp4"].out,
        )
        save(fname, data)
    return data


def T_fresp(quantum_fresp, tpath_join):
    from cosmicexplorer.qlib import MatrixLib
    F_Hz = quantum_fresp.F_Hz
    budget = load_budget("CEFinesseQuantum")
    # tfs_sb = quantum_fresp.fresp3
    mats, mlib = qf.finesse2gwinc(quantum_fresp.fresp3, quantum_fresp.fresp4, budget.ifo)

    print(mats.T.keys())

    def save_tf(key):
        if key == "DARM.AC":
            fig = plotTF(F_Hz, mats.T[key][..., 1, 0])
        else:
            fig = plotTF(F_Hz, mats.T[key][..., 0, 0])
            plotTF(F_Hz, mats.T[key][..., 1, 1], *fig.axes, ls="--")
        fig.savefig(tpath_join(f"{key.split('.')[0]}.pdf"))

    for key in mats.T.keys():
        print(key)
        save_tf(key)


def T_precomp(tpath_join):
    print("")
    budget = load_budget("CEFinesseQuantum")
    F_Hz = np.geomspace(5, 5000, 300)
    ret = qf.precomp_optomechanical_plant(F_Hz, budget.ifo, "")
    print(ret.keys())
    sr = q2.shotrad_debug(F_Hz, budget.ifo)
    print(sr.keys())


def T_dimensions(quantum_fresp):
    print("")
    budget = load_budget("CEFinesseQuantum")
    mats, mlib = qf.finesse2gwinc(quantum_fresp.fresp3, quantum_fresp.fresp4, budget.ifo)
    LOa = mlib.adjoint(mlib.LO(0))
    print(LOa.shape)
    print(mats.T["DARM.AC"].shape)
    print((LOa @ mats.T["DARM.AC"]).shape)


def T_drfpmi():
    print("")
    from gwinc import load_budget as gwinc_load
    from gwinc.optomechanicalmodels import precomp_optomechanical_plant
    F_Hz = np.geomspace(5, 5e3, 300)
    # budget = load_budget("CEFinesseQuantum")
    budget = gwinc_load("CE2silica")
    budget.ifo.Optics.MM_ARM_SRC = 0.01
    ret = precomp_optomechanical_plant(F_Hz, budget.ifo)
    print(ret.keys())
    print(ret["mats"].T["EX.pos.exc"].shape)
    sr = q2.shotrad_debug(F_Hz, budget.ifo)
