import numpy as np
import ipdb
from copy import deepcopy
import pytest

from wield.control.SFLU import SFLU
from cosmicexplorer.wield.control.model import Model
import cosmicexplorer.wield.control.components as wc
from wield.control import SISO
from wield.bunch import Bunch
from wield.control.plotting import plotTF


@pytest.fixture
def FP_model():
    Pin_W = 100
    Larm_m = 4e3
    Ti = 0.014
    M_kg = 10
    detune_rad = -1 * np.pi / 180
    suscept = lambda F_Hz: -1 / ((2 * np.pi * F_Hz)**2 * M_kg)
    suscept_ss = SISO.zpk([], [0, 0], 1 / M_kg).asSS
    model = Model()
    model.add(
        wc.Mirror("ETM", suscept=suscept, suscept_ss=suscept_ss),
        translation_xy=(15, 0),
    )
    model.add(wc.Mirror("ITM", Thr=Ti), translation_xy=(-15, 0), rotation_deg=180)
    model.connect("ETM.fr.i", "ITM.fr.o", L_m=Larm_m, detune_rad=detune_rad)
    model.connect("ITM.fr.i", "ETM.fr.o", L_m=Larm_m, detune_rad=detune_rad)
    model.add_input("ITM.bk.i", loc=(-18, 12))
    model.add_input("ETM.fr.o", loc=(8, -12))
    model.add_output("ITM.bk.o", loc=(-18, -12))
    return model


def T_FP_fresp(FP_model, tpath_join):
    print("")
    test_SS = False
    F_Hz = np.geomspace(1, 2e3, 200)
    Pin_W = 100
    tp_dc = {
        f"{opt}.{side}.{port}.tp"
        for opt in ["ETM", "ITM"]
        for side in ["fr", "bk"]
        for port in ["o", "i"]
    }
    tp_dc.remove("ETM.bk.i.tp")
    Ein_rtW = np.sqrt(Pin_W) * FP_model.mlib.LO(np.pi / 2)
    resultsDC = FP_model.computeDC(Rset=tp_dc, Cmap={"ITM.bk.i.exc": Ein_rtW})
    Rmap = {"ITM.bk.o.tp": None}
    fld_exc = {"ITM.bk.i.exc", "ETM.fr.o.exc", "ETM.pos.exc"}
    mech_exc = {"ITM.bk.i.exc", "ETM.pos.exc"}
    rslt_fld = FP_model.computeAC(F_Hz, Rmap=Rmap, Cset=fld_exc, resultsDC=resultsDC)
    rslt_mech = FP_model.computeAC(
        F_Hz, Rmap={"ETM.pos.tp": None}, Cset=mech_exc, resultsDC=resultsDC,
    )
    fig_refl = plotTF(F_Hz, rslt_fld["ITM.bk.i.exc"][..., 1, 1])
    plotTF(F_Hz, rslt_fld["ITM.bk.i.exc"][..., 1, 0], *fig_refl.axes)
    fig_refl.axes[0].set_title("Arm reflection")
    fig_trans = plotTF(F_Hz, rslt_fld["ETM.fr.o.exc"][..., 1, 1])
    fig_trans.axes[0].set_title("Arm field transmission")
    fig_disp = plotTF(F_Hz, rslt_fld["ETM.pos.exc"][..., 1, 0])
    fig_disp.axes[0].set_title("Displacement")
    fig_rp = plotTF(F_Hz, rslt_mech["ITM.bk.i.exc"][..., 0, 0])
    fig_rp.axes[0].set_title("Field to motion")
    fig_mech = plotTF(F_Hz, rslt_mech["ETM.pos.exc"])
    fig_mech.axes[0].set_title("Mechanical modification")

    if test_SS:
        rslt_fld_ss = FP_model.computeAC(
            F_Hz, Rmap=Rmap, Cset=fld_exc, resultsDC=resultsDC,
        )
        rslt_mech_ss = FP_model.computeACSS(
            F_Hz, Rmap={"ETM.pos.tp": None}, Cset=mech_exc, resultsDC=resultsDC,
        )
        plotTF(F_Hz, rslt_fld_ss["ITM.bk.i.exc"][..., 1, 1], *fig_refl.axes, ls="--")
        plotTF(F_Hz, rslt_fld_ss["ITM.bk.i.exc"][..., 1, 0], *fig_refl.axes, ls="--")
        plotTF(F_Hz, rslt_fld_ss["ETM.fr.o.exc"][..., 1, 1], *fig_trans.axes, ls="--")
        plotTF(F_Hz, rslt_fld_ss["ETM.pos.exc"][..., 1, 0], *fig_disp.axes, ls="--")
        plotTF(F_Hz, rslt_mech_ss["ITM.bk.i.exc"][..., 0, 0], *fig_rp.axes, ls="--")
        plotTF(F_Hz, rslt_mech_ss["ETM.pos.exc"].squeeze(), *fig_mech.axes, ls="--")
        for k in rslt_fld.keys():
            np.testing.assert_allclose(rslt_fld[k], rslt_fld_ss[k])
        # this will fail probably due to a wield bug right now
        # for k in rslt_mech.keys():
        #     np.testing.assert_allclose(rslt_mech[k], rslt_mech_ss[k])

    fig_refl.savefig(tpath_join("refl.pdf"))
    fig_trans.savefig(tpath_join("trans.pdf"))
    fig_disp.savefig(tpath_join("disp.pdf"))
    fig_rp.savefig(tpath_join("rp.pdf"))
    fig_mech.savefig(tpath_join("mech.pdf"))


def T_fp_graph(FP_model, tpath_join):
    print("")
    from wield.control.SFLU import nx2tikz
    sflu = FP_model._build_sflu(reduce_auto=False)
    G1 = sflu.G.copy()
    sflu.graph_reduce_auto_pos(lX=-10, rX=+10, Y=8, dY=-8)
    sflu.reduce_auto()
    sflu.graph_reduce_auto_pos_io(lX=-10, rX=+10, Y=8, dY=-8)
    G2 = sflu.G.copy()
    nx2tikz.dump_pdf(
        [G1, G2],
        fname=tpath_join("graph.pdf"),
        scale="10pt",
    )
