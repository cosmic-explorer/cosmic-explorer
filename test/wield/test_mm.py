import pytest

from wield.model.system import algo_phys

from cosmicexplorer.wield.model import DRFPMI
from cosmicexplorer import load_parameters


@pytest.mark.xfail
@pytest.mark.parametrize("vertex_type", ["reverse_ligo", "crab", "split_telescope"])
def T_basic_MM(vertex_type, tpath_join):
    params = load_parameters(f"{vertex_type}.yaml")
    sys = DRFPMI(params.IFO, vertex_type=vertex_type)
    pa = algo_phys.PhysicsAlgorithm(sys)
    overlapper = pa.mm.overlap(
        target_to=None,
        target_fr='YARM',
        waypoints=[
            'ITMX+B',
            # 'BS+A2',
            'SEM+A',
        ],
        # waypoints=[
        #     'ITMY+A',
        #     'ETMY+A',
        # ],
        Wk=1064,
    )
    plB = overlapper.plot(tpath_join('SEC.pdf'), use_in=False)
