import numpy as np
from scipy.io import loadmat

from gwinc import Struct
from wield.bunch import Bunch
from wield.utilities.file_io import load, save
from wield.control import SISO
from wield.control.plotting import plotTF


def T_extract(fpath_join, ppath_join):
    data = loadmat(fpath_join("ss_CE.mat"), squeeze_me=True, simplify_cells=True)
    ss = data["ss"]
    to = Struct(ss["out"])
    fr = Struct(ss["in"])
    # convert from matlab indexing
    for k, v in to.walk():
        to[k] = v - 1
    for k, v in fr.walk():
        fr[k] = v - 1

    zpk = Bunch(zpk=Bunch())
    z = ss["z"]
    p = ss["p"]
    k = ss["k"]
    print(z.shape)
    print(p.shape)
    print(k.shape)
    include_dofs = ["L", "P", "Y", "gnd"]
    stage_map = {
        "tst": "L3",
        "pum": "L2",
        "uim": "L1",
        "top": "M0",
        "gnd": "gnd",
    }

    def convert_key(key):
        key = key.split(".")
        key[0] = stage_map[key[0]]
        return ".".join(key)

    for to_key, to_idx in to.walk():
        if to_key.split(".")[0] != "tst":
            continue
        if to_key.split(".")[-1] not in include_dofs:
            continue
        zpk.zpk[convert_key(to_key)] = Bunch()
        for fr_key, fr_idx in fr.walk():
            if fr_key.split(".")[0] == "toposem":
                continue
            if fr_key.split(".")[-1] not in include_dofs:
                continue
            zpk.zpk[convert_key(to_key)][convert_key(fr_key)] = Bunch(
                z=z[to_idx, fr_idx],
                p=p[to_idx, fr_idx],
                k=k[to_idx, fr_idx],
            )
    save(ppath_join("ce_quad_zpk.h5"), zpk)


def T_plants(ppath_join, tpath_join):
    data = load(ppath_join("ce_quad_zpk.h5")).zpk
    print(list(data.keys()))
    F_Hz = np.geomspace(0.1, 10, 500)

    def fresp(to, fr):
        zpk = data[to][fr]
        plant = SISO.zpk(zpk.z, zpk.p, zpk.k)
        return plant.fresponse(f=F_Hz).tf

    fig = plotTF(F_Hz, fresp("L3.disp.L", "L3.drive.L"), label="L3")
    plotTF(F_Hz, fresp("L3.disp.L", "L2.drive.L"), *fig.axes, label="L2")
    plotTF(F_Hz, fresp("L3.disp.L", "M0.drive.L"), *fig.axes, label="M0")
    fig.axes[0].set_ylim(1e-11, 0.5)
    fig.axes[0].legend()
    fig.axes[0].set_title("L3 length")
    fig.savefig(tpath_join("L3_L.pdf"))

    fig = plotTF(F_Hz, fresp("L3.disp.P", "L3.drive.P"), label="L3")
    plotTF(F_Hz, fresp("L3.disp.P", "L2.drive.P"), *fig.axes, label="L2")
    plotTF(F_Hz, fresp("L3.disp.P", "M0.drive.P"), *fig.axes, label="M0")
    fig.axes[0].legend()
    fig.axes[0].set_title("L3 pitch")
    fig.savefig(tpath_join("L3_P.pdf"))

    fig = plotTF(F_Hz, fresp("L3.disp.Y", "L3.drive.Y"), label="L3")
    plotTF(F_Hz, fresp("L3.disp.Y", "L2.drive.Y"), *fig.axes, label="L2")
    plotTF(F_Hz, fresp("L3.disp.Y", "M0.drive.Y"), *fig.axes, label="M0")
    fig.axes[0].legend()
    fig.axes[0].set_title("L3 yaw")
    fig.savefig(tpath_join("L3_Y.pdf"))


def T_compare_gwinc(ppath_join, tpath_join):
    from gwinc import load_budget
    from gwinc.suspension import precomp_suspension

    zpk_data = load(ppath_join("ce_quad_zpk.h5")).zpk
    F_Hz = np.geomspace(0.1, 10, 500)
    ifo = load_budget("CE2silica").ifo
    susp = precomp_suspension(F_Hz, ifo)
    zpk = zpk_data["L3.disp.L"]["L3.drive.L"]
    plant = SISO.zpk(zpk.z, zpk.p, zpk.k)
    fresp = plant.fresponse(f=F_Hz).tf

    fig = plotTF(F_Hz, fresp, label="CE ZPK")
    plotTF(F_Hz, susp.tst_suscept, *fig.axes, ls="--", label="GWINC")
    fig.axes[0].legend()
    fig.savefig(tpath_join("L3.pdf"))
