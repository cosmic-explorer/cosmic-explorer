from cosmicexplorer import load_parameters, update_mapping
from wield.bunch import Bunch


def test_load_default():
    print("")
    parameters = load_parameters("reverse_ligo.yaml")
    print(parameters)


def test_load_custom(fpath_join):
    print("")
    p = load_parameters(fpath_join("custom.yaml"))
    print(p)
    assert p.A == 1
    assert p.B.C == 2
    assert p.B.D == 8


def test_update_mapping():
    print("")
    b1 = Bunch(a=1, b=Bunch(x=3, y=4), c=12)
    b2 = Bunch(c=20, b=Bunch(x=8))
    print(b1)
    print(b2)
    update_mapping(b1, b2)
    print(b1)
    assert b1.c == 20
    assert b1.b.x == 8
    assert b1.b.y == 4
    assert b1.a == 1


def test_inherit(fpath_join):
    print("")
    p = load_parameters(fpath_join("custom2.yaml"))
    print(p)
    assert p.A == 30
    assert p.B.C == 20
    assert p.B.D == 8
    assert p.B.E == 10
    assert p.Z == 90
    assert p.X.Y == "a"


def test_unset(fpath_join):
    print("")
    p = load_parameters(fpath_join("custom3.yaml"))
    print(p)
    assert "Z" not in p
    assert "X" not in p
    assert "C" not in p.B
    assert p.B.D == 1
    assert p.A == 30
